Salvadori14.dat https://ui.adsabs.harvard.edu/abs/2014MNRAS.437L..26S/abstract Fig.4, "all"
Pallottini14.dat https://ui.adsabs.harvard.edu/abs/2014MNRAS.440.2498P/abstract Fig.12, Zcut = 1e-8 Zsun
Jaacks18.dat https://ui.adsabs.harvard.edu/abs/2018MNRAS.475.4396J/abstract Fig.9, Z >= 1e-6 Zsun
Visbal20.dat https://ui.adsabs.harvard.edu/abs/2020arXiv200111118V/abstract Fig.5, Zcrit = 1e-6 Zsun