def plot_tutorial2():
    '''
    Plotting script to visualise results of the two scripts loop_scripts.py and analysis_trees.py.

    The plot layout is not optimized for aesthetics, but it should simply demonstrate the versatility of A-SLOTH.
    The resulting plot is a modified version of Fig. 17 in Hartwig+22
    '''

    import numpy as np
    from matplotlib import pyplot as plt

    #The file should have been generated by analyse_trees.py
    header = np.genfromtxt("Parameter_Exploration.dat",max_rows=1,dtype=str)
    data = np.genfromtxt("Parameter_Exploration.dat",skip_header=1)
    print("Loaded file")

    #Set plot parameters
    plt.rcParams.update({'font.size': 10})
    plt.rc('xtick', labelsize=10)
    plt.rc('ytick', labelsize=10)


    #Define quantities that should be plotted
    col_index = [2, 4]#Mmax, etaIII
    row_index = [11, 12, 13, 14, 15, 17, 18, 19, 20, 24, 25, 26, 27, 29, 30, 31]#observables


    columns = len(col_index)
    rows = len(row_index)

    fig, ax_array = plt.subplots(rows, columns,squeeze=True,figsize=(4.0,24.0))
    fig.subplots_adjust(hspace=0.1,wspace=0.1)

    print("Now plotting...")
    for i,ax_row in enumerate(ax_array):
        for j,axes in enumerate(ax_row):
            if(j==0):#first column
                axes.set_ylabel(header[row_index[i]])
            else:
                axes.set_yticklabels([]) 
                axes.set_xscale('log')

            
            if (i == rows-1):#last row
                axes.set_xlabel(header[col_index[j]])
            else:
                axes.set_xticklabels([])
            
            axes.tick_params(axis ='both', which ='both', direction = "in")    
            axes.scatter(data[:,col_index[j]],data[:,row_index[i]])

    plt.savefig("Parameter_Exploration_tutorial.pdf", bbox_inches='tight')

if(__name__ == "__main__"):
    plot_tutorial2()
