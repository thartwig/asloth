# Script to run Merger Code for many different parameter configurations automatically
# Can be used for parameter exploration and calibration
import os, sys
sys.path.insert(0, 'scripts') # needed to import utility

import numpy as np
from astropy.io import ascii
import time
import psutil
import glob
from matplotlib import pyplot as plt
import matplotlib
from utilities.utility import asloth_fit, asloth_config
matplotlib.use('Agg')


#SET YOUR SPECIFIC PATHS HERE
path_tree = "/mnt/data_cat5_SSD/thartwig/" #not required for EPS trees
path_output = "" #empty saves output in the a-sloth folder. Alternative directory can be specified here

#keep track of the available RAM and CPUs during runs
memory_min = 1000.#start with large value
CPU_max = 0#start with small value

wait_min = 12 # minimum time in seconds to wait until python script continues


def build(omp=True):
    os.system('make clean')
    os.system('rm *.o *.mod asloth.exe')
    if(omp):
        os.system('make omp') # parallel is usually faster
    else:
        os.system('make') # EPS trees can not be run in parallel

def run(i):
    os.system(f'./asloth.exe param_loop_{i}.nml &')
    return

def GetMyRandom(xmin,xmax):
    #sample a number liniarly between xmin and xmax
    dx = (xmax-xmin)
    return xmin+dx*np.random.rand()


def avail_mem_GB():
    '''
    Funtion returns available RAM in GB
    '''
    global memory_min
    global mem_array
    virtual = psutil.virtual_memory()
    avail_mem = virtual.available/(1024**3)
    print("Available Memory: ",avail_mem,"GB")
    mem_array.append(avail_mem)
    memory_min = np.min([memory_min,avail_mem])
    print("Minimum Memory: ",memory_min)
    return avail_mem


def CPU_use():
    '''
    Funtion returns available CPUs in %
    '''
    global CPU_max
    global CPU_array
    CPU = psutil.cpu_percent(2)#over 2s
    print("CPU use: ",CPU,"%")
    CPU_array.append(CPU)
    CPU_max = np.max([CPU_max,CPU])
    print("Max CPU: ",CPU_max,"%")
    return CPU

def N_disk_sleep():
    '''
    Funtion returns number of processes that are idle due to I/O.

    Change username accordingly
    '''
    n_sleep = 0
    try:
        for proc in psutil.process_iter(['pid', 'name', 'username']):
            if(proc.info['username']=='thartwig'):
                if(proc.status() == "disk-sleep"):
                    n_sleep += 1
    except:
        print("WARNING!!!")
        print("Maybe: psutil.NoSuchProcess: psutil.NoSuchProcess process no longer exists ??")
        n_sleep = 9

    print('Number Processes waiting for disk I/O:',n_sleep)
    return n_sleep


def RunTrees(cats,NBODY):
    '''
    This function loops over a set of merger trees and explores one combination of input parameters

    We first define input parameters that are constant within this loop.
    Then, we define tree-specific input parameters.
    Then, we check if sufficient computing resources are available.
    Eventually, we launch the A-SLOTH run.

    parameters
    ----------

        cats : list
            catalogue of merger trees [[name, nlev, RAM]]

        NBODY : bool
            N-Body or EPS-generated merger trees


    '''

    index = find_index()
    print("index: ",index)

#Generate parameters for this run
    cfg = asloth_config()

#Set fiducial parameters and explore IMF_max and ETAIII with random sampling
    cfg.config["tree_path"] = path_tree
    cfg.config["IMF_min"] = 13.6
    cfg.config["IMF_max"] = GetMyRandom(100,300)
    cfg.config["slope" ] = 1.77
    cfg.config["ETAIII"] = 10**GetMyRandom(-2.0,0.0)
    cfg.config["ETAII"] = 0.237
    cfg.config["alpha_outflow"] = 2.59
    cfg.config["m_cha_out"] = 8.39e9
    cfg.config["F_ESCIII"] = 0.525
    cfg.config["F_ESCII"] = 0.175
    cfg.config["VBC"] = 1.75
    cfg.config["c_ZIGM"] = 3.32
    cfg.config["lw_mode"] = 5

    time_start = time.time()

#loop over list of merger trees
#each tree here will be run with same set of physical input parameters
    for i,c in enumerate(cats):
        print("Next tree:",i,c[0],c[1],c[2])
        while True:
            needed_memory = c[2]
            print("Needed Memory: ",needed_memory)
            if(avail_mem_GB() > needed_memory and CPU_use() < 90 and N_disk_sleep() == 0):
                #enough resources available
                
                cfg.config["output_string"] = path_output+"output_para"+str(index)+"_"+ c[0]
                cfg.config["tree_name"] = c[0]
                cfg.config["nlev" ] = c[1]
                if(NBODY):
                    if (c[1] > 300):
                        cfg.config["cubic_box"] = False
                        cfg.config["nthreads"] = 4
                    else:#8Mpc box has fewer timesteps
                        cfg.config["cubic_box"] = True
                        cfg.config["nthreads"] = 16
                else:#EPS
                    cfg.config["cubic_box"] = False
                    cfg.config["nthreads"] = 1
                    cfg.config["tree_mass"] = 1.3e12 # mass of MW
                    cfg.config["RNG_SEED"] = i # we vary the random seed between realisations


                cfg.to_file(f"param_loop_{i}.nml")

                run(i)
                t_wait = np.max([1.5*needed_memory,wait_min]) # factor 1.5 is good for large merger trees that need to be read to memory
                print("Waiting for ",t_wait,"s") # give enough time to reach the memory allocation
                time.sleep(t_wait)
                break
            else:
                print("Not enough free resources: waiting...")
                time.sleep(wait_min)


    #write duration to file to use later
    duration_min = (time.time()-time_start)/60
    time_file = open(path_output+"para_"+str(index)+"_time.txt",'w')
    time_file.write(str(duration_min)+"\n")
    time_file.close()

def find_index():
    '''
    find next free index for output directories
    '''
    for i in range(1024):#checking up to 1024
        dir_prefix = path_output+"output_para"+str(i)+"_"
        print("Is there a directory starting with "+dir_prefix+"?")
        file_list = glob.glob(dir_prefix+"*")
        print(len(file_list))
        if(len(file_list) == 0):
            break
    return(i)

def Get_CatList(NBODY=False,N=3):
    '''
    Function to generate list of catalogues to loop over

    parameters
    ----------

        NBODY : bool
            N-Body or EPS-generated merger trees

        N : int
            number of random realisations in case of EPS-generated trees

    returns
    -------

        cats : list
            list of lists of type [[name, nlev, RAM]]
    '''
    cats = []
    if(NBODY):
        print("We will loop over DM merger trees from N-Body simulations")
        f = 'tree_files/cat_list_31.txt'
        catlist = np.genfromtxt(f, skip_header=1, dtype="str")
        for c in catlist:
            cats.append([c[0],int(c[1]),float(c[2])])
        return cats
    else:
        print("We will loop over DM merger trees generated with EPS")
        for i in range(N):
            cats.append(["EPS",256,4.5])#set last value to expected RAM per tree
        return cats

def plot_histograms(mem_array,CPU_array):
    plt.hist(mem_array,bins=40)
    plt.xlabel("Available Memory [GB]")
    plt.savefig("RAM_histogram.pdf")
    plt.clf()

    plt.hist(CPU_array,bins=25)
    plt.xlabel("Used CPU [%]")
    plt.savefig("CPU_histogram.pdf")
    plt.close("all")


if __name__ == '__main__':
    NBODY = False #if set to True, change src/asloth.h accordingly
    cats = Get_CatList(NBODY=NBODY,N=3)
    print(cats)

    mem_array = []
    CPU_array = []

    build(omp = NBODY) # because EPS crashed with OpenMP

    #loop here
    start_time = time.time()
    N_iter = 32
    duration = 0.5 #in hours

    #This loop will run until N_iter OR duration is reached
    for i in range(N_iter):
        if( time.time() > start_time + (3600*duration) ):
          break
        RunTrees(cats,NBODY)


    print("Minimum Memory: ",memory_min)

    plot_histograms(mem_array,CPU_array)

    print("All runs started. Waiting for A-SLOTH to finish...")
    print("If the python script ends before A-SLOTH, the command line might not return to prompt")
    print("In this case, press ENTER at the of of A-SLOTH to return to command line prompt")
