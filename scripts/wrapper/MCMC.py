# Script to run Merger Code for many different parameter configurations automatically
# Can be used for parameter exploration and calibration
import os, sys
sys.path.insert(0, 'scripts') # needed to import utility

import numpy as np
from astropy.io import ascii
import time
import psutil
import glob
import random
import copy
from matplotlib import pyplot as plt
import matplotlib
import socket
from utilities.utility import asloth_fit, asloth_config
from analyse_trees import get_p_from_folders
matplotlib.use('Agg')


#SET YOUR SPECIFIC PATHS HERE
hostname = socket.gethostname()
MCMCchain = "chain1"
path_tree = "/mnt/data_"+hostname+"_SSD/thartwig/" #not required for EPS trees
path_output = "/mnt/data_"+hostname+"/thartwig/MCMC_"+MCMCchain+"/" #empty saves output in the a-sloth folder. Alternative directory can be specified here

#Factor to scale jumps
MyFac = 1.0

#keep track of the available RAM and CPUs during runs
memory_min = 1000.#start with large value
CPU_max = 0#start with small value

wait_min = 25 # minimum time in seconds to wait until python script continues


#MCMC Step
class MCMCstep:
    def __init__(self):
        self.index = -1

#Set starting parameters of chain here
        self.TREENAME="H95289"
        self.TREEPATH=path_tree
        self.NUMBER_OF_TIMESTEPS=310
        self.OUTPUTSTRING="output"
        self.POPIII_IMFmin=5.819
        self.POPIII_IMFmax=152.2
        self.POPIII_slope=2.3
        self.ETA_III=11.951
        self.ETA_II=0.403
        self.ALPHAOUTFLOW=2.93
        self.MCHAOUT=8.346e9
        self.POPIII_ESC_ION=0.548
        self.POPII_ESC_ION=0.188
        self.V_BC=1.818
        self.c_ZIGM=3.233
        self.LWMODE=5

def Copy_Prev_Parameters(Step_old):

    Step_new = MCMCstep()

    Step_new.POPIII_IMFmin = Step_old.POPIII_IMFmin
    Step_new.POPIII_IMFmax = Step_old.POPIII_IMFmax
    Step_new.POPIII_slope = Step_old.POPIII_slope
    Step_new.ETA_III = Step_old.ETA_III
    Step_new.ETA_II = Step_old.ETA_II
    Step_new.ALPHAOUTFLOW = Step_old.ALPHAOUTFLOW
    Step_new.MCHAOUT = Step_old.MCHAOUT
    Step_new.POPII_ESC_ION = Step_old.POPII_ESC_ION
    Step_new.POPIII_ESC_ION = Step_old.POPIII_ESC_ION
    Step_new.V_BC = Step_old.V_BC
    Step_new.c_ZIGM = Step_old.c_ZIGM
    Step_new.index = Step_old.index

    return Step_new


def build(omp=True):
    os.system('make clean')
    os.system('rm *.o *.mod asloth.exe')
    if(omp):
        os.system('make omphdf5') # parallel is usually faster
    else:
        os.system('make') # EPS trees can not be run in parallel

def run(i):
    os.system(f'./asloth.exe param_loop_'+hostname+'_'+MCMCchain+'_'+str(i)+'.nml &')
    return

def GetMyRandom(xmin,xmax):
    #sample a number liniarly between xmin and xmax
    dx = (xmax-xmin)
    return xmin+dx*np.random.rand()


def avail_mem_GB():
    '''
    Funtion returns available RAM in GB
    '''
    global memory_min
    global mem_array
    virtual = psutil.virtual_memory()
    avail_mem = virtual.available/(1024**3)
    print("Available Memory: ",avail_mem,"GB")
    mem_array.append(avail_mem)
    memory_min = np.min([memory_min,avail_mem])
    print("Minimum Memory: ",memory_min)
    return avail_mem


def CPU_use():
    '''
    Funtion returns available CPUs in %
    '''
    global CPU_max
    global CPU_array
    CPU = psutil.cpu_percent(2)#over 2s
    print("CPU use: ",CPU,"%")
    CPU_array.append(CPU)
    CPU_max = np.max([CPU_max,CPU])
    print("Max CPU: ",CPU_max,"%")
    return CPU

def N_disk_sleep():
    '''
    Funtion returns number of processes that are idle due to I/O.

    Change username accordingly
    '''
    n_sleep = 0
    try:
        for proc in psutil.process_iter(['pid', 'name', 'username']):
            if(proc.info['username']=='thartwig'):
                if(proc.status() == "disk-sleep"):
                    n_sleep += 1
    except:
        print("WARNING!!!")
        print("Maybe: psutil.NoSuchProcess: psutil.NoSuchProcess process no longer exists ??")
        n_sleep = 9

    print('Number Processes waiting for disk I/O:',n_sleep)
    return n_sleep


def RunTrees(cats,StepNow):
    '''
    This function loops over a set of merger trees and explores one combination of input parameters

    We first define input parameters that are constant within this loop.
    Then, we define tree-specific input parameters.
    Then, we check if sufficient computing resources are available.
    Eventually, we launch the A-SLOTH run.

    parameters
    ----------

        cats : list
            catalogue of merger trees [[name, nlev, RAM]]

        StepNow : object
            TBD


    '''

    index = StepNow.index
    print("index: ",index)

#Generate parameters for this run
    cfg = asloth_config()

#Set fiducial parameters and explore IMF_max and ETAIII with random sampling
    cfg.config["tree_path"] = path_tree
    cfg.config["IMF_min"] = StepNow.POPIII_IMFmin
    cfg.config["IMF_max"] = StepNow.POPIII_IMFmax
    cfg.config["slope" ] = StepNow.POPIII_slope
    cfg.config["ETAIII"] = StepNow.ETA_III
    cfg.config["ETAII"] = StepNow.ETA_II
    cfg.config["alpha_outflow"] = StepNow.ALPHAOUTFLOW
    cfg.config["m_cha_out"] = StepNow.MCHAOUT
    cfg.config["F_ESCIII"] = StepNow.POPIII_ESC_ION
    cfg.config["F_ESCII"] = StepNow.POPII_ESC_ION
    vbc = StepNow.V_BC
    cfg.config["c_ZIGM"] = StepNow.c_ZIGM
    cfg.config["lw_mode"] = 5

    time_start = time.time()

#loop over list of merger trees
#each tree here will be run with same set of physical input parameters
    for i,c in enumerate(cats):
        print("Next tree:",i,c[0],c[1],c[2])
        while True:
            needed_memory = 1.25*c[2]
            print("Needed Memory: ",needed_memory)
            if(avail_mem_GB() > needed_memory and CPU_use() <= 95 and N_disk_sleep() == 0):
                #enough resources available
 
#                cfg.config["output_string"] = path_output+"output_para"+str(index)+"_"+ c[0]
                cfg.config["tree_name"] = c[0]
                cfg.config["nlev" ] = c[1]

                StepNow.OUTPUTSTRING = path_output+MCMCchain+"_output_para"+str(StepNow.index)+"_"
                cfg.config["output_string"] = StepNow.OUTPUTSTRING + c[0]

                StepNow.TREENAME = c[0]
                StepNow.NUMBER_OF_TIMESTEPS=c[1]

                if (c[1] > 300):
                    cfg.config["cubic_box"] = False
                    cfg.config["nthreads"] = 4
                    cfg.config["VBC"] = vbc
                else:#8Mpc box has fewer timesteps
                    cfg.config["cubic_box"] = True
                    cfg.config["nthreads"] = 16
                    cfg.config["VBC"] = 0.8


                cfg.to_file(f"param_loop_"+hostname+'_'+MCMCchain+'_'+str(i)+".nml")

                run(i)
                t_wait = np.max([0.5*needed_memory,wait_min]) # factor 1.5 is good for large merger trees that need to be read to memory
                print("Waiting for ",t_wait,"s") # give enough time to reach the memory allocation
                time.sleep(t_wait)
                break
            else:
                print("Not enough free resources: waiting...")
                time.sleep(wait_min)


    #write duration to file to use later
    duration_min = (time.time()-time_start)/60
    time_file = open(path_output+"para_"+str(index)+"_time.txt",'w')
    time_file.write(str(duration_min)+"\n")
    time_file.close()


def Get_CatList(NBODY=False,N=3):
    '''
    Function to generate list of catalogues to loop over

    parameters
    ----------

        NBODY : bool
            N-Body or EPS-generated merger trees

        N : int
            number of random realisations in case of EPS-generated trees

    returns
    -------

        cats : list
            list of lists of type [[name, nlev, RAM]]
    '''
    cats = []
    if(NBODY):
        print("We will loop over DM merger trees from N-Body simulations")
        f = 'tree_files/cat_list_31.txt'
        catlist = np.genfromtxt(f, skip_header=1, dtype="str")
        for c in catlist:
            cats.append([c[0],int(c[1]),float(c[2])])
        return cats
    else:
        print("We will loop over DM merger trees generated with EPS")
        for i in range(N):
            cats.append(["EPS",256,4.5])#set last value to expected RAM per tree
        return cats

def plot_histograms(mem_array,CPU_array):
    plt.hist(mem_array,bins=40)
    plt.xlabel("Available Memory [GB]")
    plt.savefig("RAM_histogram.pdf")
    plt.clf()

    plt.hist(CPU_array,bins=25)
    plt.xlabel("Used CPU [%]")
    plt.savefig("CPU_histogram.pdf")
    plt.close("all")


def IfStop():
    #check if stop file exists
    if(os.path.isfile("STOP")):
        print("STOP file detected")
        os.system('rm STOP')
        return True
    else:
        return False


#get iteratable object ob random indices we want to explore
def GetIter():
    idx_list = [0,1,2,3,4,5,6,7,8,9,10]#important indices twice as often?
    #idx_list = [4,6,6]
    random.shuffle(idx_list)
    return iter(idx_list)

idx_iter = GetIter()#to start with
#gives next element of list, creates new list after each loop over it
def MyIter(idx_iter):
    i_next = next(idx_iter,-1)
    if(i_next == -1):#end of list
        idx_iter = GetIter()
        i_next = next(idx_iter,-1)
    return i_next,idx_iter

def SetOneInput(Step_next, i, set_value):
    if(i==0):
        Step_next.POPIII_IMFmin  =  set_value
    elif(i==1):
        Step_next.POPIII_IMFmax  =  set_value
    elif(i==2):
        Step_next.POPIII_slope  =  set_value
    elif(i==3):
        Step_next.ETA_III  =  set_value
    elif(i==4):
        Step_next.ETA_II  =  set_value
    elif(i==5):
        Step_next.ALPHAOUTFLOW  =  set_value
    elif(i==6):
        Step_next.MCHAOUT  =  set_value
    elif(i==7):
        Step_next.POPIII_ESC_ION  =  set_value
    elif(i==8):
        Step_next.POPII_ESC_ION  =  set_value
    elif(i==9):
        Step_next.c_ZIGM  =  set_value
    elif(i==10):
        Step_next.V_BC  =  set_value

    return Step_next

def idx2str(i):
    if(i==0):
        return "POPIII_IMFmin"
    elif(i==1):
        return "POPIII_IMFmax"
    elif(i==2):
        return "POPIII_slope"
    elif(i==3):
        return "ETA_III"
    elif(i==4):
        return "ETA_II"
    elif(i==5):
        return "ALPHAOUTFLOW"
    elif(i==6):
        return "MCHAOUT"
    elif(i==7):
        return "POPIII_ESC_ION"
    elif(i==8):
        return "POPII_ESC_ION"
    elif(i==9):
        return "c_ZIGM"
    elif(i==10):
        return "V_BC"

def GetOneInput(Step_next, i):
    if(i==0):
        return Step_next.POPIII_IMFmin
    elif(i==1):
        return Step_next.POPIII_IMFmax
    elif(i==2):
        return Step_next.POPIII_slope
    elif(i==3):
        return Step_next.ETA_III
    elif(i==4):
        return Step_next.ETA_II
    elif(i==5):
        return Step_next.ALPHAOUTFLOW
    elif(i==6):
        return Step_next.MCHAOUT
    elif(i==7):
        return Step_next.POPIII_ESC_ION
    elif(i==8):
        return Step_next.POPII_ESC_ION
    elif(i==9):
        return Step_next.c_ZIGM
    elif(i==10):
        return Step_next.V_BC

def VaryOneInput(Step_next, i, para_orig):
    print("Index to vary:",i)

    #general: use fraction of stddev() between top 6 models
    
    if(i==0):
        dx = np.random.normal(scale=MyFac*2)
        Mnow = np.max([(para_orig + dx),0.8])#at least 0.8
        Mnow = np.min([Mnow,25])#at most 25 (Ishigaki+18)
        Step_next.POPIII_IMFmin  =  Mnow

    elif(i==1):
        dx = np.random.normal(scale=MyFac*0.1)#LOG
        Mnow  =  10.0**(np.log10(para_orig) + dx)
        Mnow = np.max([Mnow,100])#ensures Mmax > 100
        Mnow = np.min([Mnow,400])#ensures Mmax < 400
        Step_next.POPIII_IMFmax  =  Mnow

    elif(i==2):
        dx = np.random.normal(scale=0.23*MyFac)
        x = np.min([para_orig + dx,2.3])#limit slope at Salpeter
        x = np.max([-2.3,x])#limit too top-heavy to prevent degeneracies
        Step_next.POPIII_slope  =  x

    elif(i==3):
        dx = np.random.normal(scale=MyFac*0.4)#LOG
        ETA_now = 10.0**(np.log10(para_orig) + dx)
        Step_next.ETA_III  =  ETA_now

    elif(i==4):
        dx = np.random.normal(scale=MyFac*0.2)#LOG
        ETA_now = 10.0**(np.log10(para_orig) + dx)
        Step_next.ETA_II  =  ETA_now

    elif(i==5):
        dx = np.random.normal(scale=MyFac*0.2)
        x = para_orig + dx
        x = np.min([x,5])#limit to prevent degeneracies
        Step_next.ALPHAOUTFLOW  =  x

    elif(i==6):
        dx = np.random.normal(scale=MyFac*0.1)#LOG
        x = 10.0**(np.log10(para_orig) + dx)
        Step_next.MCHAOUT  =  x

    elif(i==7):
        dx = np.random.normal(scale=MyFac*0.1)
        x_temp = para_orig + dx
        x_temp = np.abs(x_temp)
        if(x_temp > 0.99):
            x_temp = 0.99
        Step_next.POPIII_ESC_ION  =  x_temp

    elif(i==8):
        dx = np.random.normal(scale=MyFac*0.1)
        x_temp = para_orig + dx
        x_temp = np.abs(x_temp)
        if(x_temp > 0.99):
            x_temp = 0.99
        Step_next.POPII_ESC_ION  =  x_temp

    elif(i==9):
        dx = np.random.normal(scale=0.04*MyFac)#LOG
        Step_next.c_ZIGM  =  10.0**(np.log10(para_orig) + dx)

    elif(i==10):
        dx = np.random.normal(scale=0.1*MyFac)
        x = np.max([para_orig + dx,0])
        x = np.min([para_orig + dx,2.2])#Mcrit is independent of V_BC above 2.2
        Step_next.V_BC  =  x

    return Step_next


def get_params_string(Step_new):
    #for output file
    TestedParamsString = str(Step_new.index)+" "
    TestedParamsString += str(Step_new.POPIII_IMFmin)+" "
    TestedParamsString += str(Step_new.POPIII_IMFmax)+" "
    TestedParamsString += str(Step_new.POPIII_slope)+" "
    TestedParamsString += str(Step_new.ETA_III)+" "
    TestedParamsString += str(Step_new.ETA_II)+" "
    TestedParamsString += str(Step_new.ALPHAOUTFLOW)+" "
    TestedParamsString += str(Step_new.MCHAOUT)+" "
    TestedParamsString += str(Step_new.POPIII_ESC_ION)+" "
    TestedParamsString += str(Step_new.POPII_ESC_ION)+" "
    TestedParamsString += str(Step_new.c_ZIGM)+" "
    TestedParamsString += str(Step_new.V_BC)+" "

    return TestedParamsString


def Get_N_idx(Ndim,idx_iter):
    #gets 3 next indices to explore
    idx_list = []
    while (len(idx_list) < Ndim):
        #add one more index
        i,idx_iter = MyIter(idx_iter)
        if(i not in idx_list):
            idx_list.append(i)
    return idx_list,idx_iter
    


if __name__ == '__main__':
    NBODY = True #if set to True, change src/asloth.h accordingly
    cats = Get_CatList(NBODY=NBODY,N=3)
    print(cats)
    Ncats = len(cats)
    Ndim = 3#how many different dimension should be explored simultaneously in one MCMC step?

#    file_name = "MCMC_"+MCMCchain+"_"+hostname+"_results.dat"
    file_name2 = "MCMC_"+MCMCchain+"_"+hostname+"_accepted.dat"
#    MCMC_file = open(file_name,'w')
#    MCMC_file.write("LogL LogL_tau LogL_SFRD_HST LogL_SFRD_JWST LogL_MstarMW LogL_MassMetallicity LogL_sat LogL_MDF LogL_EMPAll LogL_PISN index Mmin Mmax slope SFRIII SFRII AlphaOutflow Mchar fescIII fescII cZIGM Vbc Ntrees iacc \n")
#    MCMC_file.close()

    MCMC_file = open(file_name2,'w')
    MCMC_file.write('idx i_acc p_acc idx1 idx2 idx3 index Mmin Mmax slope SFRIII SFRII AlphaOutflow Mchar fescIII fescII cZIGM Vbc '+\
                   'LogL LogL_tau LogL_SFRD_HST LogL_SFRD_JWST LogL_MstarMW LogL_MassMetallicity LogL_sat LogL_MDF LogL_EMPAll LogL_PISN Ntrees \n')
    MCMC_file.close()

    mem_array = []
    CPU_array = []
    Step_now = MCMCstep()
    Step_old = copy.deepcopy(Step_now)#start with this initial one as old step

    i_now = 1122
    LogL_old = -31#-100 #very small so that first value is always accepted
    #step_list = []
    #params_list = []

    #loop here
    start_time = time.time()
    duration = 10000.0 #in hours
    #the code will start a new parameter exploration until duration is reached.
    while(time.time() < start_time + (3600*duration) ):
        if(IfStop()):
            break

        Step_now = Copy_Prev_Parameters(Step_now)
        Step_now.index = i_now#Step_now.index+1

        if (i_now == 0):
            #run initial parameters
            idx_list = [-1 for k in range(Ndim)]
            print("Exceptional index list for first iteration:",idx_list)
        else:
            #get next index list to explore
            idx_list,idx_iter = Get_N_idx(Ndim,idx_iter)#previously: index i            
            for i in idx_list:
                para_orig = GetOneInput(Step_now, i)#numerical value that we start this iteration with
                print("We now vary parameter:",i,"  which previously was:",para_orig)
                Step_now = VaryOneInput(Step_now, i, para_orig)

        #test Step_new
        RunTrees(cats,Step_now)
#        step_list.append(Step_now)
#####


        print("MCMC: Waiting for all trees to finish...")
        CheckFile = Step_now.OUTPUTSTRING+"*/z_cSFR.dat"#Step_now is last started
        Tstart = time.time()
        while ((time.time()-Tstart) < 3.6e4):#wait at most 10h
            file_list = glob.glob(CheckFile)
            Nready = len(file_list)
            if(Nready >= Ncats):
                print("MCMC: all trees are ready!")
                break
            else:
                print("MCMC: Trees completed:",Nready)
                time.sleep(10)

        Ntrees = Ncats#all trees included

        L_str_list = []
        LogL_list = []
        para_list = []
        dir_prefix = Step_now.OUTPUTSTRING
        MyFit = get_p_from_folders(dir_prefix,ifEPS=False)
        L_str = str(MyFit.LogL)+" "+str(MyFit.LogL_tau)+" "+str(MyFit.LogL_SFRD_HST)+" "+str(MyFit.LogL_SFRD_JWST)+" "+str(MyFit.LogL_MstarMW)+" "+str(MyFit.LogL_MassMetallicity)+" "\
               +str(MyFit.LogL_sat)+" "+str(MyFit.LogL_MDF)+" "+str(MyFit.LogL_EMPAll)+" "+str(MyFit.LogL_PISN)+" "
#            L_str_list.append(L_str)
#            LogL_list.append(MyFit.LogL)
#            para_list.append(GetOneInput(Step_now, i))#numerical value of parameter that was varied
#        print("lists")
#        print(L_str_list)
#        print(LogL_list)
#        print(para_list)

        print("New LogL:",MyFit.LogL)
        print("Previous LogL:",LogL_old)

        ParamsString_now = get_params_string(Step_now)
        L_now = MyFit.LogL
#MCMC Metropolis Hastings acceptance ratio
        if(L_now > LogL_old):
            i_accept = 1
            p_accept = 1.0
            print("Accept jump!")
            Step_old = copy.deepcopy(Step_now)
            LogL_old = L_now
        else:
            p_accept = np.exp(L_now - LogL_old)
            print("Probability to accept jump:",p_accept)
            if(p_accept > np.random.rand()):
                i_accept = 1
                print("Accept jump!")
                Step_old = copy.deepcopy(Step_now)
                LogL_old = L_now
            else:
                i_accept = 0
                print("Do NOT accept jump!")
                Step_now = copy.deepcopy(Step_old)


        #prepare next iteration
#        Step_now.index = Step_now.index#last index used

#        MCMC_file = open(file_name,'a')
#        MCMC_file.write(L_str+ParamsString_now+str(Ntrees)+" "+str(i_accept)+" \n")
#        MCMC_file.close()#to have outputs readible already during run

        #make string of used indices
        idx_str = " "
        for idx_now in idx_list:
            idx_str += (str(idx_now)+" ")

        MCMC_file = open(file_name2,'a')
        MCMC_file.write(str(i_now)+" "+str(i_accept)+" "+str(p_accept)+" "+idx_str+ParamsString_now+" "+L_str+" "+str(Ntrees)+" \n")
        MCMC_file.close()#to have outputs readible already during run
        i_now += 1





    print("Minimum Memory: ",memory_min)

    plot_histograms(mem_array,CPU_array)

    print("All runs started. Waiting for A-SLOTH to finish...")
    print("If the python script ends before A-SLOTH, the command line might not return to prompt")
    print("In this case, press ENTER at the of of A-SLOTH to return to command line prompt")
