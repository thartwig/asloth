import numpy as np
import pandas as pd
from sklearn.cluster import KMeans, AgglomerativeClustering, \
  DBSCAN, MeanShift, estimate_bandwidth, SpectralClustering, \
  AffinityPropagation, Birch

from rpy2.robjects import r
import rpy2.robjects.numpy2ri
from rpy2.robjects.packages import importr
rpy2.robjects.numpy2ri.activate()

def clustering_FisherFreemanHaltonExact(dfs, nc, exp_ratio, method):
  """
    Function to first find clusters in high dimensional space.
    Then it computes a p-value from the Fisher-Freeman-Halton test.

    Parameters:
      dfs (list of pd.DataFrame)
      nc (int): number of clusters
      exp_ratio (float): expected ratio between subset 1 and subset 2
      method (str): which unsupervised clustering algorithm to use

    Returns:
      A p-value (float).
  """

  sts = importr('stats')
  
  ids = ['Obs.', 'A-SLOTH']
  markers = ['s', 'o']

  ### Find clusters with unsupervised clustering algorithm ###
  df = dfs[0]
  for ii in range(1, len(dfs)):
   df = pd.concat([df, dfs[ii]], ignore_index=True)
  ### Remove labels before applying unsupervised clustering 
  temp = df.drop('ID', axis=1).copy()
  temp = temp.drop('ind', axis=1).copy()
  if (method == 'KMeans'):
    clust = KMeans(n_clusters=nc, random_state=1729).fit(temp)
    clus_met = '_KMeans'
  elif (method == 'Agglomerative'):
    clust = AgglomerativeClustering(n_clusters=nc).fit(temp)
    clus_met = '_Agglomerative'
  elif (method == 'DBSCAN'):
    clust = DBSCAN(eps=0.6).fit(temp) # eps=11, min_samples=6
    nc = np.unique(clust.labels_).shape[0]
    clus_met = '_DBSCAN'
  elif (method == 'MSA'):
    bandwidth = estimate_bandwidth(temp, quantile=0.1)
    clust = MeanShift(bandwidth=bandwidth).fit(temp)
    nc = np.unique(clust.labels_).shape[0]
    clus_met = '_MSA'
  elif (method == 'Spectral'):
    clust = SpectralClustering(n_clusters=nc).fit(temp)
    clus_met = '_SpectralClustering'
  elif (method == 'AffinityPropagation'):
    clust = AffinityPropagation().fit(temp)
    nc = np.unique(clust.labels_).shape[0]
    clus_met = '_AffinityPropagation'
  elif (method == 'BIRCH'):
    clust = Birch(n_clusters=nc).fit(temp)
    clus_met = '_BIRCH'
  else:
    print ('Undefined clustering method.')
    return [-100]
  df['Cluster'] = clust.labels_
  
  ### Construct the contingy table ### 
  info = get_ratio(df, nc, len(ids), ids)
  m = []
  for ii in range(nc):
    m.append( [info[ii][jj] for jj in range(len(ids))] )
  
  ### Obtain p-value from the Fisher-Freeman-Halton test
  if (nc >= 2):
    pv = sts.fisher_test(x=np.array(m), hybrid=True, workspace=1e9) # , workspace=1e9 simulate.p.value=TRUE not working
  else: 
    print (f'Number of cluster is 1')
    pv = [np.nan]
  
  return pv, m 

def get_ratio(df, nc, nd, idname):
  """
    Function to compute the how many observed/simulated data are classified in each cluster.
  
    Parameters:
      df (pd.Dataframe): the data
      nc (int): number of clusters
      nd (int): number of underlying distribution. Default: 2
      idname (str): list of names that we initially give to the data. Obs and A-SLOTH in this case. 

    Returns: 
      An array that contains how many observed/simulated data are classified in each cluster

  """
  info = np.array([ np.zeros(nd+1) for ii in range(nc) ])
  for jj in range(nc):
    for ii in range(nd):
      for kk in range(len(df['ID'])):  
        if (df['ID'][kk] == idname[ii] and df['Cluster'][kk] == jj):
          info[jj][ii] += 1
          info[jj][-1] += 1

  return info
