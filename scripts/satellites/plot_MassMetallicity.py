def plot_MassMetallicity(UserFolder,UserLabels,folder_output):
    '''
    Plot Mass Metallicity relation from A-SLOTH and compare with observations.
  
    Parameters
    ----------
        UserFolder: List[str]
            List of folder names to be plotted

        UserLabels: List[str]
            List of labels. If none, UserFolders are used as labels

        folder_output: str
            Folder in which plots will be saved

  
    Returns
    -------
        MyFit: asloth_fit
    '''
    import os.path
    import numpy as np
    import matplotlib.pyplot as plt
    import matplotlib
    from utilities.utility import asloth_fit, LogLikelihood#(x_obs,sigma,x_sim)
    from asloth_PlotConf import SetMyLayout


    SetMyLayout()
    MyFit = asloth_fit()

    N_plot = len(UserFolder)

    LogL = 0.
    NlogL = 0

    #Ursa Minor is 10th most massive Satellite according to Maoz+18
    #in units of log10(Msun)
    NthMostMassive = 5.546
    Nmassive = []

    for folder,label in zip(UserFolder,UserLabels):
        file_input = folder+"/Satellites/sats.dat"
        if (os.path.isfile(file_input) and os.stat(file_input).st_size > 0):
            #print("The file "+file_input+" exists.")
            abc = 0
        else:
            print("The file "+file_input+" does not exist or is empty.")
            return MyFit

        data = np.loadtxt(file_input, skiprows = 2)
        FeH = data[:,8]
        Mstar = np.log10(data[:,6])

        idx = np.argsort(Mstar)
        Mstar = Mstar[idx]
        FeH = FeH[idx]
        M_now = Mstar[-10]
        FeH_now = FeH[-10]
        Nmassive.append(M_now)

        for x,y in zip(Mstar, FeH):
            if(x >= 5.5 and x <= 8.5):
                x_sim = -1.69 + 0.3 * (x - 6.)
                LogL += LogLikelihood(y,0.16,x_sim)
                NlogL += 1

        plt.scatter(Mstar, FeH, marker="o", c="k",label=label,alpha = 0.125)
        plt.scatter(M_now, FeH_now, marker="x", c="orange", zorder=99)
    #Kirby+13, Fig.8 and Eq. 4
    plt.plot([5.5,8.5],[-1.69-0.3*0.5,-1.69+0.3*2.5],label="Kirby+13")

    #Nth most massive satellites
    plt.axvline(x=NthMostMassive, c="orange", label="10th most massive")
    MyMean = np.mean(Nmassive)
    MyStd = np.std(Nmassive)
    #print("10th most massive mean and std:",MyMean,MyStd)
    MySigma = np.sqrt(MyStd**2+0.1**2)#0.1 is distance to next massive satellites
    LogL_sat = LogLikelihood(NthMostMassive,MySigma,MyMean)

    #plt.xscale("linear")
    #plt.yscale("log")
    #plt.ylim(1e-6, 1e2)
    plt.ylabel("[Fe/H]")
    plt.xlabel("log(M$_*/M_\odot$)")
    plt.legend()



    # savefig.                                                                                                                          
    plt.savefig(folder_output+"MassMetallicity.pdf", bbox_inches="tight")
    plt.close()

    return LogL/float(NlogL), LogL_sat
