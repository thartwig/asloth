#script to set uniform plotting environment
from matplotlib import pyplot as plt
import configparser
import os

def SetMyLayout(col=1, ratio = 0.75):
    """
    Script to set uniform plotting environment.

    parameters:
    -----------

        col : int
            number of columns that figure should span (1 or 2)

        ratio : float
            ratio of figure height to width

    returns:
    --------

        fig: figure object
    """

#load config file
    config = configparser.ConfigParser()
    path = os.path.dirname(__file__)
    config.read(os.path.join(path,'asloth_PlotConfig.ini'))

#set layout
    if(col == 2):
        size = float(config['width']['2col'])
    #standard one column
    else:
        size = float(config['width']['1col'])
    fig = plt.figure(figsize=(size,ratio*size))

    plt.rcParams.update({'font.size': config['font']['label']})

    plt.rc('xtick', labelsize=int(config['font']['ticks']))
    plt.rc('ytick', labelsize=int(config['font']['ticks']))
    plt.rc('legend', fontsize=int(config['font']['legend']))

    return fig
