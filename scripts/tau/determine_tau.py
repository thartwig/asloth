import numpy as np
from scipy.interpolate import interp1d


# wrapper for tau function that can be called with
# only the target directory as argument
def get_tau(folder):
    file_input = folder+"/vol_fracs.dat"
    data = np.genfromtxt(file_input, skip_header=1)
    z = data[:, 0]
    f_rand = data[:, 1]
    f_add = data[:, 3]/data[:, 5]
    tau0, z, Q = calculate_tau(z, f_add)
    return tau0


def calculate_tau(z_in, Q_in):
    N = 1000
    # natural constants
    SIGMA_T = 0.665e-24  # Thomson cross section
    HUBBLE = 0.6774
    OMEGA_m = 0.3089
    OMEGA_b = 0.04860
    OMEGA_L = 0.6911
    YHE = 0.2477

    C_cgs = 2.998e10
    G_cgs = 6.674e-8
    YEAR_cgs = 3.156e7
    MP_cgs = 1.673e-24
    MSUN_cgs = 1.989e33
    MPC_cgs = 3.0857e24

    XH = 1-YHE
    MEAN_MOL = 1.0/(1.0-0.75*YHE)
    HUBBLE_cgs = HUBBLE*100.0*1.0e5/MPC_cgs
    RHO_CRIT_cgs = (3.0*HUBBLE_cgs*HUBBLE_cgs)/(8.0*3.141593*G_cgs)
    RHO_b_cgs = OMEGA_b*RHO_CRIT_cgs
    N_b_cgs = RHO_b_cgs/(MEAN_MOL*MP_cgs)
    N_H_cgs = XH*N_b_cgs

    z_min = np.min(z_in)
    z_max = np.max(z_in)

    Nz = len(z_in)
    # fit line for extrapolation through last two points
    # y = m * x + c
    x1 = z_in[Nz-1]
    x2 = z_in[Nz-2]
    y1 = Q_in[Nz-1]
    y2 = Q_in[Nz-2]
    m = (y2-y1)/(x2-x1)
    c = y1 - m*x1

    # interpolation
    f = interp1d(z_in, Q_in, bounds_error=False,
                 fill_value=(Q_in[-1], Q_in[0]))
    z = np.linspace(0, z_max, num=N, endpoint=True)
    Q = np.zeros(N)
    Q = f(z)

    # linear extrapolation
    if(np.max(Q) < 1):
        print("Linear fit with m =", m, " and c=", c)
        mask = (z < z_min)
        Q[mask] = m*z[mask] + c

    Q[Q > 1] = 1.0
    if np.min(Q) < 0:
        raise ValueError("Q less than zero, this should not happen")

    dz = z[1]-z[0]

    a = 1./(1.+z)
    dtdz = a**2.5/(HUBBLE_cgs*np.sqrt(OMEGA_L*(a*a*a)+OMEGA_m))

    # He reionisation
    f_e = np.zeros(len(z))
    f_e[:] = 1.0 + 0.25*YHE/XH
    # assumes the single ionization fraction of helium is
    # the same as hydrogen ionization fraction at z>=4
    f_e[z < 4.0] = 1.0 + 0.5*YHE/XH
    # and helium is fully doubly ionized below z=4
    tau0 = np.sum(Q*f_e*((1+z)**3)*dz*dtdz*C_cgs*SIGMA_T*N_H_cgs)

    return tau0, z, Q
