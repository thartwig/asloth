def plot_tgas_dust(output_folder):
    '''
    Illustrates time and redshift evolution of different baryonic quantities based on output file t_gas_substeps.dat.
    output_folder has to be set manually.

    Parameters
    ----------
        output_folder : str
            folder that contains results of run that should be analysed
    '''
    import sys
    sys.path.insert(0, 'scripts') # needed to import SetMyLayout

    import os.path
    import numpy as np
    import matplotlib.pyplot as plt
    from asloth_PlotConf import SetMyLayout
    

    fig = SetMyLayout(col=2, ratio = 0.5)
    axs = fig.subplots(nrows=2,ncols=1,sharex=True, sharey=False, gridspec_kw={'height_ratios': [2, 1]})
    
    
    file_input = output_folder+"/t_gas_substeps.dat"
    if os.path.isfile(file_input):
        print("The file "+file_input+" exists.")
    else:
        print("The file "+file_input+" does not exist.")
    
    data = np.genfromtxt(file_input,skip_header=2)
    ids = data[:,0]
    Ntot = len(ids)
    
    t = data[:,1]/(1e6*3.14e7)#now in Myr after BB
    z = data[:,2]
    Mstar_form = data[:,3]
    Mhalo = data[:,4]
    Mstar_II = data[:,5]
    Mhot = data[:,6]
    Mcold = data[:,7]
    Mout = data[:,8]
    Mdust_hot = data[:,9]
    Mdust_cold = data[:,10]
    Mmetals_now = data[:,11]
    SNenergy_now = data[:,12]
    MHeat_now = data[:,13]
    SNmom = data[:,14]
    Mstar_III = data[:,15]
    Zgas = data[:,16]
    ZIGM = data[:,17]-np.log10(0.02)#mass ratio to Solar
    
    #normalise to illustrate them on a mass scale
    for i in range(Ntot):
        if(SNenergy_now[i]>0):
            SNenergy_now[i]=1
        if(SNmom[i]>0):
            SNmom[i]=10
    
    
    dt = np.zeros(Ntot) # timestep used for numerical integration
    for i in range(Ntot-1):
        dt[i] = t[i+1]-t[i]
    dt[Ntot-1] = t[Ntot-1]-t[Ntot-2]#last entry
    
    Mion = 4.5e7*((0.1*(z+1))**(-1.5))
    
    axs[0].scatter(t,Mhalo,label="$M_\mathrm{vir}$",s=3,color="g")
    axs[0].scatter(t,Mout,label="$M_\mathrm{out}$",s=3,color="pink")
    
    axs[0].scatter(t,Mhot,label="$M_\mathrm{hot}$",s=3,color="r")
    axs[0].scatter(t,Mcold,label="$M_\mathrm{cold}$",s=3,color="b")
    
    axs[0].scatter(t,Mstar_II,label="$M_\mathrm{*,II}$",s=3,color="c")
    axs[0].scatter(t,Mstar_III,label="$M_\mathrm{*,III}$",s=3,color="orange")
    
    axs[0].plot(t,Mdust_hot,label="$M^\mathrm{dust}_\mathrm{hot}$", color='black')
    axs[0].plot(t,Mdust_cold,label="$M^\mathrm{dust}_\mathrm{cold}$",color="gray")
    
    axs[0].scatter(t,Mstar_form,label="$\delta M_\mathrm{*}$",s=3,color="m")
    axs[0].scatter(t,Mmetals_now,label="$\delta M_\mathrm{metals}$",color="y",s=3)
    
#additional options for quantities to plot:
    #axs[0].scatter(t,dt,label="dt/Myr",s=3,color="gray")
    #axs[0].scatter(t,SNenergy_now,label="SNenergy_now",marker="x",color="g")
    #axs[0].plot(t,10*Mion,label="10*Mion",linestyle="--",color="gray")
    #axs[0].plot(t,Mion,label="Mion",linestyle="--",color="black")
    #axs[0].axhline(y=2000,label="MJeans",linestyle=":",color="black")
    
    axs[0].set_ylim([1e-2,3e9])
    
    axs[0].set_ylabel('M$_\odot$')
    axs[0].legend(loc = "lower right",framealpha=0.9)
    axs[0].set_yscale("log")
#    axs[0].set_xticks([110,130,150,170,190],minor=True) 

#add grid to guide the eye    
    for ax_now in axs:
      ax_now.grid(True, which="both", linewidth=0.25, color='grey', linestyle='-',alpha=0.5,zorder=-32)
    
    
    axs[1].set_xlabel('Myr')
    axs[1].set_ylabel('log(Z/Z$_\odot$)')
    axs[1].scatter(t,ZIGM,s=3,color="b",label="IGM")
    axs[1].scatter(t,Zgas,s=3,color="k",label="ISM")
    axs[1].set_xlim([90,220])
    axs[1].set_ylim([-6,1.75])
    axs[1].legend(loc = "lower right")
    
    for ax in axs:
        ax.tick_params(which = 'both', direction = 'in')
    
    fig.subplots_adjust(hspace=0)
    
    fig.savefig(output_folder+"/t_gas_dust.pdf", bbox_inches='tight')
    fig.clf()

# new z vs mass code
    plt.rcParams["font.size"] = 15.
    plt.figure()
    plt.xlabel('redshift')
    plt.ylabel('$M_\odot$')
    plt.yscale('log')
    plt.xlim(6, 20)
    plt.ylim(1e0, 1e11)
    plt.xticks(np.linspace(6, 22, 9), fontsize=15)
    plt.yticks(np.logspace(0, 11, 12), fontsize=15)
    plt.plot(z, Mhot+Mcold, color='blue', label='gas')
    plt.errorbar(0, 8e9, yerr=2.1e9, capsize=5, markersize=10, ecolor='blue', markeredgecolor = "blue", color='blue')
    plt.plot(z, Mstar_II+Mstar_III, color='orange', label='stars', linestyle='dashdot')
    plt.errorbar(0, 6.3e10, yerr=1.7e10, capsize=5, markersize=10, ecolor='orange', markeredgecolor = "orange", color='orange')
    plt.plot(z, (Mhot+Mcold)*0.02*10.**(Zgas), color='green', label='metals', linestyle='dotted')
    plt.errorbar(0, 8e9*0.02+1.4e8, yerr=2.1e9*0.02+8.3e7, capsize=5, markersize=10, ecolor='green', markeredgecolor = "green", color='green')
    plt.plot(z, Mdust_hot+Mdust_cold, color='red', label='dust', linestyle='dashed')
    plt.errorbar(0, 1.4e8, yerr=8.3e7, capsize=5, markersize=10, ecolor='red', markeredgecolor = "red", color='red')
    plt.grid(linestyle=':')
    plt.legend(loc='upper right', prop={'size': 15})
    plt.savefig(output_folder+"/z_gas_dust.pdf", bbox_inches='tight')


if(__name__ == "__main__"):
    #run the function if this script is called
    plot_tgas_dust(output_folder="output_CHANGE_ME")
