import numpy as np
from scipy import stats

#get p-value from "how many sigma away"
def get_p_from_sigma(sigma):
    '''
    Get p-value from "how many sigma away"
    '''
    half_integral = stats.norm.cdf(-1.0*sigma)
    return 2*half_integral

def LogLikelihood(x_obs,sigma,x_sim):
    #return log likelihood under assumption that uncertainty is normal distributed
    likelihood = (-0.5*((x_obs-x_sim)**2)/(sigma**2)) - np.log (sigma*np.sqrt(2.0*np.pi))
    return likelihood

def Stirling(n):
    #return log_e(n!) based on Stirling's formula
    return np.log(np.sqrt(2.0*np.pi*n)) + n * np.log(n) - 1

class asloth_fit(object):
    """
    class for handling fitting parameters and results of A-SLOTH
    """
    def __init__(self):
        """
        Initializes the fit object and sets default parameters
        """
        self.set_fit_to_default()
        return

    def set_fit_to_default(self):
        """
        sets default parameters for the fit object

        parameters
        ---------
        self : asloth_fit
            set all parameters to default values
        """

        #Goodness-of-fit parameters
        self.p_all = -99.
        self.p_SFR = -99.
        self.p_tau = -99.
        self.p_EMP2All = -99.
        self.p_EMP2UMP = -99.
        self.p_MstarMW = -99.
        self.p_Satellites = -99.

        #deviation from observation as measures in "number of standard deviations away"
        self.tau_sigma = -99.
        self.EMP2All_sigma = -99.
        self.EMP2UMP_sigma = -99.
        self.MW_sigma = -99.

        self.EMP2UMP_Youakim = -99.
        self.EMP2UMP_Chiti = -99.
        self.EMP2UMP_Bonifacio = -99.

        #direct observables predicted by A-SLOTH
        self.tau = -99. #optical depth to Thomson scattering
        self.EMP2All = -99. #log(ratio of EMP stars to all stars)
        self.EMP2UMP = -99. #log(ratio of EMP stars to UMP stars)
        self.Mstar_MW = -99. #stellar mass of the MW
        self.Satellites_ks_mean = -99. #ks distance of CSMF
        self.SFR_max_dist = -99. #distance between observed and siumlated SFRD

        #other useful quantities
        self.SFR_III_max = -99.
        self.SFR_integrated = -99.

        self.sigma_Mstar = -99. #uncertainty of MW stellar mass. Includes observational and statistical uncertainty

        #(Log) Likelihoods
        self.LogL = -99.
        self.LogL_tau = -99.
        self.LogL_SFRD_HST = -99.
        self.LogL_SFRD_JWST = -99.
        self.LogL_MstarMW = -99.
        self.LogL_MassMetallicity = -99.
        self.LogL_sat = -99.
        self.LogL_MDF = -99.
        self.LogL_EMPAll = -99.
        self.LogL_PISN = -99.
        self.NPISN = 0
        self.NEMP = 0

    def Likelihoods(self):
        self.LogL_tau = LogLikelihood(0.054,0.007,self.tau)# optical depth tau = 0.054 +/- 0.007, abstract of Planck18: https://arxiv.org/abs/1807.06209

        self.LogL_MstarMW = LogLikelihood(5.43e10,self.sigma_Mstar,self.Mstar_MW)#  # McMillan+2017: M_star,MW = (5.43 +/- 0.57)1e10 Msun

        self.LogL = self.LogL_tau + self.LogL_MstarMW + self.LogL_SFRD_HST + self.LogL_SFRD_JWST + \
                    self.LogL_MassMetallicity + 2.0*self.LogL_sat + \
                3.0*self.LogL_MDF + self.LogL_EMPAll + self.LogL_PISN


    def getPISN(self,N_PISN_arr,N_EMP_arr):
        #array of number of EMP stars that are mono-nriched by one PISN, nuber of all EMP stars
        self.NPISN = np.mean(N_PISN_arr)
        self.NEMP = np.mean(N_EMP_arr)

        Nobs = 350#observed EMP stars of which we know that they are not mono-enriched

        Nlen = 0.
        LogL = 0.
        for PISN, EMP in zip(N_PISN_arr,N_EMP_arr):
            Nlen += 1.
            #we anyway want LOG likelihood, so no need to np.exp() the entire expression
            LogL += Stirling(EMP - PISN) + Stirling(EMP - Nobs) - Stirling(EMP - PISN - Nobs) - Stirling(EMP)

        self.LogL_PISN = LogL/Nlen


    def print_fit(self):
        ''''
        Prints all class properties
        '''
        print("These are the results of the current model:")
        attrs = vars(self)
        print('\n'.join("%s: %s" % item for item in attrs.items()))


    def get_p(self):
        ''''
        Calculates the goodness-of-fit parameters based on 6 different observables.
        Please see Hartwig+22 for more details.
        '''
        self.p_EMP2All   = get_p_from_sigma(self.EMP2All_sigma)
        self.p_EMP2UMP   = get_p_from_sigma(self.EMP2UMP_sigma)
        self.p_MstarMW   = get_p_from_sigma(self.MW_sigma)
        self.p_Satellites #already set by KS
        self.p_SFR = np.exp(-4.0*self.SFR_max_dist) # approximation that gives the right limits
        self.p_tau = get_p_from_sigma(self.tau_sigma)

        self.p_all = self.p_SFR*self.p_tau*self.p_EMP2All*self.p_EMP2UMP*self.p_MstarMW*self.p_Satellites
        if(self.p_all < 1e-18):
            print("   ")
            print("The final goodness of fit is suspiciously small:",self.p_all)
            print("This can have various reasons:")
            print("1. You are using EPS-based merger trees, which are not suited to reproduce observations.")
            print("   In addition, some observables can not be calculated for EPS-based merger trees.")
            print("2. Your model is not good.")
            print("   ")


class asloth_config(object):
    """
    container class for writing config namelist files for ASLOTH
    """
    def __init__(self):
        """
        Initializes the config file and sets default parameters
        """
        self.config = {}
        self.n_ele = {}
        self.config_metals = {}
        self.set_config_to_default()
        self.set_metals(["C", "Fe"])
        return

    def set_config_to_default(self):
        """
        sets default parameters for the main configuration

        parameters
        ---------
        self : asloth_config
            the configuration to be restored to defaults
        """
        self.config["tree_name"] = "H1079897"
        self.config["tree_path"] = "../trees/"
        self.config["nlev" ] = 312
        self.config["output_string"] = "output"
        self.config["IMF_min"] = 5.0
        self.config["IMF_max"] = 210
        self.config["slope" ] = 1.0
        self.config["ETAIII"] = 0.38
        self.config["ETAII"] = 0.19
        self.config["alpha_outflow"] = 0.86
        self.config["m_cha_out"] = 7500000000.0
        self.config["F_ESCIII"] = 0.6
        self.config["F_ESCII"] = 0.37
        self.config["VBC"] = 0.8
        self.config["c_ZIGM"] = 2.0
        self.config["lw_mode"] = 5
        self.config["cubic_box"] = False
        self.config["RNG_SEED" ] = 161803398
        self.config["nthreads"] = 1
        return

    def set_metals(self, metals):
        """
        sets metal configuration for a specified list of elements

        parameters
        ----------

        self : asloth_config
            configuration

        metals : list
            list of strings of the elements to be traced
        """
        self.n_ele["N_ELEMENTS"] = int(len(metals)+1)
        self.config_metals["ElementName"] = '", "'.join(metals)
        return

    def to_file(self, fname):
        with open(fname, "w+") as f:
            write_namelist(f, "config", self.config)
            write_namelist(f, "n_ele", self.n_ele)
            write_namelist(f, "config_metals", self.config_metals)
        return

def write_namelist(f, list_name, var_dict):
    """
    function to write fortran namelists

    Parameters
    ----------
    f : file
        opened file to write the namelist to
    
    list_name : str
        name of the namelist. Note that this must be the exact name used in the fortran code

    var_dict : dictionary
        dictionary of variables to be written to the file
    """

    f.write("&"+list_name+"\n")
    for k in var_dict.keys():
        if (type(var_dict[k]) == str or type(var_dict[k]) == np.str_):
            f.write(k+' = "'+var_dict[k]+'"\n')
        elif (type(var_dict[k]) == bool):
            f.write(k+' = .'+str(var_dict[k])+'.\n')
        else:
            f.write(k+' = '+str(var_dict[k])+'\n')
    f.write("/\n\n")
    return
