import numpy as np
from matplotlib import pyplot as plt

output_folder = "output_CHANGE_ME"

print("Loading data...")
data = np.genfromtxt(output_folder+"/Tutorial_5.dat",skip_header=1,dtype=float)

plt.scatter(data[:,0],data[:,1],s=1)
plt.xlabel("Redshift")
plt.ylabel("Mstar/Msun")
plt.yscale("log")

plt.savefig(output_folder+"/Tutorial_5.png")
print("Saved plot to",output_folder)
