def plot_MDF(UserFolder,UserLabels,folder_output):
    '''
    Plot Metallicity Distribution Function (MDF) from A-SLOTH and compare with observations.
  
    Parameters
    ----------
        UserFolder: List[str]
            List of folder names to be plotted

        UserLabels: List[str]
            List of labels. If none, UserFolders are used as labels

        folder_output: str
            Folder in which plots will be saved

  
    Returns
    -------
        MyFit: asloth_fit
    '''
    import os.path
    import numpy as np
    import matplotlib.pyplot as plt
    import matplotlib
    from utilities.utility import asloth_fit, LogLikelihood
    from asloth_PlotConf import SetMyLayout
    from scipy.interpolate import interp1d

    SetMyLayout()
    MyFit = asloth_fit()

    colors = list(matplotlib.colors.TABLEAU_COLORS.items())
    i = 0
    EMPfracs = []
    UMPfracs = []
    RatioFracs = []
    N_PISN_list = []
    N_EMP_list = []
    N_plot = len(UserFolder)
    transparancy = np.max([1.0-0.25*(N_plot-1),0.1]) # Modify here if you overplot many (>4) MDFs

    plt.fill_between(np.linspace(-2,1,2),0,100,facecolor='grey',alpha=0.5)

    # Plot settings. These values should be consistent with the output file "MW_MDF.dat". The MDF range and binsize can be modified in MDF.F90
    # Note that the lowest bin center is "MDFZmin - MDFdZ/2" and the highest bin center is "MDFZmax + MDFdZ/2"
    MDFZmin = -9.0
    MDFZmax = 1.0
    MDFdZ = 0.1
    VMP_bins = int((-2.0-MDFZmin)/MDFdZ) # VMP stars are [Fe/H] < -2 stars
    EMP_bins = int((-3.0-MDFZmin)/MDFdZ) # EMP stars are [Fe/H] < -3 stars
    UMP_bins = int((-4.0-MDFZmin)/MDFdZ) # UMP stars are [Fe/H] < -4 stars
   
#    LogL_MDFarray = 0#contains normalised MDFs; all to same FeH grid
 
    # The lowest metallicity to appear in "MW_MDF.pdf".
    MDFZmin_figure = -6.0

    # MW stellar mass estimate and fraction of EMP stars in halo
    MW_StellarMass = 5.4e10 # (5.43 +/- 0.57)*10^{10} Msun, McMillan+17
    MW_StellarMassInHalo = 1e9 # Bullock & Johnston05
    MW_EMPfractionInHalo = 1.0/800 # Youakim+20
    MW_EMPfraction = MW_EMPfractionInHalo * (MW_StellarMassInHalo/MW_StellarMass) # (EMP fraction in MW) = (EMP fraction in halo) * (halo fraction in MW)
    MW_EMPfractionInLog10 = np.log10(MW_EMPfraction)

    MW_UMPfraction_Youakim = MW_EMPfraction * 0.01 # Youakim+20, 1/100 of EMP stars
    MW_UMPfraction_Chiti = MW_EMPfraction * pow(10, -1.5) # Chiti+20, -1.5 dex of EMP stars
    MW_UMPfraction_Bonifacio = MW_EMPfraction * 8.0/217 # Bonifacio+21, 8/217 of EMP stars
    MW_UMPfractionInLog10_Youakim = np.log10(MW_UMPfraction_Youakim)
    MW_UMPfractionInLog10_Chiti = np.log10(MW_UMPfraction_Chiti)
    MW_UMPfractionInLog10_Bonifacio = np.log10(MW_UMPfraction_Bonifacio)
#    print("Adopted EMP fraction in MW: {0:2e}".format(MW_EMPfraction))
#    print("Adopted UMP fraction in MW (Youakim): {0:2e}".format(MW_UMPfraction_Youakim))
#    print("Adopted UMP fraction in MW (Chiti): {0:2e}".format(MW_UMPfraction_Chiti))
#    print("Adopted UMP fraction in MW (Bonifacio): {0:2e}".format(MW_UMPfraction_Bonifacio))

    for folder,label in zip(UserFolder,UserLabels):
        file_input = folder+"/MW_MDF.dat"
        if (os.path.isfile(file_input) and os.stat(file_input).st_size > 0):
#            print("The file "+file_input+" exists.")
            abc = 0
        else:
            print("The file "+file_input+" does not exist or is empty.")
            return MyFit

        data = np.loadtxt(file_input)
        Norm = np.sum(data[:VMP_bins+1, 1]) # To normalize so that the sum is 1 at [Fe/H] < -2

#Load PISN file
        file_input = folder+"/PISN_mono.dat"
        if(not os.path.isfile(file_input)):
            print("WARNING!! The file "+file_input+" does not exist!")
            return MyFit
        N_PISN = np.genfromtxt(file_input,skip_header=1,dtype=int)
        N_PISN_list.append(N_PISN)

        if(i == 0):
            #initialise
            LogL_MDFarray = np.zeros([len(UserFolder),len(data[:,0])])
        LogL_MDFarray[i,:] = np.log10(data[:,1]/Norm+1e-20)

        #A-SLOTH
#        plt.bar(data[:,0], data[:,1]/Norm, width=0.1, zorder=i, label=label, alpha=transparancy)
        if(N_plot > 4):
            plt.plot(data[:,0], data[:,1]/Norm, label=label,c='k',alpha=0.5)
        else:
            plt.plot(data[:,0], data[:,1]/Norm, label=label)

        i = i+1

        #fractions
        EMP = np.sum(data[:EMP_bins+1, 1])
        UMP = np.sum(data[:UMP_bins+1, 1])
        ALL = np.sum(data[:, 1])
        EMPfrac = np.log10(np.fmax(EMP/ALL, 1e-20))
        UMPfrac = np.log10(np.fmax(UMP/ALL, 1e-20))
        EMPfracs.append(EMPfrac)
        UMPfracs.append(UMPfrac)
        RatioFracs.append(EMPfrac-UMPfrac)
        N_EMP_list.append(EMP)

    FeH = data[:,0]#use last, assume all folders have same

    DATA_OBS_FOLDER = "Data/MDF" # Specify folder in which Obs_halo.csv exists

    # Add Youakim+20 MDF
    data_obs = np.loadtxt(DATA_OBS_FOLDER+"/Obs_halo.csv", delimiter=",", skiprows=1)
    plt.scatter(data_obs[:,0], data_obs[:,3]/27757, s=4, zorder=i+1, label="Youakim+20", color="cyan") # 27757 is the sum of values at [Fe/H] < -2 in Obs_halo.csv      
    FeH_Youakim = data_obs[:,0]
    MDF_Youakim = np.log10(data_obs[:,3]/27757+1e-20)

    # Add Bonifacio+21 MDF
    data_obs_TOPoS = np.loadtxt(DATA_OBS_FOLDER+"/Obs_halo_TOPoS.csv", delimiter=",", skiprows=1)
    plt.scatter(data_obs_TOPoS[:,0], data_obs_TOPoS[:,2]/(6363/2), s=4, zorder=i+1, label="Bonifacio+21", color="purple") # 6363 is the sum of values at [Fe/H] < -2 in Obs_halo_TOPoS.csv      
    FeH_Bonifacio = data_obs_TOPoS[:,0]
    MDF_Bonifacio = np.log10(data_obs_TOPoS[:,2]/(6363/2)+1e-20)

    # Add Chiti+21 MDF: dlogN/d[Fe/H] = 1.5 for [Fe/H] < -2.3 and 1.1 for -2.3 < [Fe/H] < -2
    C = 1/(pow(10, -3.45)/(1.5*np.log(10)) + (pow(10, -2.2)-pow(10, -2.53))*pow(10, -0.92)/(1.1*np.log(10)))
    Xbin1 = np.arange(MDFZmin, -3, 0.1)
    Xbin2 = np.arange(-3, -2.3, 0.1)
    Xbin3 = np.arange(-2.3, -1.0, 0.1)
    Ybin1 = C*pow(10, 1.5*Xbin1)*MDFdZ
    Ybin2 = C*pow(10, 1.5*Xbin2)*MDFdZ
    Ybin3 = C*pow(10, -0.92)*pow(10, 1.1*Xbin3)*MDFdZ
    plt.plot(Xbin1, Ybin1, color="red", linestyle="dashed")
    plt.plot(Xbin2, Ybin2, color="red", linestyle="solid", label="Chiti+21")
    plt.plot(Xbin3, Ybin3, color="red", linestyle="solid")
    #for a,b in zip(Xbin1,Ybin1):
    #    print(a,b)
    #extracted manually because of piecewise power-law
    FeH_Chiti = np.array([-4.9,-4.8,-4.7,-4.6,-4.5,-4.4,-4.3,-4.3,-4.2,-4.1,-4.0,
        -3.9,-3.8,-3.7,-3.6,-3.5,-3.4,-3.3,-3.3,-3.2,-3.1,-3.0,
        -2.9,-2.8,-2.7,-2.6,-2.5,-2.4,-2.3,-2.3,-2.2,-2.1,-2.0])
    MDF_Chiti = np.log10(np.array([1.704E-05,2.407E-05,3.400E-05,4.802E-05,6.784E-05,9.582E-05,1.354E-04,1.912E-04,2.701E-04,3.815E-04,
        5.388E-04,7.611E-04,1.075E-03,1.519E-03,2.145E-03,3.030E-03,4.280E-03,6.046E-03,8.540E-03,0.0121,
        0.0170,0.0241,0.0340,0.0480,0.0678,0.0958,0.1354,0.1354,0.1744,0.2246,0.2894]))


    # Labels                                                                                                                           
    plt.xscale("linear")
    plt.yscale("log")
    plt.xlim(MDFZmin_figure, MDFZmax)
    plt.ylim(1e-6, 1e2)
    plt.xlabel("[Fe/H]")
    plt.ylabel("Normalized number of stars")
    plt.legend()


    EMP_mean = np.mean(EMPfracs)
    EMP_stddev = np.std(EMPfracs)
    #print("EMP,sigma30:",EMP_stddev)
    #Chen+22 finds EMP/All almost one order of magnitude above our observation-based estimate
    #Therefore, we use a rather large uncertainty
    EMP_Obserr = np.sqrt(1.0**2+EMP_stddev**2) # icludes estimate of stellar mass in the MW halo, in log10. But other errors are also difficult to estimate.
    UMP_mean = np.mean(UMPfracs)
    UMP_stddev = np.std(UMPfracs)
    UMP_Obserr = 0.5
    
    #Ratio of EMP to UMP stars
    Ratio_mean = np.mean(RatioFracs)
    Ratio_stddev = np.std(RatioFracs)
    Ratio_Obserr = 0.3 # Youakim+20 (-2.0), Chiti+21 (-1.5), Bonifacio+21 (-1.4) estimates have the standard deviation of 0.3dex

    if (EMP_stddev > 0):
        EMP_sigma = abs(EMP_mean - MW_EMPfractionInLog10)/EMP_Obserr
    else:
        #print("!!! Caution !!! EMP_stddev is 0. We only calculate mean EMP fraction.\n")
        EMP_sigma = 9.99
    if (UMP_stddev > 0):
        # We calculate three UMP sigmas using three different observations: Youakim+20, Chiti+21, and Bonifacio+21.
        UMP_sigma_Youakim = abs(UMP_mean - MW_UMPfractionInLog10_Youakim)/np.sqrt(UMP_stddev*UMP_stddev + UMP_Obserr*UMP_Obserr)
        UMP_sigma_Chiti = abs(UMP_mean - MW_UMPfractionInLog10_Chiti)/np.sqrt(UMP_stddev*UMP_stddev + UMP_Obserr*UMP_Obserr)
        UMP_sigma_Bonifacio = abs(UMP_mean - MW_UMPfractionInLog10_Bonifacio)/np.sqrt(UMP_stddev*UMP_stddev + UMP_Obserr*UMP_Obserr)
    else:
        #print("!!! Caution !!! UMP_stddev is 0. We only calculate mean UMP fraction.\n")
        UMP_sigma_Youakim = 9.99
        UMP_sigma_Chiti = 9.99
        UMP_sigma_Bonifacio = 9.99

    if (Ratio_stddev > 0):
        #Literature ratios of EMP to UMP stars (log)
        R_Youakim = 2.0
        R_Chiti = 1.5
        R_Bonifacio = np.log10(217./8.)
        Ratio_sigma_Youakim = abs(Ratio_mean - R_Youakim)/np.sqrt(Ratio_stddev*Ratio_stddev + Ratio_Obserr*Ratio_Obserr)
        Ratio_sigma_Chiti = abs(Ratio_mean - R_Chiti)/np.sqrt(Ratio_stddev*Ratio_stddev + Ratio_Obserr*Ratio_Obserr)
        Ratio_sigma_Bonifacio = abs(Ratio_mean - R_Bonifacio)/np.sqrt(Ratio_stddev*Ratio_stddev + Ratio_Obserr*Ratio_Obserr)
        #set sigma to zero if results is between the three literature values
        UMP_min = np.min([R_Youakim,R_Chiti,R_Bonifacio])
        UMP_max = np.max([R_Youakim,R_Chiti,R_Bonifacio])
#        print("UMP mean/min/max:",Ratio_mean,UMP_min,UMP_max)
        if(Ratio_mean > UMP_min and Ratio_mean < UMP_max):
            Ratio_sigma_Youakim = 0
            Ratio_sigma_Chiti = 0
            Ratio_sigma_Bonifacio = 0

#        print("Ratio_sigma (Youakim): {0}".format(Ratio_sigma_Youakim))
#        print("Ratio_sigma (Chiti): {0}".format(Ratio_sigma_Chiti))
#        print("Ratio_sigma (Bonifacio): {0}".format(Ratio_sigma_Bonifacio))
    else:
        #print("!!! Caution !!! Ratio_stddev is 0. Provisionally we set Ratio_sigmas = 9.99.\n")
        Ratio_sigma_Youakim = 9.99
        Ratio_sigma_Chiti = 9.99
        Ratio_sigma_Bonifacio = 9.99
        
    # Position of EMPfrac and UMPfrac on MW_MDF.pdf in data coordinate
    Annotate_X = MDFZmin_figure+2*MDFdZ
    Annotate_EMP_Y = pow(10, 1.5)
    Annotate_UMP_Y = pow(10, 1.0)
    Annotate_UMPsigma_Y = pow(10, 0.5)
    
    if False:
        if (EMP_sigma == 9.99):
            plt.annotate("log$_{10}$"+"(EMPfrac): {0:.2f}".format(EMP_mean), [Annotate_X, Annotate_EMP_Y])
        else:
            plt.annotate("log$_{10}$"+"(EMPfrac): {0:.2f} $\pm$ {1:.2f}, {2:.1f} $\sigma$".format(EMP_mean, EMP_stddev, EMP_sigma), [Annotate_X, Annotate_EMP_Y])

        if (UMP_sigma_Youakim == 9.99):
            plt.annotate("log$_{10}$"+"(UMPfrac): {0:.2f}".format(UMP_mean), [Annotate_X, Annotate_UMP_Y])
        else:
            plt.annotate("log$_{10}$"+"(UMPfrac): {0:.2f} $\pm$ {1:.2f}".format(UMP_mean, UMP_stddev), [Annotate_X, Annotate_UMP_Y])
            plt.annotate("{0:.1f} $\sigma$ (Y20), {1:.1f} $\sigma$ (C21), {2:.1f} $\sigma$ (B21)".format(UMP_sigma_Youakim, UMP_sigma_Chiti, UMP_sigma_Bonifacio), [Annotate_X, Annotate_UMPsigma_Y])

    # savefig.                                                                                                                          
    plt.savefig(folder_output+"MW_MDF.pdf", bbox_inches="tight")
    plt.close()


    #cap at some max value
    EMP_sigma = np.min([19.99,EMP_sigma])
    UMP_sigma_Youakim = np.min([19.99,UMP_sigma_Youakim])
    UMP_sigma_Chiti = np.min([19.99,UMP_sigma_Chiti])
    UMP_sigma_Bonifacio = np.min([19.99,UMP_sigma_Bonifacio])
    Ratio_sigma_Youakim = np.min([19.99,Ratio_sigma_Youakim])
    Ratio_sigma_Chiti = np.min([19.99,Ratio_sigma_Chiti])
    Ratio_sigma_Bonifacio = np.min([19.99,Ratio_sigma_Bonifacio])

    if (len(EMPfracs)<2 or len(UMPfracs)<2 or len(RatioFracs)<2):
        print("The MDF lists are empty (normal if just one tree is used).")
    else:
        MyFit.EMP2All_sigma = EMP_sigma
        MyFit.EMP2UMP_Youakim = Ratio_sigma_Youakim
        MyFit.EMP2UMP_Chiti = Ratio_sigma_Chiti
        MyFit.EMP2UMP_Bonifacio = Ratio_sigma_Bonifacio
        MyFit.EMP2All = EMP_mean
        MyFit.EMP2UMP = Ratio_mean


#Calculate LogLikelihood
    FeH_min = -4.9
    FeH_max = -2.0#reliable ranges
    MDF_mean = np.zeros(len(FeH))
    MDF_std = np.zeros(len(FeH))
    for j,x in enumerate(FeH):
        MDF_mean[j] = np.mean(LogL_MDFarray[:,j])
        if(len(LogL_MDFarray[:,j])>1):
            MDF_std[j] = np.std(LogL_MDFarray[:,j])
        else:
            MDF_std[:] = 0.5#dex

#functions that will return mean MDS values and their scatter over merger tree realisations
    f_mean = interp1d(FeH, MDF_mean, bounds_error=True)
    f_std = interp1d(FeH, MDF_std, bounds_error=True)
    #print("MDF,sigma30:")
    #for a in FeH:
    #    print(a,f_std(a))

    LogL = 0.
    Nobs = 0
    for FeH_obs, MDF_obs, FeH_min in zip([FeH_Youakim,FeH_Bonifacio,FeH_Chiti],[MDF_Youakim,MDF_Bonifacio,MDF_Chiti],[-3.9,-3.9,-4.9]):
        for FeH, MDF in zip(FeH_obs,MDF_obs):
            if(FeH >= FeH_min and FeH <= FeH_max):
                LogL += LogLikelihood(MDF,f_std(FeH),f_mean(FeH))
                Nobs += 1
    MyFit.LogL_MDF = LogL/float(Nobs)#divide by number of data points

#fraction of EMP stars to all stars
    MyFit.LogL_EMPAll = LogLikelihood(MW_EMPfractionInLog10,EMP_Obserr,EMP_mean)

#Non-detection of mono-enrichment by PISN
    MyFit.getPISN(N_PISN_list,N_EMP_list)

    return MyFit
