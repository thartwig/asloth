import glob

def readMWproperties(UserFolder):
  import os
  import pandas as pd

  columns = ['mhalo','Mstar_total',\
  'Mstar','Mhot', 'Mcold', 'num_PopIIProgenitor']

  MW_star = []
  for folder in UserFolder:
    file_input = folder+"/MW_properties.dat"
    if os.path.isfile(file_input):
      print("The file "+file_input+" exists.")
      df = pd.read_csv(file_input, delim_whitespace=True, skiprows=2, header=None)
      df.columns = columns 
      MW_star.append(df['Mstar'][0])
    else:
      print (file_input+" not found.")
    
  return MW_star

def plot_comparison(UserFolder, UserLabels=None):
    """
    Function to plot results from A-SLOTH and compare them to literature or other runs.
  
    We use the most recent folder to plot and compare to literature.
    If one or more folders are specified, they will all be plotted.
    This can also be used to plot just one specific folder.
 
 
    Parameters
    ----------

        UserFolder: list[str]
            List of folder names to be plotted.

        UserLabels: list[str]
            List of labels. If none, UserFolders are used as labels

  
    Returns: Beautiful plots

  
    """

    import sys
    if(sys.version_info.major < 3):
        print("WARNING: we strongly recommend to run this script with python3")
        print("You are using python verion ",sys.version_info.major)

    import os.path
    import matplotlib
    import numpy as np
    from utilities.utility import asloth_fit
    from tau.plot_Vion import plot_Vion
    from SFR.plot_SFR import plot_SFR
    from plot_Vmetal import plot_Vmetal
    from plot_MDF import plot_MDF
    from satellites.plot_MassMetallicity import plot_MassMetallicity
    from SMHM.plot_scatterSMHM import plot_CumuSMF, plot_scatterSMHM
    from SMHM.plot_binnedSMHM import plot_binnedSMHM
    from SFR.compare_SFR import compare_SFR

    matplotlib.use('Agg')#optional for plotting on remote machine

    if(UserLabels == None or len(UserLabels) != len(UserFolder)):
        # Use folder names also as labels
        UserLabels = UserFolder.copy()

    Nlabel = len(UserLabels)
    if(Nlabel > 4):
        #use only one label, set others to empty
        #otherwise legend too crowded
        UserLabels = ['A-SLOTH']
        for i in range(Nlabel-1):
            UserLabels.append("")

    print("We will plot and compare the following folders with the corresponding labels:")
    print(UserFolder)
    print(UserLabels)

    Nplot = len(UserFolder)
    print("We will plot and compare outputs from "+str(Nplot)+" different runs.")

    if(os.path.isdir(UserFolder[0])):
        print("The directory "+UserFolder[0]+" exists and we will use it to create plots.")
        # create folder for newly created plots
        folder_output = UserFolder[0]+"/plots/"
        if not os.path.exists(folder_output):
            print("Plots will be saved in "+folder_output)
            os.makedirs(folder_output)
    else:
        print("ERROR: "+UserFolder[0]+" does not exists or is not a directory.")


    MyFit = asloth_fit()

# MDF
    MDF_fit = plot_MDF(UserFolder, UserLabels, folder_output)
    MDF_fit.EMP2UMP_sigma = np.amin([MDF_fit.EMP2UMP_Youakim,MDF_fit.EMP2UMP_Chiti,MDF_fit.EMP2UMP_Bonifacio])
    MyFit = MDF_fit

#Mass-metallicity relation
#    MyFit.LogL_MassMetallicity, MyFit.LogL_sat = plot_MassMetallicity(UserFolder, UserLabels, folder_output)

# SFR
    MyFit.SFR_integrated, MyFit.LogL_SFRD_HST, MyFit.LogL_SFRD_JWST = plot_SFR(UserFolder, UserLabels, folder_output)
    MyFit.SFR_max_dist, MyFit.SFR_III_max = compare_SFR(folder=UserFolder[0])#only first folder, no comparison

# Vion
    MyFit.tau_sigma, MyFit.tau = plot_Vion(UserFolder, UserLabels, folder_output)

# Vmetal
    plot_Vmetal(UserFolder, UserLabels, folder_output)


# SMHM comparison: it compares individual SMHM relations (binned) from UserFolder
# Does not produce output for EPS mode, because we can not follow satellites without spatial information
#    plot_binnedSMHM(UserFolder, UserLabels, folder_output)
#    plot_scatterSMHM(UserFolder, UserLabels, folder_output)

# CumuSMF
    ks_list, p_list, Mstar_list = plot_CumuSMF(UserFolder, UserLabels, folder_output, True)
    if(np.sum(Mstar_list) < 0):
        #A-SLOTH did probably not output satellite information.
        #Hence, we need to get the MW stellar mass separately:
        Mstar_list = readMWproperties(UserFolder)
        print("Mstar_list",Mstar_list)

# Calculate fit quality based on stellar masses of MW and satellites
    MyFit.p_Satellites = np.mean(p_list)
    MyFit.Satellites_ks_mean = np.mean(ks_list)
    Mstar_mean = np.mean(Mstar_list)
    Mstar_std = np.std(Mstar_list)
    # McMillan+2017: M_star,MW = (5.43 +/- 0.57)1e10 Msun
    # https://ui.adsabs.harvard.edu/abs/2015ApJ...806...96L/abstract
    Mdiff = np.abs(Mstar_mean-5.43e10)
    #print("MW stellar mass, sigma30:",Mstar_std)
    sigma_M = np.sqrt(0.57e10**2+Mstar_std**2)

    MyFit.MW_sigma = Mdiff/sigma_M
    MyFit.sigma_Mstar = sigma_M
    MyFit.Mstar_MW = Mstar_mean

    MyFit.Likelihoods()


# Return parameters to quantify fit. Not relevant if we only want to use this script for plotting
    return MyFit


if __name__ == '__main__':
    #if program is called directly
    import sys
    if(sys.version_info.major < 3):
        print("WARNING: we strongly recommend to run this script with python3")
        print("You are using python verion ",sys.version_info.major)

    #manually define folders that should be plotted
    UserFolder = ["Data/EPS_MW_SEED0","Data/EPS_MW_SEED1"]  # Can be N>=1 folder(s)
    UserLabels = ["SEED0","SEED1"]

    plot_newest_folder = True
    if(plot_newest_folder):
        #find latest folder
        folder_list = sorted(glob.glob("output_*"))
        #add newest folder
        UserFolder.insert(0,folder_list[-1])
        UserLabels.insert(0,"last run")

    MyFit = plot_comparison(UserFolder, UserLabels=UserLabels)

    #Goodness-of-fit parameters
    MyFit.get_p()

    MyFit.print_fit()
