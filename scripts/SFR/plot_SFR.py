#HST based on Bouwens+16 and Oesch+18
#Format: [redshift, log10(SFRD), sigma_SFR[dex]]
SFRD_HST = [
[4.9,-1.387,0.124],
[5.9,-1.640,0.130],
[6.8,-1.883,0.076],
[7.9,-2.213,0.065],
[10.2,-3.287,0.162]]

#JWST based on Bouwens+22, Donnan+22,Harikane+22
SFRD_JWST = [
[8.7,-3.082,0.238],
[10.5,-3.507,0.295],
[12.6,-3.089,0.368],
[8.0,-2.700,0.051],
[9.0,-2.945,0.108],
[10.5,-3.262,0.151],
[13.25,-4.084,0.151],
[9.0,-3.147,0.137],
[12.0,-3.839,0.260]]

def plot_SFR(UserFolder, UserLabels, folder_output):
    '''
    Plot star formation rate densities from A-SLOTH with observations
  
    Parameters
    ----------
        UserFolder: List[str]
            List of folder names to be plotted

        UserLabels: List[str]
            List of labels. If none, UserFolders are used as labels

        folder_output: str
            Folder in which plots will be saved

  
    Returns
    -------
        SFR_integrated: float
            in Msun/Mpc**3
    '''

    import os.path
    import numpy as np
    import matplotlib
    import matplotlib.pyplot as plt
    from asloth_PlotConf import SetMyLayout
    from SFR.SFR_integral import SFR_integral

    # overwrite size for 2-panel figure
    fig = SetMyLayout(col=1, ratio=2.0)

    xmin = 0
    xmax = 32
    ymin = 1e-6
    ymax = 0.1
    unit = "$\mathrm{M}_\odot\,\mathrm{yr}^{-1}\,\mathrm{Mpc}^{-3}$"

# PopIII
    plt.subplot(3, 1, 1)
    plt.ylabel(unit)
    plt.yscale("log")
    plt.ylim((ymin, ymax))
    plt.xlim((xmin, xmax))
    plt.annotate("PopIII SFRD", (0.7*xmax, ymax*0.1))
    plt.tick_params(
      axis='x',           # changes apply to the x-axis
      labelbottom=False)  # labels along the bottom edge are off

    for i in range(len(UserLabels)):
        if(len(UserLabels[i])>16):
            UserLabels[i] = UserLabels[i][-16:]#use only last characters if strong is too long. Otherwise strange format

    for folder, label in zip(UserFolder, UserLabels):
        SFR_file = folder+"/z_cSFR.dat"
        if not os.path.isfile(SFR_file):
            print("The file "+SFR_file+" does not exist.")
            return -99.

        data = np.genfromtxt(SFR_file, skip_header=2)

        # A-SLOTH
        z = data[:,0]
        cSFRII = data[:,3]  # PopII cosmic star formation rate density (Msun /yr /cMpc^3)
        cSFRIII = data[:,2]  # PopIII cosmic star formation rate density (Msun /yr /cMpc^3)

        plt.plot(z, cSFRIII, label=label+", PopIII", linewidth=3)

        SFR_integrated = SFR_integral(z, cSFRIII)
        if(False):#option to print additional vlaues from literature
            print("Cosmic PopIII stellar density from A-SLOTH:")
            print(SFR_integrated,"Msun/Mpc^3")
            print("Cosmic PopIII stellar densities from literature:")
            print("S18:    289406 Msun/Mpc^3")
            print("J18:    217031 Msun/Mpc^3")
            print("dS14:   130310 Msun/Mpc^3")
            print("J13_III: 49613 Msun/Mpc^3")
            print("V20III:  38262 Msun/Mpc^3")
            print("T09III:   1913 Msun/Mpc^3")
            print("M18III:   1796 Msun/Mpc^3")


    # Pop II
    data_B15 = np.genfromtxt('Data/SFR/Behroozi15.dat')
    data_B19 = np.genfromtxt('Data/SFR/Behroozi19.dat')
    data_M14 = np.genfromtxt('Data/SFR/Madau14.dat')
    data_S18II = np.genfromtxt('Data/SFR/Sarmento19_PopII.dat') #https://ui.adsabs.harvard.edu/abs/2019ApJ...871..206S/abstract
    data_J13_II = np.genfromtxt('Data/SFR/Johnson13_PopII.dat') #https://arxiv.org/abs/1206.5824
    data_F16 = np.genfromtxt('Data/SFR/Finkelstein16.dat') #https://arxiv.org/abs/1511.05558
    data_T09II = np.genfromtxt('Data/SFR/Trenti09_PopII.dat') #https://arxiv.org/abs/0901.0711
    data_Xu16 = np.genfromtxt('Data/SFR/Xu16_tot.dat') #https://arxiv.org/abs/1604.07842 (Renaissance)
    data_M18II = np.genfromtxt('Data/SFR/Mebane18_PopII.dat') #https://arxiv.org/abs/1710.02528
    data_V20II = np.genfromtxt('Data/SFR/Visbal20_PopII.dat') #https://arxiv.org/abs/2001.11118

    # PopIII
    data_S18III = np.genfromtxt('Data/SFR/Sarmento19_PopIII.dat') #https://ui.adsabs.harvard.edu/abs/2019ApJ...871..206S/abstract
#    print("S18:",SFR_integral(data_S18III[:,0],data_S18III[:,1]),"Msun/Mpc^3")
    data_J13_III = np.genfromtxt('Data/SFR/Johnson13_PopIII.dat')#https://arxiv.org/abs/1206.5824
#    print("J13_III:",SFR_integral(data_J13_III[:,0],data_J13_III[:,1]),"Msun/Mpc^3")
    data_J18 = np.genfromtxt('Data/SFR/Jaacks18.dat')#https://arxiv.org/abs/1705.08059
#    print("J18:",SFR_integral(data_J18[:,0],data_J18[:,1]),"Msun/Mpc^3")
    data_dS14 = np.genfromtxt('Data/SFR/deSouza14.dat') #https://arxiv.org/abs/1401.2995 (average between min and max)
#    print("dS14:",SFR_integral(data_dS14[:,0],data_dS14[:,1]),"Msun/Mpc^3")
    data_T09III = np.genfromtxt('Data/SFR/Trenti09_PopIII.dat') #https://arxiv.org/abs/0901.0711
#    print("T09III:",SFR_integral(data_T09III[:,0],data_T09III[:,1]),"Msun/Mpc^3")
    data_M18III = np.genfromtxt('Data/SFR/Mebane18_PopIII.dat') #https://arxiv.org/abs/1710.02528
#    print("M18III:",SFR_integral(data_M18III[:,0],data_M18III[:,1]),"Msun/Mpc^3")
    data_V20III = np.genfromtxt('Data/SFR/Visbal20_PopIII.dat')#https://arxiv.org/abs/2001.11118
#    print("V20III:",SFR_integral(data_V20III[:,0],data_V20III[:,1]),"Msun/Mpc^3")
    data_S20III = np.genfromtxt('Data/SFR/Skinner20.dat')

### POP III ###
    plt.plot(data_S18III[:,0], data_S18III[:,1], label="Sarmento+19", linestyle='dashed')
    plt.plot(data_J13_III[:,0], data_J13_III[:,1], label="Johnson+13", linestyle='dashed')
    plt.plot(data_J18[:,0], data_J18[:,1], label="Jaacks+18", linestyle='dashed')
    plt.plot(data_dS14[:,0], data_dS14[:,1], label="deSouza+14", linestyle='dashed')
    plt.plot(data_T09III[:,0], data_T09III[:,1], label="Trenti+09", linestyle='dashed')
    plt.plot(data_M18III[:,0], data_M18III[:,1], label="Mebane+18", linestyle='dashed')
    plt.plot(data_V20III[:,0], data_V20III[:,1], label="Visbal+20", linestyle='dashed')
    plt.plot(data_S20III[:,0], data_S20III[:,1], label="Skinner+20", linestyle='dashed')


    if(len(UserLabels)<4):
        plt.legend(loc='lower right', bbox_to_anchor=(1.0, 1.05), ncol=3, fancybox=True)

    ### POP II ###
    plt.subplot(3, 1, 2)
    plt.ylabel(unit)
    plt.yscale("log")
    plt.ylim((ymin, ymax))
    plt.xlim((xmin, xmax))
    plt.annotate("PopII SFRD", (0.7*xmax, ymax*0.1))
    plt.tick_params(axis='x',labelbottom=False)

    for folder, label in zip(UserFolder, UserLabels):
        SFR_file = folder+"/z_cSFR.dat"
        if not os.path.isfile(SFR_file):
            print("The file "+SFR_file+" does not exist.")
            return -99.

        data = np.genfromtxt(SFR_file, skip_header=2)

        # A-SLOTH
        z = data[:,0]
        cSFRII = data[:,3] #  PopII cosmic star formation rate density (Msun /yr /cMpc^3)
        cSFRIII = data[:,2] #  PopIII cosmic star formation rate density (Msun /yr /cMpc^3)
        plt.plot(z, cSFRII, label=label+", PopII", linewidth=3)

    plt.plot(data_S18II[:,0], data_S18II[:,1], label="Sarmento+19", linestyle='dashed')
    plt.plot(data_J13_II[:,0], data_J13_II[:,1], label="Johnson+13", linestyle='dashed')
    plt.plot(data_T09II[:,0], data_T09II[:,1], label="Trenti+09", linestyle='dashed')
    plt.plot(data_Xu16[:,0], data_Xu16[:,1], label="Xu+16", linestyle='dashed')
    plt.plot(data_M18II[:,0], data_M18II[:,1], label="Mebane+18", linestyle='dashed')
    plt.plot(data_V20II[:,0], data_V20II[:,1], label="Visbal+20", linestyle='dashed')

    if(len(UserLabels)<4):
        plt.legend(loc='lower right', bbox_to_anchor=(1.0, 1.05), ncol=3, fancybox=True)



    ### TOTAL ###
    plt.subplot(3, 1, 3)
    plt.xlabel('redshift')
    plt.ylabel(unit)
    plt.yscale("log")
    plt.ylim((ymin, ymax))
    plt.xlim((xmin, xmax))
    plt.annotate("Total SFRD", (0.7*xmax, ymax*0.1))


    for folder, label in zip(UserFolder, UserLabels):
        SFR_file = folder+"/z_cSFR.dat"
        if not os.path.isfile(SFR_file):
            print("The file "+SFR_file+" does not exist.")
            return -99.

        data = np.genfromtxt(SFR_file, skip_header=2)

        # A-SLOTH
        z = data[:,0]
        cSFRII = data[:,3] #  PopII cosmic star formation rate density (Msun /yr /cMpc^3)
        cSFRIII = data[:,2] #  PopIII cosmic star formation rate density (Msun /yr /cMpc^3)
        plt.plot(z, cSFRII+cSFRIII, linewidth=3, label=label+", total")

    #calculate log likelihoods
    #here, because otherwise z is overwritten
    LL_HST = LL_SFRD(SFRD_HST, z, cSFRII+cSFRIII)
    LL_JWST = LL_SFRD(SFRD_JWST, z, cSFRII+cSFRIII)

    plt.plot(data_F16[:,0], data_F16[:,1], label="Finkelstein16", linestyle=':')
    plt.plot(data_B15[:,0], data_B15[:,1], label="Behroozi+15", linestyle=':')
    plt.plot(data_B19[:,0], data_B19[:,1], label="Behroozi+19", linestyle=':')
    plt.plot(data_M14[:,0], data_M14[:,1], label="Madau&Dickinson14", linestyle=':')

#Plot observed data points
    for telescope, name, c in zip([SFRD_HST,SFRD_JWST],["HST","JWST"],["k","grey"]):
        for i,data_now in enumerate(telescope):
            z = data_now[0]
            SFRD = data_now[1]
            sigma_dex = data_now[2]
            err_min = 10.0**(SFRD - sigma_dex)
            err_max = 10.0**(SFRD + sigma_dex)
            SFRD = 10.0**SFRD
            if(i==0):
                plt.errorbar(z,SFRD,yerr=[[SFRD-err_min],[err_max-SFRD]],color=c,zorder=99,marker="x",label=name)
            else:
                plt.errorbar(z,SFRD,yerr=[[SFRD-err_min],[err_max-SFRD]],color=c,zorder=99,marker="x")

    if(len(UserLabels)<4):
        plt.legend(loc='lower right', bbox_to_anchor=(1.0, 1.05), ncol=3, fancybox=True)



    plt.tight_layout()
    plt.savefig(folder_output+"z_SFRD.pdf", bbox_inches="tight")
    plt.clf()

    return SFR_integrated, LL_HST, LL_JWST


def LL_SFRD(SFRD_obs, z_sim, SFRD_sim):
    import numpy as np
    from utilities.utility import LogLikelihood#(x_obs,sigma,x_sim)
    from scipy.interpolate import interp1d

    #operate in log-space
    SFRD_sim = np.log10(SFRD_sim+1e-12)#add tiny offset to avoid -inf
    f = interp1d(z_sim, SFRD_sim, bounds_error=True)

    LogL = 0.
    Nobs = len(SFRD_obs)
    for data_now in SFRD_obs:
        z = data_now[0]
        SFRD = data_now[1]
        sigma_dex = data_now[2]
        LogL += LogLikelihood(SFRD,sigma_dex,f(z))

    return LogL/float(Nobs)#divide by number of data points
