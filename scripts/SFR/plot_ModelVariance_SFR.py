'''
Plot star formation rate densities from all accepted MCMC runs
'''

import sys
sys.path.insert(0, 'scripts') # needed to import SetMyLayout

import glob
import os
import numpy as np
import matplotlib.pyplot as plt
from asloth_PlotConf import SetMyLayout

pop = 2

# overwrite size for 2-panel figure
fig = SetMyLayout(col=1, ratio=1.0)

unit = "$\mathrm{M}_\odot\,\mathrm{yr}^{-1}\,\mathrm{Mpc}^{-3}$"

# PopIII
plt.ylabel(unit)
plt.xlabel("redshift")
plt.yscale("log")
plt.ylim((1e-8, 3e-3))
#    plt.xlim((xmin, xmax))

if(pop == 3):
    plt.title("PopIII SFRD")
if(pop == 2):
    plt.title("PopII SFRD")

Nz = 77#number of redshift bins
Ntot = 16000#large number

SFRD_array = np.zeros([Ntot,77])

count = 0
for cat in ["4","5","3"]:
    for chain in ["1","2"]:
        print(cat, chain)
        MCMC_file = "MCMC_chain"+chain+"_cat"+cat+"_accepted.dat"
        MCMC_data = np.genfromtxt(MCMC_file,skip_header=1,dtype=float)
        if_accepted = MCMC_data[:,1]
        for i, i_acc in enumerate(if_accepted):
            if i_acc:
                #use this accepted run
                folder = "/mnt/data_cat"+cat+"/thartwig/MCMC_chain"+chain+"/chain"+chain+"_output_para"+str(i)+"_T8_*"
                SFR_list = glob.glob(folder)
                print(i,"Use this folder:",SFR_list)

                SFR_file = SFR_list[0]+"/z_cSFR.dat"
                if not os.path.isfile(SFR_file):
                    print("File does not exist:",SFR_file)
                    continue

                data = np.genfromtxt(SFR_file, skip_header=2)

                z = data[:,0]
                print(np.shape(z))
                if(pop == 2):
                    cSFR = data[:,3]  # PopII cosmic star formation rate density (Msun /yr /cMpc^3)
                if(pop == 3):
                    cSFR = data[:,2]  # PopIII cosmic star formation rate density (Msun /yr /cMpc^3)
                SFRD_array[count,:] = cSFR

                plt.plot(z, cSFR, linewidth=1, color="k", alpha=0.1)
                count += 1
                if(count == Ntot):
                    print("!!! WARNING !!! Increase Ntot.")


print("Count:",count)

np.save("SFRD_redshift_pop"+str(pop),z)
np.save("SFRD_arrays"+str(pop),SFRD_array[:count,:])


if False:
    # PopIII
    data_S18III = np.genfromtxt('Data/SFR/Sarmento19_PopIII.dat') #https://ui.adsabs.harvard.edu/abs/2019ApJ...871..206S/abstract
    data_J13_III = np.genfromtxt('Data/SFR/Johnson13_PopIII.dat')#https://arxiv.org/abs/1206.5824
    data_J18 = np.genfromtxt('Data/SFR/Jaacks18.dat')#https://arxiv.org/abs/1705.08059
    data_dS14 = np.genfromtxt('Data/SFR/deSouza14.dat') #https://arxiv.org/abs/1401.2995 (average between min and max)
    data_T09III = np.genfromtxt('Data/SFR/Trenti09_PopIII.dat') #https://arxiv.org/abs/0901.0711
    data_M18III = np.genfromtxt('Data/SFR/Mebane18_PopIII.dat') #https://arxiv.org/abs/1710.02528
    data_V20III = np.genfromtxt('Data/SFR/Visbal20_PopIII.dat')#https://arxiv.org/abs/2001.11118
    data_S20III = np.genfromtxt('Data/SFR/Skinner20.dat')

### POP III ###
    plt.plot(data_S18III[:,0], data_S18III[:,1], label="Sarmento+19", linestyle='dashed')
    plt.plot(data_J13_III[:,0], data_J13_III[:,1], label="Johnson+13", linestyle='dashed')
    plt.plot(data_J18[:,0], data_J18[:,1], label="Jaacks+18", linestyle='dashed')
    plt.plot(data_dS14[:,0], data_dS14[:,1], label="deSouza+14", linestyle='dashed')
    plt.plot(data_T09III[:,0], data_T09III[:,1], label="Trenti+09", linestyle='dashed')
    plt.plot(data_M18III[:,0], data_M18III[:,1], label="Mebane+18", linestyle='dashed')
    plt.plot(data_V20III[:,0], data_V20III[:,1], label="Visbal+20", linestyle='dashed')
    plt.plot(data_S20III[:,0], data_S20III[:,1], label="Skinner+20", linestyle='dashed')


#    plt.legend(loc='lower right', bbox_to_anchor=(1.0, 1.05), ncol=3, fancybox=True)




plt.tight_layout()
plt.savefig("z_SFRD_ModelVariance_pop"+str(pop)+".pdf", bbox_inches="tight")
plt.clf()
