def SFR_integral(z,cSFRd):
    """
    Takes the cosmic star formation rate density as input,
    converts redshift to real time,
    and integrates cSFRd over time to obtain total stellar density.
    """
    import numpy as np
    from astropy.cosmology import Planck15 as cosmo#FlatLambdaCDM

    if(len(z) != len(cSFRd)):
       print("ERROR in SFR_integral:",len(z),len(cSFRd))
       return

#convert redshift to time
    t = [cosmo.age(z_now).value for z_now in z]
    t = np.array(t) * 1e9#convert to years

    integral = np.trapz(cSFRd, x=t)

    return np.abs(integral)#abs because z array is sometimes opposite direction
