import numpy as np
import matplotlib.pyplot as plt
import os
import pandas as pd
from scipy.stats import ks_2samp
from asloth_PlotConf import SetMyLayout

sat_McConnachie2012 = [
  ['SAGA_BooI',   2.9e4, 64,  2.4,  0.81e6, -2.62], # McConnachie 2012
  ['SAGA_BooII',  1.0e3, 40,  10.5, 3.3e6,  -2.41], # McConnachie 2012
  ['SAGA_CVnI',   2.3e5, 218, 7.6,  19.e6,  -1.91], # McConnachie 2012, Kirby 2013
  ['SAGA_CVnII',  7.9e3, 161, 4.6,  0.91e6, -2.12], # McConnachie 2012, Kirby 2013
  ['SAGA_Car',    3.8e5, 107, 6.6,  6.3e6,  -1.46], # McConnachie 2012
  ['SAGA_Com',    3.7e3, 45,  4.6,  0.94e6, -2.25], # McConnachie 2012, Kirby 2013
  ['SAGA_Dra',    2.9e5, 76,  9.1,  11.e6,  -1.98], # McConnachie 2012, Kirby 2013
  ['SAGA_For',    2.0e7, 149, 11.7, 56.e6,  -1.04], # McConnachie 2012, Kirby 2013
  ['SAGA_GruI',   2.0e3, 120, 2.9,  0.48e6, -2.52], # formula from Wolf 2010 
  ['SAGA_Her',    3.7e4, 126, 3.7,  2.6e6,  -2.39], # McConnachie 2012, Kirby 2013
  ['SAGA_HorI',   2.0e3, 79,  4.9,  0.51e6, -2.44], # formula from Wolf 2010
  ['SAGA_LeoI',   5.5e6, 258, 9.2,  12.e6,  -1.45], # McConnachie 2012, Kirby 2013
  ['SAGA_LeoII',  7.4e5, 236, 6.6,  4.6e6,  -1.63], # McConnachie 2012, Kirby 2013
  ['SAGA_LeoIV',  1.9e4, 155, 3.3,  1.3e6,  -2.45], # McConnachie 2012, Kirby 2013
  ['SAGA_LeoT',   1.4e5, 417, 7.5,  3.9e6,  -1.74], # McConnachie 2012, Kirby 2013
  ['SAGA_PscII',  8.6e3, 181, 5.4,  1.6e6,  -2.58], # formula from Wolf 2010
  ['SAGA_RetII',  1.0e3, 30,  3.3,  0.56e6, -2.83], # Simon 2015 
  ['SAGA_Sgr',    2.1e7, 18,  9.6,  190.e6, -0.55], # McConnachie 2012
  ['SAGA_Scl',    2.3e6, 86,  9.2,  14.e6,  -1.68], # McConnachie 2012, Kirby 2013
  ['SAGA_Seg1',   3.4e2, 28,  3.7,  0.26e6, -2.52], # McConnachie 2012
  ['SAGA_Seg2',   8.6e2, 41,  2.2,  0.23e6, -2.14], # McConnachie 2012, Kirby 2013
  ['SAGA_Sex',    4.4e5, 89,  7.9,  25.e6,  -1.94], # McConnachie 2012, Kirby 2013
  ['SAGA_TriII',  0., 30,  3.4,  0.36e6,    -2.40], # couldn't find stellar mass.... 
  ['SAGA_TucII',  4.9e3, 69,  8.6,  8.3e6,  -2.94], # formula from Wolf 2010
  ['SAGA_TucIII', 2.0e3, 25,  1.2,  0.05e6, -2.25], # formula from Wolf 2010
  ['SAGA_UMa',    1.4e4, 102, 7.0,  11.e6,  -2.10], # McConnachie 2012, Kirby 2013
  ['SAGA_UMaII',  4.1e3, 38,  5.6,  3.9e6,  -2.18], # McConnachie 2012, Kirby 2013
  ['SAGA_UMi',    2.9e5, 78,  9.5,  9.5e6,  -2.13], # McConnachie 2012, Kirby 2013
  ['SAGA_WilI',   1.0e3, 43,  4.0,  2.7e6,  -1.40], # McConnachie 2012
  ['SMC',         10.**(8.67), 0., 0., 6.5e9,  -2],       
  ['LMC',         10.**(9.19), 0., 0., 1.1e11, -2]        
  ]

sat_Munoz2018 = [
['Sculptor', 6.262],
['Whiting 1', 2.951],
['Segue2', 2.676],
['Fornax', 7.317],
['AM 1', 3.944],
['Eridanus', 3.904],
['Palomar 2', 5.558],
['Carina', 5.706],
#['NGC 2419', 5.670],#NGC:globular cluster
['Koposov 2', 2.302],
['UMa II', 3.630],
['Pyxis', 4.215],
['Leo T', 4.973],
['Palomar 3', 4.127],
['Segue 1', 2.452],
['Leo I', 6.642],
['Sextans', 5.419],
['UMa I', 3.981],
['Willman 1', 2.943],
['Leo II', 5.828],
['Palomar 4', 4.339],
['Leo V', 3.692],
['Leo IV', 3.930],
['Koposov 1', 2.348],
['ComBer', 3.682],
['CVn II', 4.002],
['CVn I', 5.451],
['AM 4', 2.293],
['Bootes II', 3.106],
['Bootes I', 4.338],
#['NGC 5694', 5.107],
['Munoz 1', 2.127],
#['NGC 5824', 5.648],
['Umi', 5.546],
['Palomar 14', 4.093],
['Hercules', 4.266],
#['NGC 6229', 5.150],
['Palomar 15', 4.198],
['Draco', 5.417],
#['NGC 7006', 4.901],
['Segue 3', 2.280],
['Pisces II', 3.620],
['Palomar 13', 3.066],
#['NGC 7492', 4.375],
['Laevens2', 2.572],
['Eridanus III', 2.881],
['Horologium I', 3.351],
['Horologium II', 2.555],
['Reticulum II', 3.482],
['Eridanus II', 4.815],
['Pictoris I', 3.311],
['laevens 1', 3.852],
['Hydra II', 3.771],
['Indus I', 3.260], 
['Balbinot', 2.421],
['Kim 1', 1.639],
['Grus I', 3.321],
['Phoenix II', 3.252],
['Tucana II', 3.44],

['Gaia 2', 2.72], # Munoz 2018, Vband
['Gaia 1', 3.96], # Koposov et al. (2017), first found in Munoz 2018
['Draco II', 3.08], # Benjamin P. M. Laevens 2015, Vband
['Sagittarius', 7.32],
['Tucana III', 2.88],

['Tucana IV', 3.33], # Munoz 2018
['DESJ0034-4902', 3.12], # Munoz 2018
['DESJ0111-1341', 1.8], # Munoz 2018
['Cetus II', 1.92], # Munoz 2018
['DESJ0225-0304', 2.36], # Munoz 2018
['Reticulum III', 3.24], # Munoz 2018
['Columba I', 3.72], # Munoz 2018
['Crater II', 5.04], # Munoz 2018
['Virgo I', 2.24], # Munoz 2018
['Sagittarius II', 4.0], # Benjamin P. M. Laevens 2015
['Indus II', 3.64], # Munoz 2018
['Laevens3', 3.68], # Munoz 2018
['Grus II', 3.48], # Munoz 2018
['Pegasus III', 3.56], # Munoz 2018
['Aquarius II', 0.176], # Munoz 2018
['Tucana V', 2.56], # Munoz 2018
['SMC', 8.64],
['LMC', 9.16]
]

MW_star_obs = 5.43e10
err_MW_star_obs = 0.57e10

def read_data(UserFolder,UserLabels,folder_output):
  """
  Function to read data to plot SMHM relation 
 
  If one or more folders are specified, they will all be plotted.
  This can also be used to plot just one specific folder.
 
  Parameters:
      UserFolder (str): List of folder names to be plotted.
      UserLabels (str): List of labels. If none, UserFolders are used as labels
      folder_output (str): where the plot is saved

  Returns:
      Virial masses and stellar masses of galaxies in pandas.DataFrame format
 
  """

  columns_SMHM = ['id','mhalo','i_sub',\
  'M_peak','M_starII',\
  'M_starII_surv', 'L_starII_surv', 'num_PopIIProgenitor']

  df_small = []
  for folder,label in zip(UserFolder,UserLabels):
    file_input = folder+"/SMHM_filter.dat"
    if os.path.isfile(file_input):
      #print("The file "+file_input+" exists.")
      df = pd.read_csv(file_input, delim_whitespace=True, skiprows=2, header=None)
      df.columns = columns_SMHM 

      df_small.append(df)
    
  return df_small


def plot_CumuSMF(UserFolder,UserLabels,folder_output,if_plot):
  """
  Function to return ks statistics, p-values, and MW stellar masses
 
  If one or more folders are specified, they will all be plotted.
  This can also be used to plot just one specific folder.
 
  Parameters:
      UserFolder (str): List of folder names to be plotted.
      UserLabels (str): List of labels. If none, UserFolders are used as labels.
      folder_output (str): where the plot is saved.

  Returns:
      Lists of ks statistic, p-value and MW stellar mass.
 
  """

  df_small = read_data(UserFolder,UserLabels,folder_output)
  if (df_small):
    ks, pv, MW_mass = CumulativeNumberOfSatellites( df_small, 'CumulativeNumberSubhalo', \
      'M_starII_surv', if_plot, folder_output )

    return ks, pv, MW_mass
  else:
    return [-99.], [-99.], [-99.]

def plot_scatterSMHM(UserFolder,UserLabels,folder_output):
  """
  Function to read data and plot scatter SMHM relation.
 
  If one or more folders are specified, they will all be plotted.
  This can also be used to plot just one specific folder.
 
  Parameters:
      UserFolder (str): List of folder names to be plotted.
      UserLabels (str): List of labels. If none, UserFolders are used as labels.
      folder_output (str): where the plot is saved.

  Returns:
      None 
 
  """
  df_small = read_data(UserFolder,UserLabels,folder_output)
  if (df_small):
    AMtwodhistogram(df_small, np.log10(2.9e5), folder_output, \
      'scatterSMHM', 'M_starII_surv', 300, False )
 
class TwoDBound:
    def __init__(self, xmin, xmax, ymin, ymax):
        self.xmin = xmin
        self.xmax = xmax
        self.ymin = ymin
        self.ymax = ymax

def M_AM(M):
  """
  Function to compute expected stellar mass of the galaxy at given virial mas (Garrison-Kimmel et al. 2014).
 
  Parameters:
    M (float): virial mass of the halo
  
  Returns:
    expected stellar mass
 
  """
  eps = 0.0167109061
  M1  = 3.26588e11
  f_bwc_ = f_bwc(0.0)

  M_star = 10.0**(np.log10(eps*M1)+f_bwc(np.log10(M/M1))-f_bwc_)
  return M_star

def f_bwc(x):
  del_ = 3.508
  gam = 0.316
  alph=-1.92
  y = -np.log10(10.0**(alph*x)+1)
  y = y+del_*(np.log10(1+np.exp(x)))**gam/(1+np.exp(10.0**(-x)))

  return y

def AMtwodhistogram( df_list, y0, p_dir, \
  output_name, WhichMass, climmax, if_imfana ):

  """
  The actual function to plot scatter SMHM relation.
 
  Parameters:
    df_list (list of pd.DataFrame): input data
    y0 (float): the observational completeness
    p_dir (str): output folder

  Returns:
 
  """
  MX = np.linspace(3.e9, 1e13, num=int(1e5), endpoint=True)
  MAMX = M_AM(MX)
  AMBound = TwoDBound(2, 13, 2, 13)  
  df_star = [ df_list[ii][ df_list[ii][WhichMass] > 0. ]  for ii in range(len(df_list)) ]

  if ( len(df_star[0]['M_peak']) == 0):
    print ('This run gives no galaxies that host stars.')
    return 

  MW_list_halo = [df_star[0]['M_peak'][0]]
  MW_list_star = [df_star[0][WhichMass][0]]

  df_Mpeak = df_star[0]['M_peak']
  df_Mstar = df_star[0][WhichMass]
  for ii in range(1,len(df_star)):
    df_Mpeak = df_Mpeak.append(df_star[ii]['M_peak'], ignore_index=True)
    df_Mstar = df_Mstar.append(df_star[ii][WhichMass], ignore_index=True)
    MW_list_halo.append(df_star[ii]['M_peak'][0])
    MW_list_star.append(df_star[ii][WhichMass][0])

  M_peak = np.log10(df_Mpeak.values)
  M_starII = np.log10(df_Mstar.values)

  xedges, yedges = np.linspace(AMBound.xmin, AMBound.xmax, num=56, endpoint=True), \
    np.linspace(AMBound.ymin, AMBound.ymax, num=56, endpoint=True)
  hist, xedges, yedges = np.histogram2d(M_peak, M_starII, (xedges, yedges))
  xidx = np.clip(np.digitize(M_peak, xedges), 0, hist.shape[0]-1) # 
  yidx = np.clip(np.digitize(M_starII, yedges), 0, hist.shape[1]-1) # 
  c = hist[xidx, yidx]
  
  fig = plt.figure(figsize=(8,8))
  ax = fig.add_subplot(1, 1, 1, aspect=1)
  plt.fill_between( [AMBound.xmin, AMBound.xmax], \
    y0, facecolor='gray', alpha=0.5, edgecolor='none' )
  data_ = plt.scatter(M_peak, M_starII, marker=',', c=c, s=2, alpha=1., label='A-SLOTH', \
    edgecolor='none')  
  cb = plt.colorbar(data_, shrink=0.8)
  plt.clim(0,climmax) 

  plt.scatter(np.log10(MW_list_halo), np.log10(MW_list_star), \
    marker='v', edgecolor='cyan', s=3, facecolors='none', label='MW in A-SLOTH')
  ax.errorbar( np.log10(1.3e12), np.log10(MW_star_obs), xerr=(np.log10(1.6e12)-np.log10(1.3e12)), \
    yerr=( np.log10(MW_star_obs+err_MW_star_obs)-np.log10(MW_star_obs) ), \
    label="$M_\mathrm{*, obs} = 5.43^{+0.57}_{-0.57} \\times 10^{10} \mathrm{M}_\odot$ (McMillan+17)"\
    +"\n$M_\mathrm{vir, obs} = 1.3^{+0.3}_{-0.3} \\times 10^{12} \mathrm{M}_\odot$ (Posti&Helmi+19)", \
    alpha=1.0, color='b' )
  plt.plot( np.log10(MX), np.log10(MAMX), label='AM (Garrison-Kimmel +14)', color='r', alpha=0.5 )

  titlesize = 15
  labelsize = 15
  ticksize = 12
  plt.xlim(AMBound.xmin, AMBound.xmax)
  plt.xlabel('log10 (M$_\mathrm{vir}$/M$_\odot$)', fontsize=labelsize)
  plt.ylim(AMBound.ymin, AMBound.ymax)
  plt.ylabel('log10 (M$_\mathrm{*}$/M$_\odot$)', fontsize=labelsize)
  cb.set_label('$N_\mathrm{gal} / (0.04 ~\mathrm{dex}^2)$', fontsize=labelsize)
  plt.legend(loc=(0.01, 0.7), fancybox=True, framealpha=0., fontsize=ticksize)
  ax.tick_params(which='major', length=5, labelsize=ticksize, labelcolor='0.25')
  # print (p_dir + 'starMpeak_2dhist_' + output_name)
  plt.savefig ( p_dir + output_name + '.png', \
    format='png', dpi=360, bbox_inches='tight' )
  plt.close()

def CumulativeNumberOfSatellites(df_list, output_name, WhichMass, if_plot, p_dir):
  """
  The actual function to plot cumulative stellar mass function and compute ks statistic, 
  p-value from the KS test.
 
  Parameters:
    df_list (list of pd.DataFrame): input data
    output_name (str): figure name
    WhichMass (float): which stellar mass to use (the total or survival)
    if_plot (bool): whether to make plots or not
    p_dir (str): output folder

  Returns:
    lists of ks statistic, p-value, MW stellar mass

  """
  df_star = [ df_list[ii][df_list[ii][WhichMass] > 0.].sort_values(by=[WhichMass], \
                ascending=False) for ii in range(len(df_list))]

  MW_mass = np.zeros(len(df_star))
  ks = np.zeros(len(df_star))
  pv = np.zeros(len(df_star))
  for ii in range(len(df_star)):
    yy = np.arange( len(df_star[ii][WhichMass]) )
    df_star[ii]['count'] = yy
    MW_mass[ii] = np.max(df_list[ii][WhichMass])

  _list  = sat_McConnachie2012.copy()
  b_list = sat_Munoz2018.copy()

  up_obs = np.array(b_list, dtype=object)
  up_obs = up_obs[up_obs[:,1].argsort()]
  new_upobs = [10.**a for a in up_obs[:,1]][::-1]
  y_newupobs = np.arange(new_upobs.__len__()) + 1

  df_star_complete = [ df_star[ii][1:12] for ii in range(len(df_list))]

  obs_complete = []
  com_index = 0
  x = np.logspace(np.log10(2.9e5), 10, num=10, endpoint=True)
  obs_y = np.zeros(10)
  for ii in range(len(new_upobs)):
    if new_upobs[ii] >= 2.9e5:
      obs_complete.append(new_upobs[ii])
    if new_upobs[ii-1] > 2.9e5 and new_upobs[ii] <= 2.9e5:
      com_index = ii

  for ii in range(10):
    for jj in range(len(obs_complete)):
      if (obs_complete[jj] >= x[ii]): 
        obs_y[ii] += 1

  for ii in range(len(df_star)):
    if len(df_star_complete[ii][WhichMass]) == 0:
      ks[ii] = 1.
      pv[ii] = 0.
      print ('All A-SLOTH galaxies have stellar masses below the completeness limit.')
    else:
      a  = ks_2samp(df_star_complete[ii][WhichMass], obs_complete)
      ks[ii] = a[0]
      pv[ii] = a[1]

  if (if_plot == True):
    MWfrac_out = 0.
    fig = SetMyLayout()
    #fig, ax = plt.subplots()
    plt.plot(new_upobs[0:com_index+1], y_newupobs[0:com_index+1], label='observed MW satellites', color='b')
    plt.plot(new_upobs[com_index:], y_newupobs[com_index:], 'b--')
    for ii in range(len(df_star)-1):
      plt.plot(df_star[ii][WhichMass], df_star[ii]['count'], alpha=0.5, color='k', label="")
    plt.plot(df_star[-1][WhichMass], df_star[-1]['count'], alpha=0.5, color='k', label='A-SLOTH')
    #ax.axvspan(1.e3, 10.**5.44, alpha=0.5, color='gray')
    plt.fill_between(np.linspace(1e3,10.**5.44,2),1,1e3,facecolor='grey',alpha=0.5)
    plt.xlim(1.e3, 1.e10)
    plt.ylim(1, 1e3)
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel('M$_{*}$/M$_\odot$')
    plt.ylabel('$N(>$M$_{*})$')
    plt.legend()
    plt.savefig(p_dir + output_name + '_' + WhichMass + '.pdf', format='pdf', dpi=360, bbox_inches='tight')
    plt.clf()
    plt.close()

  return ks, pv, MW_mass
