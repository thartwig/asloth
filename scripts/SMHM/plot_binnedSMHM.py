#-*-coding:utf-8 -*- 

import numpy as np
import os
import pandas as pd
import matplotlib.pyplot as plt
from asloth_PlotConf import SetMyLayout

cps = ['', 'Greens']
cs = ['r', 'g']

columns_SMHM = ['id','mhalo','i_sub',\
'M_peak','M_starII','M_starII_surv', 'L_starII_surv', 'num_PopIIProgenitor']

year_cgs=3.16e7
MW_star_obs = 5.43e10
err_MW_star_obs = 0.57e10

class TwoDBound:
    def __init__(self, xmin, xmax, ymin, ymax):
        self.xmin = xmin
        self.xmax = xmax
        self.ymin = ymin
        self.ymax = ymax

def M_AM(M):
  eps = 0.0167109061
  M1  = 3.26588e11
  f_bwc_ = f_bwc(0.0)

  M_star = 10.0**(np.log10(eps*M1)+f_bwc(np.log10(M/M1))-f_bwc_)
  return M_star

def f_bwc(x):
  del_ = 3.508
  gam  = 0.316
  alph = -1.92
  y = -np.log10(10.0**(alph*x)+1)
  y = y+del_*(np.log10(1+np.exp(x)))**gam/(1+np.exp(10.0**(-x)))

  return y

################################################################################################

def PlotNadler20SMHM():
  """
  The function plots SMHM relation from Nadler et al. 2020. 
 
  """
  ### data points to reproduce Fig. 6 in Nadler+20 (https://ui.adsabs.harvard.edu/abs/2020ApJ...893...48N/abstract)
  #data
  center = np.array([ 7.6,  8.2,  8.8,  9.4, 10. , 10.6, 11.2])
  mstar_vals_mean = np.array([1.89614677, 2.90308217, 4.09352102, 5.38237732, 6.39770067,
         7.57797297, 9.20333154])
  mstar_vals_std = np.array([0.47704042, 0.50708294, 0.55040551, 0.64301304, 0.49810196,
         0.45789603, 0.27304321])
  plt.fill_between(center, (mstar_vals_mean-mstar_vals_std), \
    (mstar_vals_mean+mstar_vals_std), facecolor="k",alpha=0.3, label=r'Nadler+20',zorder=999)
  plt.fill_between(center, (mstar_vals_mean-2*mstar_vals_std), \
    (mstar_vals_mean+2*mstar_vals_std), facecolor="k",alpha=0.15,zorder=999)
  ### data points to reproduce Fig. 6 in Nadler+20 (https://ui.adsabs.harvard.edu/abs/2020ApJ...893...48N/abstract)

################################################################################################

def IMFANA(AMBound, MX, MAMX, imf_list, y0, p_dir, \
  imfname, WhichMass, climmax, cps, cs, ilabel, fname):
  """
  The function plots binned SMHM relation. 
 
  Parameters:
    AMBound: boundary of the SMHM relation from abundance matching (AM)
    MX: virial masses
    MAMX: stellar masses obtained from the AM technique at given virial masses
    imf_list (list of pd.DataFrame): input data
    y0 (float): observational completeness
    p_dir (str): output folder

  Returns:
    plot of SMHM relation

  """
  fig = SetMyLayout()
  plt.plot( np.log10(MX), np.log10(MAMX), label='AM (Garrison-Kimmel+14)', color='r', alpha=1 )
  PlotNadler20SMHM()
  for ii in range(len(imf_list)):
    if(len(imf_list) > 4):
      AM_BinMeanstd(imf_list[ii], WhichMass, ilabel[ii], ii, c='k')
    else:
      AM_BinMeanstd(imf_list[ii], WhichMass, ilabel[ii], ii)


  plt.hlines(xmin=2, xmax=13, y=np.log10(2.9e5), linestyles='dashed', colors='grey')

  plt.xlim(6, 13)
  plt.xlabel('log$_{10}$ (M$_\mathrm{vir, peak}$/M$_\odot$)')
  plt.ylim(AMBound.ymin, AMBound.ymax)
  plt.ylabel('log$_{10}$ (M$_\mathrm{*}$/M$_\odot$)')
  plt.legend()
  plt.savefig ( p_dir + fname + '.pdf', bbox_inches='tight' )
  plt.close()

def AM_BinMeanstd(df_list, WhichMass, tname, index_data, c=None):
  """
  The actual function to plot binned SMHM relation.
 
  Parameters:
    df_list (list of pd.DataFrame): input data
    WhichMass (float): which stellar mass to use (the total or survival)
    tnmae (str): labels
    index-data: index of the data
    c: specific color for the SMHM relation

  Returns:
    adding a binned SMHM relation to the plot
 
  """
  ### binned SMHM relation from one dataset/simulation
  df_star = [ df_list[ii][ df_list[ii][WhichMass] > 0. ]  for ii in range(len(df_list)) ]
  df_all = df_star[0]
  for ii in range(1, len(df_star)):
    df_all = pd.concat([df_all, df_star[ii]])
  df_all = df_all.sort_values(by='M_peak')
  df_all['M_peak'] = np.log10(df_all['M_peak'])
  df_all[WhichMass] = np.log10(df_all[WhichMass])

  nbin = 20
  mp = np.linspace(6, 13, num=nbin+1, endpoint=True)
  mpmid = (mp[1:]+mp[:nbin])/2
  ms = np.zeros((nbin, 2)) # mean, std
  for ii in range(nbin):
    dfsub = df_all[ df_all['M_peak'] > mp[ii] ].copy()
    dfsub = dfsub[ dfsub['M_peak'] <= mp[ii+1] ]
    ms[ii][0] = np.mean(dfsub[WhichMass])
    ms[ii][1] = np.std(dfsub[WhichMass])

  if(c=='k'): #  all black
    plt.errorbar(x=mpmid, y=ms[:,0], yerr=ms[:,1], label=tname, alpha=0.4, fmt='o-',c=c)
  else:
    plt.errorbar(x=mpmid, y=ms[:,0], yerr=ms[:,1], label=tname, alpha=0.5, fmt='o-')

def plot_binnedSMHM(UserFolder, UserLabels, folder_output):
  """
  The function reads data from multiple folders and plot the binned SMHM relations 
 
  If one or more folders are specified, they will all be plotted as separate SMHM relations.
  This can also be used to plot just one specific folder.

  Parameters:
      UserFolder (str): List of folder names to be plotted.
      UserLabels (str): List of labels. If none, UserFolders are used as labels
      folder_output (str): where the plot is saved

  Returns:
    plot of SMHM relation comparison

  """

  MX = np.linspace(3.e9, 1e13, num=int(1e5), endpoint=True)
  MAMX = M_AM(MX)
  AMBound = TwoDBound(-0.1, 13, -0.1, 13)

  imf_list = []
  for idir in range(len(UserFolder)):
    summary_data = pd.DataFrame(data={'Paraset': [], 'ks': [], 'ks_std': [], 'pv': [], \
      'pv_std': [], 'MSE': [], 'mse_std':[], 'M_star':[], 'sigma': [], 'Mstar_std': [], \
      'alpha': [], 'etaii': [], 'mcha':[], 'SFR_z0':[], 'SFR_std':[]})
    df_list = []
    desa = "SMHM_filter"
    columns = columns_SMHM
    datafile = UserFolder[idir]+ '/' + desa + ".dat"
    if os.path.isfile(datafile) and os.path.exists(datafile):
      #print (datafile, 'found') 
      df = pd.read_csv(datafile, delim_whitespace=True, skiprows=2, header=None)
      df.columns = columns
      df_list.append(df)
      imf_list.append(df_list)
    else:
      print('SMHM_filter.txt does not exist.')
      print('This is normal for EPS mode. Otherwise, please check filepath.')
      return

  if ( len(imf_list) < 1):
    print ('Not enough input for SMHM comparison. Please check filepath.')
    return
  else:
    IMFANA(AMBound, MX, MAMX, imf_list, np.log10(2.9e5), \
      folder_output, '', 'M_starII_surv', 500, \
      cps, cs, UserLabels, 'binnedSMHM')
