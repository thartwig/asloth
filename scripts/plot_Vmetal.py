def plot_Vmetal(UserFolder, UserLabels, folder_output):
    """
    Function to plot metal enriched volume fraction
  
    Parameters
    ----------
        UserFolder: List[str]
            List of folder names to be plotted

        UserLabels: List[str]
            List of labels. If none, UserFolders are used as labels

        folder_output: str
            Folder in which plots will be saved
  
    """
    import os.path
    import numpy as np
    import matplotlib.pyplot as plt
    from asloth_PlotConf import SetMyLayout

    SetMyLayout()

    for folder, label in zip(UserFolder, UserLabels):
        file_input = folder+"/vol_fracs.dat"
        if not os.path.isfile(file_input):
            print("The file "+file_input+" does not exist.")
            return

        data = np.genfromtxt(file_input, skip_header=1)
        # A-SLOTH
        z = data[:,0]
        f_rand = data[:,2]  # metal enriched fraction by random sampling
        f_sum = data[:,4]/data[:,5]  # metal enriched fraction by summing bubbles

        plt.plot(z,f_sum,label=label) #  adding bubbles
        #if(max(f_rand > 0)):  # only works for well-defined volumes
        #    plt.plot(z,f_rand,label=label+", random sampling")

# LITERATURE #
    data_J18 = np.genfromtxt('Data/Vmetal/Jaacks18.dat')
    data_S14 = np.genfromtxt('Data/Vmetal/Salvadori14.dat')
    data_P14 = np.genfromtxt('Data/Vmetal/Pallottini14.dat')
    data_V20 = np.genfromtxt('Data/Vmetal/Visbal20.dat')

    plt.plot(data_J18[:,0], data_J18[:,1], label="Jaacks+18", linestyle='dashed')
    plt.plot(data_S14[:,0], data_S14[:,1], label="Salvadori+14", linestyle='dashed')
    plt.plot(data_P14[:,0], data_P14[:,1], label="Pallottini+14", linestyle='dashed')
    plt.plot(data_V20[:,0], data_V20[:,1], label="Visbal+20", linestyle='dashed')

    plt.ylim((1e-8, 1))

    plt.xlabel('redshift')
    plt.ylabel('metal-enriched volume fraction')
    plt.legend()
    plt.yscale("log")

    plt.tight_layout()
    plt.savefig(folder_output+"z_Vmetal.pdf")
    plt.clf()
