$(BUILD_DIR)/feedback_arrays.o: $(BUILD_DIR)/num_pars.o $(BUILD_DIR)/defined_types.o $(BUILD_DIR)/cosmic_time.o
$(BUILD_DIR)/vol_fracs.o: $(BUILD_DIR)/cosmic_time.o $(BUILD_DIR)/spatial/bubble_tree.o $(BUILD_DIR)/in_files.o $(BUILD_DIR)/num_pars.o $(BUILD_DIR)/feedback_arrays.o $(BUILD_DIR)/config.o $(BUILD_DIR)/utility/utility.o $(BUILD_DIR)/utility/random_object.o
$(BUILD_DIR)/init_tree.o: $(BUILD_DIR)/defined_types.o $(BUILD_DIR)/num_pars.o $(BUILD_DIR)/memory_modules.o $(BUILD_DIR)/utility/utility.o $(BUILD_DIR)/crit_mass.o $(BUILD_DIR)/cosmic_time.o $(BUILD_DIR)/metals/MetalMix_dZ.o $(BUILD_DIR)/utility/savgol.o $(BUILD_DIR)/metals/MDF.o $(BUILD_DIR)/metals/metals.o $(BUILD_DIR)/EPS/parameters.o $(BUILD_DIR)/EPS/modified_merger_tree.o $(BUILD_DIR)/in_files.o
$(BUILD_DIR)/SF_routines.o: $(BUILD_DIR)/defined_types.o $(BUILD_DIR)/num_pars.o $(BUILD_DIR)/utility/random_object.o $(BUILD_DIR)/utility/utility.o $(BUILD_DIR)/metals/metal_functions.o $(BUILD_DIR)/metals/MDF.o $(BUILD_DIR)/metals/metals.o $(BUILD_DIR)/stellar/populations.o $(BUILD_DIR)/chemistry.o $(BUILD_DIR)/cosmic_time.o $(BUILD_DIR)/stellar/SEVN.o
$(BUILD_DIR)/read_tree.o: $(BUILD_DIR)/defined_types.o $(BUILD_DIR)/in_files.o $(BUILD_DIR)/num_pars.o $(BUILD_DIR)/memory_modules.o $(BUILD_DIR)/utility/utility.o $(BUILD_DIR)/cosmic_time.o
$(BUILD_DIR)/star_formation.o: $(BUILD_DIR)/spatial/bubble_tree.o $(BUILD_DIR)/defined_types.o $(BUILD_DIR)/num_pars.o $(BUILD_DIR)/metals/metal_functions.o $(BUILD_DIR)/snowplow.o $(BUILD_DIR)/utility/random_object.o $(BUILD_DIR)/SF_routines.o $(BUILD_DIR)/in_files.o $(BUILD_DIR)/crit_mass.o $(BUILD_DIR)/utility/utility.o $(BUILD_DIR)/utility/determine_Rvir.o $(BUILD_DIR)/metals/IGM_Z_routines.o $(BUILD_DIR)/metals/MetalMix_dZ.o
$(BUILD_DIR)/cosmic_time.o: $(BUILD_DIR)/num_pars.o $(BUILD_DIR)/in_files.o
$(BUILD_DIR)/to_file.o: $(BUILD_DIR)/num_pars.o $(BUILD_DIR)/in_files.o $(BUILD_DIR)/utility/utility.o $(BUILD_DIR)/utility/real_to_string.o $(BUILD_DIR)/config.o $(BUILD_DIR)/cosmic_time.o $(BUILD_DIR)/defined_types.o $(BUILD_DIR)/feedback_arrays.o $(BUILD_DIR)/utility/determine_Rvir.o $(BUILD_DIR)/memory_modules.o $(BUILD_DIR)/metals/MDF.o $(BUILD_DIR)/metals/metal_functions.o
$(BUILD_DIR)/crit_mass.o: $(BUILD_DIR)/num_pars.o $(BUILD_DIR)/cosmic_time.o
$(BUILD_DIR)/asloth.o: $(BUILD_DIR)/config.o $(BUILD_DIR)/defined_types.o $(BUILD_DIR)/EPS/EPS_wrapper.o $(BUILD_DIR)/memory_modules.o $(BUILD_DIR)/num_pars.o $(BUILD_DIR)/to_file.o $(BUILD_DIR)/in_files.o $(BUILD_DIR)/utility/check_flags.o $(BUILD_DIR)/utility/utility.o $(BUILD_DIR)/vol_fracs.o $(BUILD_DIR)/feedback_routines.o $(BUILD_DIR)/spatial/bubble_tree.o $(BUILD_DIR)/read_tree.o $(BUILD_DIR)/init_tree.o $(BUILD_DIR)/star_formation.o $(BUILD_DIR)/metals/read_yields.o $(BUILD_DIR)/metals/metal_functions.o $(BUILD_DIR)/stellar/SEVN.o $(BUILD_DIR)/stellar/populations.o $(BUILD_DIR)/SF_routines.o $(BUILD_DIR)/crit_mass.o $(BUILD_DIR)/cosmic_time.o
$(BUILD_DIR)/in_files.o:
$(BUILD_DIR)/defined_types.o: $(BUILD_DIR)/metals/metals.o $(BUILD_DIR)/utility/utility.o
$(BUILD_DIR)/feedback_routines.o: $(BUILD_DIR)/spatial/bubble_tree.o $(BUILD_DIR)/defined_types.o $(BUILD_DIR)/feedback_arrays.o $(BUILD_DIR)/crit_mass.o $(BUILD_DIR)/utility/random_object.o
$(BUILD_DIR)/memory_modules.o: $(BUILD_DIR)/defined_types.o
$(BUILD_DIR)/snowplow.o: $(BUILD_DIR)/num_pars.o $(BUILD_DIR)/utility/determine_Rvir.o
$(BUILD_DIR)/chemistry.o: $(BUILD_DIR)/num_pars.o
$(BUILD_DIR)/num_pars.o:
$(BUILD_DIR)/config.o: $(BUILD_DIR)/num_pars.o $(BUILD_DIR)/in_files.o $(BUILD_DIR)/utility/random_object.o
$(BUILD_DIR)/utility/interp.o: |utility
$(BUILD_DIR)/utility/savgol.o: $(BUILD_DIR)/utility/utility.o |utility
$(BUILD_DIR)/utility/real_to_string.o: |utility
$(BUILD_DIR)/utility/check_flags.o: |utility
$(BUILD_DIR)/utility/utility.o: $(BUILD_DIR)/in_files.o $(BUILD_DIR)/num_pars.o |utility
$(BUILD_DIR)/utility/random_object.o: |utility
$(BUILD_DIR)/utility/locate.o: |utility
$(BUILD_DIR)/utility/determine_Rvir.o: $(BUILD_DIR)/num_pars.o |utility
$(BUILD_DIR)/stellar/IMF.o: $(BUILD_DIR)/utility/utility.o |stellar
$(BUILD_DIR)/stellar/populations.o: $(BUILD_DIR)/num_pars.o $(BUILD_DIR)/stellar/props.o $(BUILD_DIR)/utility/utility.o $(BUILD_DIR)/stellar/IMF.o $(BUILD_DIR)/metals/metals.o |stellar
$(BUILD_DIR)/stellar/props.o: $(BUILD_DIR)/num_pars.o |stellar
$(BUILD_DIR)/stellar/SEVN.o: $(BUILD_DIR)/num_pars.o |stellar
$(BUILD_DIR)/EPS/hyperbolic.o: |EPS
$(BUILD_DIR)/EPS/memory.o: $(BUILD_DIR)/memory_modules.o |EPS
$(BUILD_DIR)/EPS/EPS_wrapper.o: $(BUILD_DIR)/EPS/parameters.o $(BUILD_DIR)/EPS/make_tree.o $(BUILD_DIR)/memory_modules.o $(BUILD_DIR)/EPS/tree_routines.o $(BUILD_DIR)/EPS/modified_merger_tree.o $(BUILD_DIR)/defined_types.o $(BUILD_DIR)/num_pars.o $(BUILD_DIR)/init_tree.o |EPS
$(BUILD_DIR)/EPS/spline.o: |EPS
$(BUILD_DIR)/EPS/tree_routines.o: $(BUILD_DIR)/defined_types.o |EPS
$(BUILD_DIR)/EPS/parameters.o: |EPS
$(BUILD_DIR)/EPS/indexxx.o: $(BUILD_DIR)/num_pars.o |EPS
$(BUILD_DIR)/EPS/sigmacdm_spline.o: $(BUILD_DIR)/num_pars.o $(BUILD_DIR)/EPS/parameters.o |EPS
$(BUILD_DIR)/EPS/deltcrit.o: $(BUILD_DIR)/num_pars.o |EPS
$(BUILD_DIR)/EPS/unresolved_mass.o: $(BUILD_DIR)/EPS/modified_merger_tree.o |EPS
$(BUILD_DIR)/EPS/modified_merger_tree.o: |EPS
$(BUILD_DIR)/EPS/split_PCH.o: $(BUILD_DIR)/num_pars.o $(BUILD_DIR)/EPS/parameters.o $(BUILD_DIR)/EPS/modified_merger_tree.o $(BUILD_DIR)/utility/random_object.o |EPS
$(BUILD_DIR)/EPS/make_tree.o: $(BUILD_DIR)/num_pars.o $(BUILD_DIR)/cosmic_time.o $(BUILD_DIR)/defined_types.o $(BUILD_DIR)/memory_modules.o $(BUILD_DIR)/utility/random_object.o $(BUILD_DIR)/EPS/parameters.o |EPS
$(BUILD_DIR)/EPS/transfer_function.o: $(BUILD_DIR)/num_pars.o $(BUILD_DIR)/EPS/parameters.o |EPS
$(BUILD_DIR)/metals/IGM_Z_routines.o: $(BUILD_DIR)/defined_types.o |metals
$(BUILD_DIR)/metals/read_yields.o: $(BUILD_DIR)/stellar/populations.o $(BUILD_DIR)/config.o $(BUILD_DIR)/metals/metals.o |metals
$(BUILD_DIR)/metals/metals.o: $(BUILD_DIR)/utility/utility.o |metals
$(BUILD_DIR)/metals/MetalMix_dZ.o: $(BUILD_DIR)/num_pars.o $(BUILD_DIR)/defined_types.o $(BUILD_DIR)/utility/random_object.o |metals
$(BUILD_DIR)/metals/metal_functions.o: $(BUILD_DIR)/num_pars.o $(BUILD_DIR)/defined_types.o $(BUILD_DIR)/utility/utility.o $(BUILD_DIR)/metals/metals.o |metals
$(BUILD_DIR)/metals/MDF.o: $(BUILD_DIR)/defined_types.o |metals
$(BUILD_DIR)/spatial/bubble_tree.o: $(BUILD_DIR)/num_pars.o $(BUILD_DIR)/defined_types.o $(BUILD_DIR)/config.o $(BUILD_DIR)/cosmic_time.o $(BUILD_DIR)/crit_mass.o $(BUILD_DIR)/metals/metal_functions.o $(BUILD_DIR)/spatial/bubble_tree_types.o |spatial
$(BUILD_DIR)/spatial/bubble_tree_types.o: $(BUILD_DIR)/utility/utility.o $(BUILD_DIR)/metals/metals.o |spatial
