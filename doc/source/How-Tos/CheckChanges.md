# Check Changes
If you have modified the source code and want to check the resulting differences, you can use the provided python scripts to do so. Let's say you ran the fiducial version of A-SLOTH and saved the output in the folder `output_v0`. Then you implement some new physics or improve some routine, run the code again and obtain a new output folder with the name `output_v1`.

You can visually compare the output of these two folders with the function `scripts/plot_asloth.py`. You should set
```
UserFolder = ["output_v0","output_v1"]
UserLabels = ["v0","v1"]
plot_newest_folder = False
```
and run it with `python3 scripts/plot_asloth.py`. You can use the produced plots to inspect the changes from your code update.
