# Changing which chemical elements are traced
If you want to trace different elements, you can specify these in the parameter file under `ElementName`. You should then also update `N_ELEMENTS` so that N_ELEMENTS = len(ElementName)+1.
