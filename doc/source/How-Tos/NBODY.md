# Get a tree
The code can be run in the EPS mode without any additional merger tree data. However, we recommend the EPS mode only for testing. To obtain reliable results, one should obtain dark matter merger trees from simulations.
A-SLOTH uses its own binary format. Please contact the team on advice on how to obtain trees. The code includes a sample-script to convert merger trees from consistent-trees to our own format (reindex/reindex.F90, please look at the comments in the source code for instructions on how to use it).
