Utility modules
===============

.. toctree::
   :maxdepth: 1
   :caption: Contents:


Converters
----------
.. f:automodule:: converters

Savitzki-Golay filter
---------------------
.. f:automodule:: filter_module

Utility
---------------------
.. f:automodule:: utility

Virial radius
-------------
.. f:automodule:: virial_radius

Check flags
-----------
.. f:automodule:: check_flags

Random number generator
-----------------------
.. f:automodule:: random_object
