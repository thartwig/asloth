Stellar populations
===================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

Populations
-----------
.. f:automodule:: populations

Initial mass functions
----------------------
.. f:automodule:: imfs

Stellar Properties
------------------
.. f:automodule:: stellar_props
