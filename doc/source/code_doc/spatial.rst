Spatially resolved feedback
===========================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

Modules and routines for efficiently tracing spatially resolved feedback

Bubble Tree
-----------

.. f:automodule:: bubble_tree

Bubble tree types
-----------------
.. f:automodule:: bubble_tree_types
