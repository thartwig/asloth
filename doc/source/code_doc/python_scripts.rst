
Python Scripts
==================================

.. toctree::
   :maxdepth: 1
   :caption: Contents:


scripts/plot\_asloth.py
-------------------------------

.. automodule:: scripts.plot_asloth
   :members:
   :undoc-members:


scripts/tau/plot\_Vion.py
-------------------------------

.. currentmodule:: tau.plot_Vion

.. autofunction:: plot_Vion

.. autofunction:: get_tau_sigma


scripts/wrapper
-------------------------------
This script allows to explore input parameters by automatically launching various runs.
Users can set their specific path\_output variable.
If you want to run with NBODY merger trees, also set path\_tree.
If you think that disk I/O might be a bottleneck, you should also set your username in the function N\_disk\_sleep() to work correctly.

.. currentmodule:: wrapper.loop_trees

.. autofunction:: RunTrees

.. autofunction:: Get_CatList

.. autofunction:: avail_mem_GB

.. autofunction:: CPU_use

.. autofunction:: N_disk_sleep



.. currentmodule:: wrapper.analyse_trees

.. autofunction:: get_p_from_folders


.. currentmodule:: wrapper.tutorial_GridPlot

.. autofunction:: plot_tutorial2



scripts/tau/plot\_tgas.py
-------------------------------

.. currentmodule:: utilities.plot_tgas

.. autofunction:: plot_tgas


scripts/utility.py
-------------------------------

.. automodule:: utilities.utility
   :members:
   :undoc-members:


scripts/SMHM/plot\_scatterSMHM.py
---------------------------------

.. currentmodule:: SMHM.plot_scatterSMHM
.. autofunction:: plot_scatterSMHM
.. autofunction:: plot_CumuSMF
.. autofunction:: read_data
.. autofunction:: M_AM
.. autofunction:: f_bwc
.. autofunction:: AMtwodhistogram
.. autofunction:: CumulativeNumberOfSatellites


scripts/SMHM/plot\_binnedSMHM.py
---------------------------------

.. currentmodule:: SMHM.plot_binnedSMHM
.. autofunction:: plot_binnedSMHM
.. autofunction:: IMFANA
.. autofunction:: AM_BinMeanstd
.. autofunction:: PlotNadler20SMHM

scripts/plot\_mcrit.py
-------------------------------

.. currentmodule:: utilities.plot_mcrit
.. autofunction:: plot_mcrit
.. autofunction:: crit_mass

scripts/plot\_stellarprop.py
-------------------------------

.. currentmodule:: utilities.plot_stellarprop
.. autofunction:: plot_stellarproperties
