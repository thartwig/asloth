
Main Code
==================================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

ASLOTH main program
-------------------
.. f:autoprogram:: ASLOTH

Configuration
-------------
config
......
.. f:automodule:: config

Type definitions
----------------
defined_types
.............
.. f:automodule:: defined_types

Tree Reader
-----------
tree_reader
...........
.. f:automodule:: tree_reader

Tree initialization
-------------------
tree_initialization
...................
.. f:automodule:: tree_initialization


numerical and resolution parameters
-----------------------------------
numerical parameters
....................
.. f:automodule:: numerical_parameters

resolution parameters
.....................
.. f:automodule:: resolution_parameters

Precomputed time-dependent quantities
-------------------------------------
crit_masses
...........
.. f:automodule:: crit_masses
cosmic_time
...........
.. f:automodule:: cosmic_time

EPS wrapper
-----------
eps_wrapper
...........
.. f:automodule:: eps_wrapper

Chemistry
---------
chemistry
.........
.. f:automodule:: chemistry

Input files
-----------
.. f:automodule:: input_files

Memory modules
--------------
Tree_memory_arrays
..................
.. f:automodule:: tree_memory_arrays

Tree_memory_arrays_passable
...........................
.. f:automodule:: tree_memory_arrays_passable

Star formation
--------------
SF_routines
...........
.. f:automodule:: sf_routines
Star_formation
..............
.. f:automodule:: star_formation

Stellar feedback
----------------
feedback_routines
.................
.. f:automodule:: feedback_routines
snowplow
........
.. f:automodule:: snowplow

Feedback tracing
----------------
feedback_arrays
...............
.. f:automodule:: feedback_arrays
volume_fractions
................
.. f:automodule:: volume_fractions

Outputs
-------
to_file
.......
.. f:automodule:: to_file

