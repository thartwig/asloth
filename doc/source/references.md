# References and Links

## Articles by the ASLOTH collaboration
[Chen et al. 2022](https://ui.adsabs.harvard.edu/abs/2022MNRAS.517.6140C/abstract)
[Hartwig et al. 2022](https://ui.adsabs.harvard.edu/abs/2022arXiv220600223H/abstract)  
[Magg et al. 2022](https://ui.adsabs.harvard.edu/abs/2021arXiv211015948M/abstract)  
[Chen et al. 2022](https://ui.adsabs.harvard.edu/abs/2022MNRAS.513..934C/abstract)  
[Tarumi et al. 2020](https://ui.adsabs.harvard.edu/abs/2020ApJ...897...58T/abstract)  
[Hartwig et al. 2018](https://ui.adsabs.harvard.edu/abs/2018MNRAS.478.1795H/abstract)  
[Magg et al. 2018](https://ui.adsabs.harvard.edu/abs/2018MNRAS.473.5308M/abstract)  
[Magg et al. 2016](https://ui.adsabs.harvard.edu/abs/2016MNRAS.462.3591M/abstract)  
[Hartwig et al. 2016b](https://ui.adsabs.harvard.edu/abs/2016MNRAS.462.2184H/abstract)  
[Hartwig et al. 2016a](https://ui.adsabs.harvard.edu/abs/2016MNRAS.460L..74H/abstract)  
[Hartwig et al. 2015](https://ui.adsabs.harvard.edu/abs/2015MNRAS.447.3892H/abstract)  

## The Caterpillar Project
[Caterpillar Website](https://www.caterpillarproject.org/)
