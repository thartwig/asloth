# Script to run Merger Code for many different parameter configurations automatically
# Can be used for parameter exploration and calibration

# Based on work done by Shafqat Riaz; Riaz et al (2022), ApJL 937 L6 [https://doi.org/10.3847/2041-8213/ac8ea6].
# Many places updated for dust modeling in Tsuna et al (2023), MNRAS accepted [https://doi.org/10.1093/mnras/stad3043]. 

import os, sys
# NOTE hmf is turned off from the original script; in Tsuna+(2023) we simply sample the halo masses log-uniformly
#import hmf
sys.path.insert(0, 'scripts') # needed to import utility

import numpy as np
from astropy.io import ascii
import time
import psutil
import glob
from matplotlib import pyplot as plt
import matplotlib
from utilities.utility import asloth_fit, asloth_config
matplotlib.use('Agg')


#SET YOUR SPECIFIC PATHS HERE
path_tree = "not_required_for_EPS_trees"
path_output = "/home/tsuna/asloth/loop_haloes_Mar7/" #empty saves output in the a-sloth folder. Alternative directory can be specified here

#index = 0
#j = 0 
#keep track of the available RAM and CPUs during runs
memory_min = 1000.#start with large value
CPU_max = 0#start with small value

wait_min = 2 # minimum time in seconds to wait until python script continues


def build(omp=True):
    os.system('make clean')
    os.system('rm *.o *.mod asloth.exe')
#    if(omp):
#        os.system('make omp') # parallel is usually faster
#    else:
    os.system('make') # EPS trees can not be run in parallel

def run(i):
    os.system(f'./asloth.exe param_loop_{i}.nml&')
    f = open("input_param.txt", 'w')
    f.write(str(alpha))
    f.close()
    return

def GetMyRandom(xmin,xmax):
    #sample a number liniarly between xmin and xmax
    dx = (xmax-xmin)
    return xmin+dx*np.random.rand()


def avail_mem_GB():
    '''
    Funtion returns available RAM in GB
    '''
    global memory_min
    global mem_array
    virtual = psutil.virtual_memory()
    avail_mem = virtual.available/(1024**3)
    print("Available Memory: ",avail_mem,"GB")
    mem_array.append(avail_mem)
    memory_min = np.min([memory_min,avail_mem])
    print("Minimum Memory: ",memory_min)
    return avail_mem


def CPU_use():
    '''
    Funtion returns available CPUs in %
    '''
    global CPU_max
    global CPU_array
    CPU = psutil.cpu_percent(2)#over 2s
    print("CPU use: ",CPU,"%")
    CPU_array.append(CPU)
    CPU_max = np.max([CPU_max,CPU])
    print("Max CPU: ",CPU_max,"%")
    return CPU

def N_disk_sleep():
    '''
    Funtion returns number of processes that are idle due to I/O.

    Change username accordingly
    '''
    n_sleep = 0
    try:
        for proc in psutil.process_iter(['pid', 'name', 'username']):
            if(proc.info['username']=='tsuna'):
                if(proc.status() == "disk-sleep"):
                    n_sleep += 1
    except:
        print("WARNING!!!")
        print("Maybe: psutil.NoSuchProcess: psutil.NoSuchProcess process no longer exists ??")
        n_sleep = 9

    print('Number Processes waiting for disk I/O:',n_sleep)
    return n_sleep

def RunTrees(Mhalo):
    '''
    This function loops over a set of merger trees and explores one combination of input parameters

    We first define input parameters that are constant within this loop.
    Then, we define tree-specific input parameters.
    Then, we check if sufficient computing resources are available.
    Eventually, we launch the A-SLOTH run.

    parameters
    ----------

        cats : list
            catalogue of merger trees [[name, nlev, RAM]]

        NBODY : bool
            N-Body or EPS-generated merger trees


    '''

    index = find_index()
    print("index: ",index)

#Generate parameters for this run
    cfg = asloth_config()

#Set fiducial parameters
    cfg.config["tree_path"] = path_tree
    cfg.config["IMF_min"] = IMFMIN#1.0 changed from fiducial value 5.0
    cfg.config["IMF_max"] = IMFMAX#210
    cfg.config["slope" ] = alpha #1.0
    cfg.config["ETAIII"] = 0.38
    cfg.config["ETAII"] = 0.19
    cfg.config["alpha_outflow"] = 0.86
    cfg.config["m_cha_out"] = 7.5e9
    cfg.config["F_ESCIII"] = 0.6
    cfg.config["F_ESCII"] = 0.37
    cfg.config["VBC"] = 0.8
    cfg.config["t_dust_grow_sol"] = t_dust_grow_sol
    cfg.config["lw_mode"] = 5


    time_start = time.time()

#loop over random seeds
#set the number of explored random seeds here
    if (Mhalo < 1e12):
        numseed= 10
    else:
        numseed = 10
    for j in range(numseed):
        while True:
            needed_memory = 3#GB
            print("Needed Memory: ",needed_memory)
            if(avail_mem_GB() > needed_memory and CPU_use() < 90 and N_disk_sleep() == 0):
                #enough resources available
                
                cfg.config["output_string"] = path_output+"output_para"+str(index)+"_z"+str(redshift)+"_Mhalo"+str(np.log10(Mhalo))+"_tgrow"+str(t_dust_grow_sol/1e6)
                cfg.config["tree_name"] = "NotRelevantForEPS"
                cfg.config["nlev" ] = 256
                cfg.config["cubic_box"] = False
                cfg.config["nthreads"] = 1
                cfg.config["tree_mass"] = Mhalo # mass of halo
                cfg.config["RNG_SEED"] = j # we vary the random seed between realisations
                cfg.config["zmin"] = redshift

                cfg.to_file(f"param_loop_{j}.nml")

                run(j)
                t_wait = np.max([needed_memory,wait_min])
                print("Waiting for ",t_wait,"s") # give enough time to reach the memory allocation
                time.sleep(t_wait)
                break
            else:
                print("Not enough free resources: waiting...")
                time.sleep(wait_min)


    #write duration to file to use later
    duration_min = (time.time()-time_start)/60
    time_file = open(path_output+"para_"+str(index)+"_time.txt",'w')
    time_file.write(str(duration_min)+"\n")
    time_file.close()

def find_index():
    '''
    find next free index for output directories
    '''
    for i in range(2700):#checking up to 1024
        dir_prefix = path_output+"output_para"+str(i)+"_"
        #print("Is there a directory starting with "+dir_prefix+"?")
        file_list = glob.glob(dir_prefix+"*")
        #print(len(file_list))
        if(len(file_list) == 0):
            break
    return(i)


def plot_histograms(mem_array,CPU_array):
    plt.hist(mem_array,bins=40)
    plt.xlabel("Available Memory [GB]")
    plt.savefig("RAM_histogram.pdf")
    plt.clf()

    plt.hist(CPU_array,bins=25)
    plt.xlabel("Used CPU [%]")
    plt.savefig("CPU_histogram.pdf")
    plt.close("all")


if __name__ == '__main__':
    

    NBODY = False #if set to True, change src/asloth.h accordingly

    mem_array = []
    CPU_array = []

    build(omp = NBODY) # because EPS crashed with OpenMP

    start_time = time.time()

    #print('********** Printing all the parameters values *************')
    #print(hmf.MassFunction.get_all_parameter_defaults(recursive=False) )

    #print('********** Printing quantaties *************')
    #print(hmf.MassFunction.quantities_available())


    for redshift in [7,11]:

#        import hmf  # this needs to be installed see: hmf.readthedocs.io/en/latest/installation.html

#        Mminhalo = np.log10(1e9) # minimum halo mass, i.e, lower limit in halo mass function

        haloNum = 40 # numbers of halos you want to sample randomly

        #Mmax is the maximumm halo mass, i.e., upper limiit in halo mass function

#        m,hmf = hmf.helpers.sample.sample_mf(haloNum,Mminhalo,z = redshift, hmf_model="Warren",Mmax=11, dlog10m = 0.01, cosmo_params={"Om0":0.31, "Ob0" : 0.049, "H0": 68})
        #print(hmf.cosmo,m)
        #print('**************Printing the halo masses*********')
#        file_hmfMass = open(path_output+str(Mminhalo)+'_'+str(redshift)+'_'+str(haloNum)+'.txt', 'w')
#        counter = 1
#        for mass in m:
#            file_hmfMass.write(str(counter)+'\t'+str(redshift)+'\t'+str("{:.4e}".format(mass))+"\n")
#            counter +=1
#        print('***********************')
#        print("halo masses has taken successfully from HMF")
#        file_hmfMass.close()

        #Loop over halo masses
        for t_dust_grow_sol in [3e7, 3e6]:
        #for Mhalo in m: #[1e9]:
          for Mhalo in np.logspace(8,12,haloNum):
              print(Mhalo)
              alpha = 1.0
              IMFMIN = 5.0
              IMFMAX = 210
              RunTrees(Mhalo)
#            for alpha in [1.0]:#[0.0, 1.0, 2.3]:
#                for IMFMIN in [5.0]:#[1.0, 3.0, 10.0]:
#                    for IMFMAX in [210]:#[100, 210, 300]:
                        #for Mhalo in [3e6]:
#                        RunTrees(Mhalo)

                        #cloudy_script_generator()


    print("Minimum Memory: ",memory_min)

    plot_histograms(mem_array,CPU_array)

    print("All runs started. Waiting for A-SLOTH to finish...")
    print("If the python script ends before A-SLOTH, the command line might not return to prompt")
    print("In this case, press ENTER at the of of A-SLOTH to return to command line prompt")

