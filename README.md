# A-SLOTH

The semi-analytical model A-SLOTH (Ancient Stars and Local Observables by Tracing Halos) is the first public code that connects the formation of the first stars and galaxies to observables. After several successful projects with this model, we publish the source code in this repository.
The model is based on dark matter merger trees that can either be generated based on Extended Press-Schechter theory or that can be imported from dark matter simulations. On top of these merger trees, A-SLOTH applies analytical recipes for baryonic physics to model the formation of both metal-free and metal-poor stars and the transition between them with unprecedented precision and fidelity.
A-SLOTH samples individual stars and includes radiative, chemical, and mechanical feedback. It is calibrated based on six observables, such as the optical depth to Thomson scattering, the stellar mass of the Milky Way and its satellite galaxies, the number of extremely-metal poor stars, and the cosmic star formation rate density at high redshift.
A-SLOTH has versatile applications with moderate computational requirements. It can be used to constrain the properties of the first stars and high-z galaxies based on local observables, predicts properties of the oldest and most metal-poor stars in the Milky Way, can serve as a subgrid model for larger cosmological simulations, and predicts next-generation observables of the early Universe, such as supernova rates or gravitational wave events.



## Quick Start

We provide a detailed documentation and tutorials [here](https://a-sloth.readthedocs.io/en/latest/index.html). The same documentation can be found in the folder `/doc` (for offline use).

You can clone the repository with
```
git clone https://gitlab.com/thartwig/asloth.git
```
and then compile and run the fiducial version of A-SLOTH with these commands:
```
make
./asloth.exe param_template.nml
```

Then, you can analyse the results with:
```
python scripts/plot_asloth.py
```

We recommend to first go through the [provided tutorials](https://a-sloth.readthedocs.io/en/latest/getting_started.html#tutorials).





## Support
See [here](doc/source/help.md).

## Usage Policy
See [here](doc/source/usage_policy.md).

## License
See [here](LICENSE.md).



## Authors and acknowledgment
Main developers: M. Magg, T. Hartwig, L.-H. Chen, Y. Tarumi

Current maintainer: S. C. O. Glover

Co-Authors: V. Bromm, A. P. Ji, R. S. Klessen, M. A. Latif, V. Lipatova, M. Volonteri, N. Yoshida

We thank Boyuan Liu, Britton Smith, Anna Schauer, and Betül Uysal for valuable discussions.



## Project status
2023/02/28
Code paper: [![DOI](https://joss.theoj.org/papers/10.21105/joss.04417/status.svg)](https://doi.org/10.21105/joss.04417)

Methods paper published in ApJ and available on [arXiv](https://arxiv.org/abs/2206.00223)


## Version
1.3.0: 2024/10/04 Fixed some bugs in treatment of cooling time. Added support for SEVN stellar evolution tracks, calculation of satellite mass-Z relation. 

Previous:

1.2.1: 2022/09/28 improved p-value for UMP2EMP: now p=1 if model value is BETWEEN observed values

1.2.0: 2022/09/27 BUGFIX: now check mass again before inheriting external ionisation state

1.1.1: 2022/09/12 added new module that can create and read smaller versions of merger tree files (for runs with #NBODY).

1.1.0: 2022/06/22 After code review in JOSS [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.6683682.svg)](https://doi.org/10.5281/zenodo.6683682)

1.0.0: 2022/04/03 First commit to public repository
