# Compiler options inclusion
# Compile and Compiler flags
default_F90 = gfortran

ifndef F90
$(info F90 variable not set, using $(default_F90))
F90 = $(default_F90)
else 
$(info Using compiler: $(F90))
endif

# General rules
.SUFFIXES:
.SUFFIXES: .exe .o .F90 .f90 .mod .f


build/%.o:src/%.F90
	$(F90) -c $(OPTF) $< $(DFLAGS) -o build/$*.o 


all:	asloth.exe

# if you want to add compiler flags, please do that here

ifneq (,$(findstring gfortran,$(F90)))
  DFLAGS := -O3 -march=native -fdefault-real-8 -g -Jmods -Lmods
  omp_opt := -fopenmp -DHAVE_OMP
endif

ifneq (,$(findstring ifort,$(F90)))
  DFLAGS := -O3 -fpp -xHost -ipo -heap-arrays -g -r8 -D IFORT -shared-intel -module mods
  omp_opt := -qopenmp -DHAVE_OMP
endif

BUILD_DIR = build

# Object code list
# IMPORTANT: if you add files to this list or change how they are using eachother please run
# $ python scripts/utilities/make_dependency_list.py
# This is necessary to ensure correct compilation
TREE_OBJS = num_pars.o defined_types.o feedback_arrays.o memory_modules.o spatial/bubble_tree.o spatial/bubble_tree_types.o feedback_routines.o in_files.o cosmic_time.o crit_mass.o snowplow.o to_file.o init_tree.o read_tree.o star_formation.o SF_routines.o vol_fracs.o chemistry.o stellar/populations.o stellar/props.o stellar/IMF.o stellar/SEVN.o config.o

UTILITY_OBJS = utility/savgol.o utility/utility.o utility/random_object.o utility/interp.o utility/locate.o utility/real_to_string.o utility/determine_Rvir.o utility/check_flags.o
METAL_OBJS = metals/MDF.o metals/metals.o metals/read_yields.o metals/MetalMix_dZ.o metals/metal_functions.o metals/IGM_Z_routines.o 
EPS_OBJS = EPS/tree_routines.o EPS/modified_merger_tree.o EPS/deltcrit.o EPS/memory.o EPS/sigmacdm_spline.o EPS/hyperbolic.o EPS/EPS_wrapper.o EPS/split_PCH.o EPS/spline.o EPS/make_tree.o EPS/indexxx.o EPS/transfer_function.o EPS/unresolved_mass.o EPS/parameters.o 
TREE_OBJS := $(TREE_OBJS) $(UTILITY_OBJS) $(METAL_OBJS) $(EPS_OBJS)
TREE_OBJS := $(addprefix $(BUILD_DIR)/,$(TREE_OBJS))


.PHONY: debug
debug: DFLAGS := -Og -g -Wall -fdefault-real-8  -Wline-truncation  -Wcharacter-truncation  -Wsurprising  -Waliasing  -Wunused-parameter  -fwhole-file  -fcheck=all  -std=f2008  -pedantic  -fbacktrace -fall-intrinsics -Jmods -Lmods
# -Wimplicit-interface
debug: asloth.exe

.PHONY: O1
O1: DFLAGS= -O1 -march=native -fbounds-check -fdefault-real-8 -g -Jmods -Lmods -fbounds-check
O1: asloth.exe


.PHONY: profile
profile: DFLAGS := -O3 -pg -no-pie -march=native -fbounds-check -fdefault-real-8 -Jmods -Lmods 
profile: asloth.exe

.PHONY: trace
trace: DFLAGS := -O3 -g -Wall  -Wline-truncation  -Wcharacter-truncation  -Wsurprising  -Waliasing  -Wunused-parameter  -fwhole-file  -fcheck=all  -std=f2008  -pedantic  -fbacktrace -Jmods -Lmods 
# -Wimplicit-interface
trace: asloth.exe

#Please note: omp only works if NBODY is set
.PHONY: omp
omp: DFLAGS := $(DFLAGS) $(omp_opt)
omp: asloth.exe

#Check and update your hdf5 paths here
#Only needed for option #SEVN
.PHONY: hdf5
hdf5: HDF5_INSTALL = /usr/lib/x86_64-linux-gnu/hdf5/serial
hdf5: DFLAGS := -O3 -march=native -fdefault-real-8 -g -Jmods -Lmods -I/usr/include/hdf5/serial -L$(HDF5_INSTALL)
hdf5: HDF5_LINK =  $(HDF5_INSTALL)/libhdf5hl_fortran.a $(HDF5_INSTALL)/libhdf5_hl.a $(HDF5_INSTALL)/libhdf5_fortran.a $(HDF5_INSTALL)/libhdf5.a -lpthread -lsz -lz -ldl -lm
hdf5: DLINK = $(HDF5_LINK)
hdf5: asloth.exe


.PHONY: omphdf5
omphdf5: HDF5_INSTALL = /usr/lib/x86_64-linux-gnu/hdf5/serial
omphdf5: DFLAGS := $(DFLAGS) $(omp_opt) -I/usr/include/hdf5/serial -L$(HDF5_INSTALL)
omphdf5: HDF5_LINK =  $(HDF5_INSTALL)/libhdf5hl_fortran.a $(HDF5_INSTALL)/libhdf5_hl.a $(HDF5_INSTALL)/libhdf5_fortran.a $(HDF5_INSTALL)/libhdf5.a -lpthread -lsz -lz -ldl -lm
omphdf5: DLINK = $(HDF5_LINK)
omphdf5: asloth.exe



clean:
	\rm -r mods/* asloth.exe build/*

#Dependencies
all: src/asloth.h $(BUILD_DIR)/defined_types.o $(BUILD_DIR)/num_pars.o
include dependencies.mk

stellar:
	mkdir -p $(BUILD_DIR)/stellar
metals:
	mkdir -p $(BUILD_DIR)/metals
utility:
	mkdir -p $(BUILD_DIR)/utility
EPS:
	mkdir -p $(BUILD_DIR)/EPS
spatial:
	mkdir -p $(BUILD_DIR)/spatial

$(BUILD_DIR)/asloth.o: $(TREE_OBJS) 

#Rule for making the executable 

ifdef ADD_LINKS
asloth.exe: DLINK := $(DLINK) $(ADD_LINKS)
endif


asloth.exe: $(TREE_OBJS) $(BUILD_DIR)/asloth.o 
	$(F90) $(TREE_OBJS) $(BUILD_DIR)/asloth.o $(DFLAGS) -o asloth.exe $(DLINK)


reindex.exe:
	$(F90) reindex/reindex.F90 $(DFLAGS) -o reindex.exe
