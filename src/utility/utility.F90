#include "../asloth.h"
module utility
  ! General purpose utility functions
  use input_files
  use numerical_parameters, only: stdout
#ifdef IFORT
  use IFPORT
#endif
  implicit none
#ifndef IFORT
  intrinsic :: GETPID
#endif
  real :: start_cpu_time
  real :: current_cpu_time
  integer :: iunit_list(16) ! list of file IDs
  public assert
#ifdef MEM_REPORT
  public mem_report
#endif
  interface realloc
    ! subroutines to resize/allocate arrays. Newly allocated memory (i.e. full
    ! arrays or added parts of arrays) are initialized to 0 
    procedure :: r_realloc_1d, i_realloc_1d, r_realloc_2d
  end interface
  contains
#ifdef MEM_REPORT
    subroutine mem_report
        integer pid, iunit
        character(len=100) :: command_string
        real :: r_mem, v_mem
        pid=GETPID()
        write(command_string, *) "ps --no-headers -q", pid, "-o rss,vsize > "//trim(output_string)//".tmp_mem.dat"
        call execute_command_line(command_string, wait=.true.)
        open(newunit=iunit, file=trim(output_string)//".tmp_mem.dat")
        read (iunit, *) r_mem, v_mem
        close(iunit, status="delete")
        r_mem = r_mem/1048576
        v_mem = v_mem/1048576
        call CPU_TIME(current_cpu_time)
        write(iunit_list(1), FMT="(3F6.1)") (current_cpu_time - start_cpu_time)/60, r_mem, v_mem
    end subroutine
#endif

    subroutine assert(test_passed, message)
        logical, intent(in) :: test_passed
        character(len=*), intent(in) :: message
        if (.not. test_passed) then
          write(*,*) "ERROR:", message
          stop
        endif
    end subroutine assert

    subroutine r_realloc_1d(a, N_new)
        real, dimension(:), allocatable, intent(inout) :: a
        integer, intent(in) :: N_new
        integer :: N
        real, dimension(:), allocatable :: tmp
        logical :: alloc
        alloc = allocated(a)
        if (alloc) then
          N = size(a)
          allocate(tmp(N))
          tmp = a
          deallocate(a)
        endif
        allocate(a(N_new))
        a = 0.0
        if (alloc) a(1:N) = tmp
    end subroutine

    subroutine i_realloc_1d(a, N_new)
        integer, dimension(:), allocatable, intent(inout) :: a
        integer, intent(in) :: N_new
        integer :: N
        integer, dimension(:), allocatable :: tmp
        logical :: alloc
        
        alloc = allocated(a)
        if (alloc) then
          N = size(a)
          allocate(tmp(N))
          tmp = a
          deallocate(a)
        endif
        allocate(a(N_new))
        a = 0
        if (alloc) a(1:N) = tmp
    end subroutine

    subroutine r_realloc_2d(a, N1_new, N2_new)
        real, dimension(:,:), allocatable, intent(inout) :: a
        integer, intent(in) :: N1_new, N2_new
        integer, dimension(2) :: N
        real, dimension(:, :), allocatable :: tmp

        logical :: alloc
        
        alloc = allocated(a)
        if (alloc) then
          N = shape(a)
          allocate(tmp(N(1), N(2)))
          tmp = a
          deallocate(a)
        endif
        allocate(a(N1_new, N2_new))
        a = 0.0
        if (alloc) a(1:N(1),1:N(2)) = tmp
    end subroutine

    subroutine print_bar(prog, msg, complete)
        real :: prog
        character(len=*), intent(in) :: msg
        integer :: bar_i, j
        character :: bar(33)
        logical :: is_complete
        logical, optional :: complete
        is_complete = .False.
        if (present(complete)) is_complete=complete
        ! setting up progress bar
        bar(:) = ' '
        bar(1) = '['
        bar(33) = ']'
        ! until where is it full?
        bar_i = int(prog*30+2)
        ! filling progress bar
        do j=2, bar_i
          bar(j) = '#'
        end do
        ! printing
        if (is_complete) then
          write(stdout,FMT="(A, 33A, F6.2,A, A)",ADVANCE="Yes") &
              & msg, bar, (prog)*100.0, "%", " -- Complete"
        else
          write(stdout,FMT="(A, 33A, F6.2,A, A)",ADVANCE="NO") &
              & msg, bar, (prog)*100.0, "%", achar(13)
        endif
    end subroutine print_bar
end module
