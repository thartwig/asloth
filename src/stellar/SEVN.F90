#include "../asloth.h"

module sevn
  ! Helper functions for stellar parameters based on SEVN

#ifdef SEVN
  use HDF5
  implicit none

  REAL, DIMENSION(3,432,74,15) :: SEVN3D !Contains values of time, luminosity, radius at different mass and metallicity
  REAL, DIMENSION(15) :: Z_float !Contains the 15 different tabulates metallicities
  REAL, DIMENSION(74,15) :: Z_Mass_arr !Contains the ZAMS masses (depends on metallicity). Empty values are -99
  INTEGER, DIMENSION(74,15) :: Z_Mass_NmaxTime !Contains the maximum index up to which MyArray3D(:,:,M,Z) is filled with values
  INTEGER :: Ndim_Z, Ndim_mass, Ndim_time

contains

  subroutine init_SEVN()
    !Load HDF5 file and fill arrays that we need for stellar parameters
    use Numerical_Parameters, only: Z_Metals_Solar

    CHARACTER(LEN=64), PARAMETER :: filename = "Data/SEVN/Z_mass_time.hdf5" ! File name
    !Details about this file and how it was created in scripts/utilities/SEN_to_ASLOTH.ipynb

    INTEGER(HID_T) :: file_id       ! File identifier
    INTEGER(HID_T) :: dset_id       ! Dataset identifier
    INTEGER(HID_T) :: space_id       ! Dataspace identifier
    INTEGER(HID_T) :: dtype_id       ! Datatype identifier


    INTEGER     ::   error ! Error flag
    INTEGER     ::  i, j
    REAL        ::  Mmax
    INTEGER(HSIZE_T), DIMENSION(4) :: data_dims


    Ndim_Z = 15
    Ndim_mass = 74
    Ndim_time = 432

    !Dimensions of following arrays:
    !432: times in Myr
    !74: masses in Msun
    !15: metallicities in Mmetals/Mhydrogen
    !masses and times are not all filled. Empty values are -99

!    REAL, DIMENSION(3,432,74,15) :: SEVN3D !Contains values of time, luminosity, radius at different mass and metallicity
    !1: time/Myr
    !2: luminosity/Lsun
    !3: radius/Rsun
!    REAL, DIMENSION(15) :: Z_float !Contains the 15 different tabulates metallicities
!    REAL, DIMENSION(74,15) :: Z_Mass_arr !Contains the ZAMS masses (depends on metallicity). Empty values are -99
!    INTEGER, DIMENSION(74,15) :: Z_Mass_NmaxTime !Contains the maximum index up to which SEVN3D(:,:,M,Z) is filled with values



    write(0,*)"Reading SEVN HDF file..."



   ! Initialize FORTRAN interface.
   CALL h5open_f(error)

   ! Open an existing file.
   CALL h5fopen_f (filename, H5F_ACC_RDONLY_F, file_id, error)

   ! Open an existing dataset.
   CALL h5dopen_f(file_id, "MyArray3D", dset_id, error)
   !Get dataspace ID
   CALL h5dget_space_f(dset_id, space_id,error)

   !Get data
   CALL h5dread_f(dset_id, H5T_NATIVE_DOUBLE, SEVN3D, data_dims, error)
   !write(0,*), SEVN3D(:,1,1,1)


   CALL h5dopen_f(file_id, "Z_float", dset_id, error)
   CALL h5dget_space_f(dset_id, space_id,error)
   CALL h5dread_f(dset_id, H5T_NATIVE_DOUBLE, Z_float, data_dims, error)
   !write(0,*)Z_float

   CALL h5dopen_f(file_id, "Z_Mass_arr", dset_id, error)
   CALL h5dget_space_f(dset_id, space_id,error)
   CALL h5dread_f(dset_id, H5T_NATIVE_DOUBLE, Z_Mass_arr, data_dims, error)
   !write(0,*)Z_Mass_arr(1,:)

   CALL h5dopen_f(file_id, "Z_Mass_NmaxTime", dset_id, error)
   CALL h5dget_space_f(dset_id, space_id,error)
   CALL h5dread_f(dset_id, H5T_NATIVE_INTEGER, Z_Mass_NmaxTime, data_dims, error)
   !write(0,*)Z_Mass_NmaxTime(1,:)


   CALL h5close_f(error)

!Fill empt values with increasing value to simplify bisection nearest neighbour search
   Mmax = MAXVAL(Z_Mass_arr)
   write(0,*)"The maximum available ZAMS mass for stellar properties is",Mmax
!   write(0,*)Z_Mass_arr(:,10)
   do i=1, Ndim_Z
     do j=1, Ndim_mass
       if(Z_Mass_arr(j,i) < 0) then
         Z_Mass_arr(j,i) = 2*Mmax
       endif
     enddo
   enddo

!convert metallicities to logarithmic relative to Solar to be able to find nearest neighbour in log
  do i=1, Ndim_Z
    Z_float(i) = LOG10(Z_float(i))-Z_Metals_Solar
  enddo

  end subroutine init_SEVN


  subroutine get_SEVN(time,mass,metallicity,radius,luminosity)
      !returns values of radius and luminosity of a star based on input
      !Stellar matellicity (input is in [M/H], will be converted to log10(Mmetal/Mstar) later if needed)
      !time, i.e., stellar age (unit: Myr)
      !mass (zero age main sequence in solar masses)

      real, intent(in)  :: time, mass, metallicity
      real, intent(out) :: radius, luminosity

      real :: dist_min, dist_now
      integer :: i,j
      integer :: idx_time_max
      integer :: idx_Z, idx_M, idx_t !closest indices of metalicity, mass, and time that needs to be determined
      
      if(metallicity < -6) then !PopIII
        idx_Z = 1
      else
        if(metallicity>0.5) then !higher than table
          idx_Z = Ndim_Z
        else
          !find closest index
          idx_Z = 1
          dist_min = metallicity-Z_float(1) !should be positive
          do i=1, Ndim_Z
            dist_now = ABS(metallicity-Z_float(i))
            if(dist_now < dist_min)then
              dist_min = dist_now
              idx_Z = i
            endif
          enddo

        endif
      endif
!      write(0,*)"The nearest metallicity index is",idx_Z
!      write(0,*)"This corresponds to metallicity",Z_float(idx_Z)
!      write(0,*)"Metallicity in and out",metallicity,Z_float

      !Find closest mass index
      !array with masses: Z_Mass_arr(:,idx_Z)
      idx_M = GetNearestNeighbour_Bisection(Z_Mass_arr(:,idx_Z),Ndim_mass,mass)

      idx_time_max = Z_Mass_NmaxTime(idx_M,idx_Z)!SEVN3D is filled up to this index

      !(3,432,74,15) :: SEVN3D   (reminder for dimensions)
      idx_t = GetNearestNeighbour_Bisection(SEVN3D(1,:idx_time_max,idx_M,idx_Z),idx_time_max,time)

      radius = SEVN3D(3,idx_t,idx_M,idx_Z)
      luminosity = SEVN3D(2,idx_t,idx_M,idx_Z)
  end subroutine get_SEVN


  function GetNearestNeighbour_Bisection(array,Ndim,x) result(idx_nearest)
      integer, intent(in) :: Ndim !length of input array
      real, dimension(Ndim), intent(in) ::  array !array in which we want to find the nearest neighbour
      !we assume the input array contains increasing numbers
      real, intent(in) :: x !value to which we want to find the closest value on the grid
      integer:: idx_nearest !nearest index

      real :: dx_low, dx_high
      integer :: idx_low, idx_high, idx_mid

      if (x <= array(1)) then
        idx_nearest = 1
      elseif(x >= array(Ndim)) then
        idx_nearest = Ndim
      else!do bisection method
        idx_low = 1
        idx_high = Ndim
        do while(idx_high > idx_low + 1)!while there is still one index between them
          idx_mid = (idx_high+idx_low)/2!will be rounded to integer
          if(array(idx_mid) > x) then
            idx_high = idx_mid
          else
            idx_low = idx_mid
          endif
!          write(0,*)idx_low, idx_mid, idx_high, array(idx_mid)
        enddo
        !check which one is closer
        dx_low = x - array(idx_low)
        dx_high = array(idx_high) - x
        if(dx_low < dx_high)then
          idx_nearest = idx_low
        else
          idx_nearest = idx_high
        endif
      endif
!      write(0,*)"Input and suggested nearest neighbour:",x,array(idx_nearest)

  end function GetNearestNeighbour_Bisection


#endif

end module sevn
