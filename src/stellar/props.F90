#include "../asloth.h"

module stellar_props
  use Numerical_Parameters
  use resolution_parameters
  implicit none
  
contains

  elemental function stellar_lifetime(m,Zsun) result(LT)
      ! Fit to data from SEVN
      ! See file doc/source/FitSEVN_lifetime.pdf for details on the origin and validity ranges of the data
      ! Input: m - ZAMS mass of star in solar masses
      !        Z - stellar metallicity in units of log(Z/Zsun)
      ! Return: stellar lifeitme in yr
      real, intent(in) :: m,Zsun
      real :: LT
      real :: x1, x2, x3
      real :: b0, b1, b2, b3
      real :: Z

      if(Zsun > 0) then!supersolar
        b0 =  -0.244
        b1 =  1.81
        b2 =  -4.63
        b3 =  10.4
      elseif(Zsun < -7) then!primordial
        b0 =  -0.185
        b1 =  1.4
        b2 =  -3.75
        b3 =  9.82
      else!in between
        Z = Zsun+LogZSolar! convert to Z=log10(Mmetals/Mstar)
        b0 = -0.00297*Z**3 - 0.0441*Z**2 - 0.216*Z - 0.501
        b1 = 0.0192*Z**3 + 0.282*Z**2 + 1.37*Z + 3.42
        b2 = -0.0361*Z**3 - 0.529*Z**2 - 2.56*Z - 7.63
        b3 = 0.0181*Z**3 + 0.269*Z**2 + 1.34*Z + 12.0
      endif

      x1 = LOG10(m)
      x2 = LOG10(m)**2
      x3 = LOG10(m)**3
      LT = 10.0 ** (b0*x3 + b1*x2 + b2*x1 + b3)
  end function stellar_lifetime



  elemental function stellar_QH(m,Zsun) result(Q)
      ! Fit to data from Klessen&Glover 2022 in prep
      ! See file doc/source/FitPhotonFluxes.pdf for details on the origin and validity ranges of the data
      ! Input: m - ZAMS mass of star in solar masses
      !        Z - stellar metallicity in units of log(Z/Zsun)
      ! Return: flux of ionising photons (>13.6eV) per second
      real, intent(in) :: m,Zsun
      real :: Q
      real :: x1, x2, x3
      real :: b0, b1, b2, b3
      real :: Z

      if(Zsun > 0) then!supersolar
        b0 =  1.47
        b1 =  -8.57
        b2 =  18.3
        b3 =  35.8
      elseif(Zsun < -4) then!primordial (not exactly, but the last non-primordial model is available for Z=0.0004)
        b0 =  0.71
        b1 =  -4.38
        b2 =  10.3
        b3 =  41.3
      else!in between
        Z = Zsun+LogZSolar! convert to Z=log10(Mmetals/Mstar)
        b0 = -0.738*Z**2 - 3.50*Z - 2.52
        b1 = 2.52*Z**2 + 11.0*Z + 3.4
        b2 = -2.05*Z**2 - 6.67*Z + 12.8
        b3 = -0.0163*Z**2 - 2.65*Z + 31.0
      endif

      x1  = LOG10(m)
      x2 = LOG10(m)**2
      x3 = LOG10(m)**3
      Q = 10.0 ** (b0*x3 + b1*x2 + b2*x1 + b3)
  end function stellar_QH




#ifdef SCHAERER02
  elemental function Q_PopIII(m) result(Q)
      ! Only 9-500 Msun stars
      ! At zero metallicity, based on Schaerer02, Table6 (no mass loss)
      ! Functional form to get Q for different masses of stars
      real, intent(in) :: m
      real :: Q
      real :: x, x2

      x = LOG10(m)
      x2 = LOG10(m)**2
      Q = 10.0 ** (43.61 + 4.90*x - 0.83*x2)
  end function Q_PopIII

  elemental function lifetime_PopIII(m) result(LT)
      ! fit from Schaerer02, Table 6 (no mass loss)
      ! The fit is only provided for 9-500Msun
      ! But the fit also matches the points by Margio+01 and Ekstrom+08 down to
      ! 0.7Msun
      real, intent(in) :: m
      real :: LT
      real :: x, x2, x3

      x  = LOG10(m)
      x2 = LOG10(m)**2
      x3 = LOG10(m)**3
      LT = 10.0 ** (9.785 - 3.759*x + 1.413*x2 - 0.186*x3)
  end function lifetime_PopIII


  elemental function Q_PopII(m) result(Q)
      ! Only 7-150 Msun stars
      ! At 1/50Zsun, based on Schaerer02, Table6
      ! Functional form to get Q for different masses of stars
      real, intent(in) :: m
      real :: Q
      real :: x, x2, x3

      x = LOG10(m)
      x2 = LOG10(m)**2
      x3 = LOG10(m)**3
      Q = 10.0 ** (27.80 + 30.68*x - 14.80*x2 + 2.50*x3)
  end function Q_PopII


  elemental function lifetime_PopII(m) result(LT)
      ! fit to Stahler&Palla05, Table 1.1
      ! Points only for 0.8-60Msun
      ! fit reliable to about 200Msun
      real, intent(in) :: m
      real :: LT
      real :: x1, x2, x3
      x1 = LOG10(m)
      x2 = LOG10(m)**2
      x3 = LOG10(m)**3
      LT = 10.0 ** (10.0 - 3.68*x1 + 1.17*x2 - 0.12*x3)
    end function lifetime_PopII
#endif
!Schaerer02

  elemental function E_SN_PopII(m) result(E_SN)
    ! This routin gives the SNe energy to each mass bin 
    ! Currently all CCSNe have 1.e51 ergs
    ! All PISNe have 3.3.e52 ergs (mean value)
    ! Nomoto 2013
    real, intent(in) :: m
    real :: E_SN

    if (m .ge. M_CCSN_MIN .and. m  .lt. M_CCSN_MAX) then
      E_SN = 1.e51
    else
      E_SN = 0.
    endif
  end function E_SN_PopII

  elemental function E_SN_PopIII(m) result(E_SN)
    real, intent(in) :: m
    real :: E_SN

    !Be careful that these SN energy ranges match the mass ranges
    !for metal ejection. For example, a PopIII star of 10Msun does not produce
    !metals according to Nomoto+13
    if (m .ge. M_CCSN_MIN .and. m  .lt. M_CCSN_MAX) then
      E_SN = 1.e51
    else if (m .ge. M_PISN_MIN .and. m .lt. M_PISN_MAX) then
      E_SN = 3.3e52
    else
      E_SN = 0.
    endif
  end function E_SN_PopIII

#ifdef DUST
  elemental function Dust_Weighted_E_SN_PopII(m) result(E_SN)
    ! This routin gives the SNe energy to each mass bin 
    ! Currently all CCSNe have 1.e51 ergs
    ! All PISNe have 3.3.e52 ergs (mean value)
    ! Nomoto 2013
    real, intent(in) :: m
    real :: E_SN

    if (m .ge. M_CCSN_MIN .and. m  .lt. M_CCSN_MAX) then
      E_SN = 1.e51 * EPS_DUST_CCSN
    else
      E_SN = 0.
    endif
  end function Dust_Weighted_E_SN_PopII

  elemental function Dust_Weighted_E_SN_PopIII(m) result(E_SN)
    real, intent(in) :: m
    real :: E_SN

    !Be careful that these SN energy ranges match the mass ranges
    !for metal ejection. For example, a PopIII star of 10Msun does not produce
    !metals according to Nomoto+13
    if (m .ge. M_CCSN_MIN .and. m  .lt. M_CCSN_MAX) then
      E_SN = 1.e51 * EPS_DUST_CCSN
    else if (m .ge. M_PISN_MIN .and. m .lt. M_PISN_MAX) then
      E_SN = 3.3e52 * EPS_DUST_PISN
    else
      E_SN = 0.
    endif
  end function Dust_Weighted_E_SN_PopIII
#endif

  elemental function Momentum_SN(E_SN) result(P_SN)
    real, intent(in) :: E_SN
    real :: P_SN
    real :: n

    n = 1.
    P_SN =  2.6e5 * (E_SN/1.e51)**(16./17.) * n**(-2./17.) * 1.e5 ! Msun cm s^-1
    ! A. Gatto 2015 MNRAS
  end function Momentum_SN


  elemental function Lbol_III(m) result(Lbol)
    real, intent(in) :: m !in solar masses
    real :: Lbol !in solar luminosities
    !Mass-luminosity relation for PopIII stars
    !Based on Windhorst+18, ApJS, 234, 41
    !https://ui.adsabs.harvard.edu/abs/2018ApJS..234...41W/abstract
    !Eq. 3 & Tab. 1
    !These results are in good agreement with Schaerer02 (Tab. 3, column 2)
    if(m>100) then
        Lbol = (10**6.147) * (m/100)**1.16
    elseif(m>20) then
        Lbol = (10**6.147) * (m/100)**2.06
    elseif(m>0.8) then
        Lbol = (10**4.7082) * (m/20)**3.2
    else
        Lbol = 0!unrealistic PopIII mass
    endif
  end function Lbol_III



  elemental function Lbol_II(m) result(Lbol)
    real, intent(in) :: m !in solar masses
    real :: Lbol !in solar luminosities
    !Mass-luminosity relation for PopII stars
    !Inspired by Wikipedia: https://en.wikipedia.org/wiki/Mass%E2%80%93luminosity_relation
    !Based on:
    !Salaris, Maurizio; Santi Cassisi (2005). Evolution of stars and stellar populations. John Wiley & Sons. pp. 138–140. ISBN 978-0-470-09220-0.
    !Duric, Nebojsa (2004). Advanced astrophysics. Cambridge University Press. p. 19. ISBN 978-0-521-52571-8.
    !"The Eddington Limit (Lecture 18)" jila.colorado.edu. 2003. Retrieved 2019-01-22.

    if(m > 55) then
        Lbol = 3.2e4*m
    elseif(m>2) then
        Lbol = 1.4 * (m**3.5)
    elseif(m > 0.43) then
        Lbol = m**4.0
    elseif(m>0.08) then
        Lbol = 0.23 * (m**2.3)
    else
        !Brown Dwarf?
        Lbol = 0
    endif

  end function Lbol_II


end module stellar_props
