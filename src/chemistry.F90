! This module uses part of the SGChem primordial chemistry network of Arepo (code and comments)
! The sources for the chemical reaction rates can be found in
! Glover, S. C. O. 2015, MNRAS, 451, 2082, doi: 10.1093/mnras/stv1059
! Glover, S. C. O., & Abel, T. 2008, MNRAS, 388, 1627, doi: 10.1111/j.1365-2966.2008.13224.x
! Please contact Simon Glover for permissions and usage of these chemistry
! routines
module chemistry
  use numerical_parameters
  implicit none
  private :: ch22, ch21, ph4, cl4, cl64 ! chemistry and cooling rate coefficients

  ! helium nucleon number relative to hydrogen
  real, private, parameter :: ABHE = Y_Helium/X_Hydrogen/4.0 
  real, private, parameter :: x_ion = 2.0e-4 ! residual ionization fraction
contains

  ! Function that computes the typical times-scale on which hot medium should be
  ! converted into cold medium. This is only used for halos with Tvir < 1e4 K,
  ! and also only for halos where this time-scale is > t_dyn. For these halos,
  ! we assume that the limiting timescale is the H2 formation timescale. For halos
  ! with Tvir > 1e4 K or with t_cool < t_dyn, we assume that the limiting timescale
  ! for making cold gas available for star formation is the dynamical timescale
  function t_hot2cold(n,T,z) result(res)
      real, intent(in) :: n, T, z
      real :: res
      res = fH2_crit(n, T, z)/H2_form_rate(n, T, z)
      return
  end function 

  ! H2 formation rate via the H- pathway. Assumes H- formation is the limiting step,
  ! but also accounts for destruction of H- by CMB
  function H2_form_rate(n, T, z) result(res)
      real, intent(in) :: n, T, z
      real :: res
      res = ch21(T)*x_ion*n/(1.0+ph4(z)/(ch22(T)*n))
      return
  end function


  ! Critical H2 fraction for cooling within 20% of a Hubble time (~ halo dynamical time)
  ! This formula is from Glover (2013) [arXiv:1209.2509]: it is Eq. 36 in that paper, and
  ! details of its derivation & underlying assumptions can be found there
  function fH2_crit(n, T, z) result(res)
      real, intent(in) :: n, T, z
      real :: res
      res = 5.2e-32*T/H2_cooling_rate(n, T)*((1.0+z)/10.0)**1.5
      return
  end function

  function H2_cooling_rate(n, T) result(res)
      real, intent(in) :: n, T
      real :: res
      res = cl4(T)*n + cl64(T)*n*ABHE
      return
  end function

  ! -- fits of rate coefficients below -- !

  ! Formation of H_2 by associative detachment of H- (H + H- => H_2 + e)
  function ch22(temp) result(res)
      real, intent(in) :: temp
      real :: res
      res = 1.35e-9 * (temp**9.8493e-2 + 3.2852e-1  &
      &       * temp**5.5610e-1 + 2.771e-7 * temp**2.1826e0) & 
      &       / (1e0 + 6.191e-3 * temp**1.0461e0 &
      &       + 8.9712e-11 * temp**3.0424e0 &
      &       + 3.2576e-14 * temp**3.7741e0)
      return

  end function

  ! Formation of H- by photo-attachment (H + e => H-)
  function ch21(temp) result(res)
      real, intent(in) :: temp
      real :: res
      real :: logT
      logT = log10(temp)
      if (temp .le. 6000) then
        res = 10e0**(-17.845e0 &
        & + 0.762e0  *  logT &
        & + 0.1523e0 * logT**2 &
        & - 3.274e-2 * logT**3 )
      else
        res = 10e0**(-16.4199e0 &
        & + 0.1998e0* logT**2 &
        & - 5.447e-3  * logT**4 &
        & + 4.0415e-5 * logT**6)
      endif
  end function



  ! At high redshift, we also need to account for dissociation of H- by the CMB.
  ! There are two contributions: one from the thermal spectrum and a second from
  ! the non-thermal photons produced during recombination (see Hirata & Padmanabhan, 2006).
  function ph4(z) result(res)
      real, intent(in) :: z
      real :: res
      real :: CMB_temp
      CMB_temp = 2.726e0 * (1e0 + z)
      ! Thermal (fit from Galli & Palla 1998):
      res = 0.11e0 * CMB_temp**2.13e0 * exp(-8.823e3 / CMB_temp)
      ! Non-thermal (fit from Coppola et al 2011):
      res = res + 8.0e-8 * CMB_temp**1.3e0 * exp(-2.3e3 / CMB_temp)
      return
  end function

  !
  ! (cl64) -- H2-He, low density rate: based on FRZ98, BFD99; assumes 3:1 o:p ratio
  !
  function cl64(temp) result(res)
      real, intent(in) :: temp
      real :: res
      real :: logT3
      logT3 = log10(temp/1000.0)
      if (temp .lt. 1e1) then
        res = 0e0
      elseif (temp .lt. 1e4) then
        res = 1e1**(-23.689237e0 &
        &  + 2.1892372  * logT3 &
        &  - 0.81520438 * logT3**2 &
        &  + 0.29036281 * logT3**3 &
        &  - 0.16596184 * logT3**4 &
        &  + 0.19191375 * logT3**5)
      else
        res = 1e1**(-23.689237 &
        &   + 2.1892372 &
        &   - 0.81520438 &
        &   + 0.29036281 &
        &   - 0.16596184 &
        &   + 0.19191375)
      endif
  end function

  ! 
  ! (cl4) -- H2-H, low density rate: based on WF07, assumes 3:1 o:p ratio
  !     
  ! Data spans range 100 < T < 6000K, but is well-behaved up to 10000K;
  ! at higher T, we assume rate remains constant (but note that at these
  ! temperatures, atomic cooling dominates)
  !
  function cl4(temp) result(res)
      real, intent(in) :: temp
      real :: res
      real :: logT3
      logT3 = log10(temp/1000.0)

      if (temp .lt. 1e1) then
        res = 0.0
      elseif (temp .lt. 1e2) then
        res = 1e1**(-16.818342 &
        &  + 37.383713 * logT3 &
        &  + 58.145166 * logT3**2 &
        &  + 48.656103 * logT3**3 &
        &  + 20.159831 * logT3**4 &
        &  + 3.8479610 * logT3**5)
      elseif (temp .lt. 1e3) then
        res = 1e1**(-24.311209 &
        &  + 3.5692468 * logT3 &
        &  - 11.332860 * logT3**2 &
        &  - 27.850082 * logT3**3 &
        &  - 21.328264 * logT3**4 &
        &  - 4.2519023 * logT3**5)
      elseif (temp .lt. 1e4) then
        res = 1e1**(-24.311209 &
        &  + 4.6450521 * logT3 &
        &  - 3.7209846 * logT3**2 &
        &  + 5.9369081 * logT3**3 &
        &  - 5.5108047 * logT3**4 &
        &  + 1.5538288 * logT3**5)
      else
        res = 1e1**(-24.311209 &
        &   + 4.6450521 &
        &   - 3.7209846 &
        &   + 5.9369081 &
        &   - 5.5108047 &
        &   + 1.5538288)
      endif
  end function

end module
