#include "asloth.h"
module star_formation
  use bubble_tree
  use Defined_Types ! defined_types.F90
  use Numerical_Parameters
  use resolution_parameters
  use metal_functions
  use snowplow
  use random_object
  use SF_routines
  use input_files
  use crit_masses, only: get_T_vir
  use utility, only: iunit_list
  use virial_radius, only: RVIR_MPC
#ifdef METAL_YIELDS
  use IGM_Z
  use MetalMix_dZ
#endif
  implicit none

contains

  subroutine prepare_node(P_Node, M_star_max, is_ion)
      type(TreeNode), intent(inout) :: P_Node ! parent node
      type(TreeNode), pointer :: C_Node ! child nodes
      real, intent(out) :: M_star_max
      logical, intent(out) :: is_ion
      real :: Mh_max, R_ion_last, R_enr_last

      C_Node => P_Node%child

      call P_Node%set_baryons(C_Node)

      ! setting up initial values for star formation
      ! inherit from the first child
      Mh_max= C_Node%mhalo
      is_ion = C_Node%i_ion
      R_enr_last = C_Node%r_en**3 ! enrichment from previous steps
      R_ion_last = C_Node%R_ion**3 ! ionisation form previous steps
      M_star_max = C_Node%M_starII
      P_Node%N_PISN = C_Node%N_PISN
      P_Node%PISN_mono = C_Node%PISN_mono

      ! walk through the other childs
      do while(associated(C_Node%sibling))
        C_Node => C_Node%sibling
        ! compute total old properties before star formation
        call P_Node%add_baryons(C_Node)
        ! compute properties of largest node
        R_enr_last = R_enr_last + C_Node%r_en**3 ! for enrichment
        R_ion_last = R_ion_last+ C_Node%R_ion**3
        P_Node%N_PISN = P_Node%N_PISN + C_Node%N_PISN
        P_Node%PISN_mono = P_Node%PISN_mono + C_Node%PISN_mono

        if (C_Node%mhalo .gt. Mh_max) then
          Mh_max = C_Node%mhalo
          is_ion = C_Node%i_ion
        end if
      end do
      P_Node%i_ion = is_ion !set from most massive child
      if(P_Node%mhalo .ge. Atomic_Cooling_mass(P_Node%jlevel)) then
        !If halo is sufficiently massive, it is not influenced by external ionising radiation
        !For simplicity, we do not follow the more sophisticated treatment by Visbal+17 here,
        !because the effect is small and the computational effort large.
        P_Node%i_ion = .false.
      endif

      R_ion_last = R_ion_last**(1.0/3.0)*(1+zlev(C_Node%jlevel))/(1+zlev(P_Node%jlevel)) ! cosmic expansion
      R_enr_last = R_enr_last**(1.0/3.0)*(1+zlev(C_Node%jlevel))/(1+zlev(P_Node%jlevel)) ! cosmic expansion
      P_Node%R_ion = R_ion_last
      P_Node%r_en = max(P_Node%r_en, R_enr_last)
#ifdef METAL_YIELDS
      call check_m_metals(P_Node)
      ! We only call check_m_metals once.
      call inherit_IGM_Z(P_Node)
#endif

  end subroutine




  subroutine SF_step(Node)
      type(TreeNode), target, intent(inout) :: Node
      type(TreeNode), pointer :: P_Node, C_Node ! parent node, child nodes
      type(TreeNode), pointer :: Dummy_Node

      type(rng) :: random_thisNode
      real :: M_star_max
      real :: M_star_old, M_star_old_II, M_star_old_III ! information before the SF
      real :: del_M_hot, del_M_hot_now, V_circ
      real :: t_dyn, del_t, coldgas_tff, t_cool
      real :: dt ! in s

      real :: M_star_form, M_star_form_II, M_star_form_III, M_out
      real :: R_vir_current, v_current
      real :: V_dyn
      real :: M_out_cold, M_out_hot, M_out_max

      integer :: j, ii ! for do loop
      integer :: pop_now !!which population, depending on the metallicity
      type(population) :: pop
      real :: t_current
      real :: TotalMassNewStars, TotalSNeMomentumThisStep, TotalSNeEnergyThisStep, MHeatThisStep
      real :: n_hot ! hot gas mass density
      real :: dm_concentration 
      !remember if there were SNe:
      !because TotalSNeEnergyThisStep gets consumed and may be zero again
      logical :: if_SN

      real :: T_vir  ! for atomic cooling limit
      real, dimension(N_ELEMENTS) :: Z_temp!only needed as function input without metals
      real, dimension(N_ELEMENTS) :: TotalMetalEjectaThisStep!needed as function input without metals?
#ifdef METAL_YIELDS
      real :: frac_remain, frac_out
      real, dimension(N_ELEMENTS) :: Z_ThisStep
      !gas metallicity in this step, assuming homogeneous mixing between H and metals
      real :: Mgas_firststep, dZ_now
      real, dimension(N_ELEMENTS) :: MetalsSum
      logical :: i_ext!if externally enriched
#endif
#ifdef DUST
      real :: t_dust_grow
      real :: M_dust_dest ! mass of dust destruction by SN shock
      real :: M_dust_heatthisstep ! dust mass moving from cold to hot phase
      real :: L_gal ! bolometric luminosity of galaxy that triggers dust ejection
#endif
      ! can't include these in ifdef-clause, as they are embedded in the existing functions
      real :: t_dust, t_dust_rad_cold, t_dust_rad_hot
      real :: TotalDustEjectaThisStep, DustWeightedTotalSNeEnergyThisStep
      real :: M_out_cd ! outflow due to cold dust

      real :: this_SFR
      logical :: is_ion

      real :: N_ion_III,  N_ion_II, N_ion
      real :: dN_ion_III, dN_ion_II
      type(Stars_HighMass), pointer :: NextStarToOutput => null()
      real :: mass_out, age
      integer :: id_out, pop_out, num_out
#ifdef DEBUG
      real :: time_start, time_end

      call CPU_TIME(time_start)
#endif

      P_Node => Node%parent
      C_Node => P_Node%child
      !If Node has other siblings with i_SF == True,
      !SF is only triggered for the first sibling with i_SF == True
      do while (.not. C_Node%i_SF)
        C_Node => C_Node%sibling
      end do
      if (.not. associated(C_Node, Node)) return ! Makes sure we only call this routine once for each halo
      P_Node%i_SF = .true. !this is needed to make sure that P_node calls sf_step too under all circumstances
      call random_thisNode%init(P_Node%id) 
      call prepare_node(P_Node, M_star_max, is_ion)
      pop_now = 0 !initialise to catch problems
      pop = popII
#ifdef METAL_YIELDS
      Z_ThisStep(:) = -20
      dZ_now = 0.0
#endif
      N_ion_III = 0.0
      N_ion_II  = 0.0
      N_ion     = 0.0
#ifdef DUST
      TotalDustEjectaThisStep = 0.0
      DustWeightedTotalSNeEnergyThisStep = 0.0
#endif
      ! We trace massive Pop II stars with pointers, inherit the linked list from child nodes here
      C_Node => P_Node%child

      P_Node%HighStars_first => C_Node%HighStars_first
      P_Node%HighStars_last => C_Node%HighStars_last
      ! walk through the other childs
      do while(associated(C_Node%sibling))
        C_Node => C_Node%sibling
        if (associated(C_Node%HighStars_first)) then
          if (associated(P_Node%HighStars_first)) then
            P_Node%HighStars_last%next => C_Node%HighStars_first ! the first star of C_Node is the next of the last star of P_Node
            C_Node%HighStars_first%prev => P_Node%HighStars_last
            P_Node%HighStars_last => C_Node%HighStars_last ! the last star of P_Node is now also the last of C_Node
          else
            P_Node%HighStars_first => C_Node%HighStars_first ! P_Node has no stars yet, the first is star is the first of C_Node
            P_Node%HighStars_last => C_Node%HighStars_last ! the last star of P_Node is now also the last of C_Node
          endif
        endif
      end do ! end walking through childs

      j = Node%jlevel
      del_t = tlev(j-1)-tlev(j) ! s
      R_vir_current = RVIR_MPC(P_Node%M_peak,zlev(j-1))
      ! We assume NFW dark matter profile, obtain the concentration here. We need this for gas binding energy calculation.
      call dm_concen(P_Node%M_peak, zlev(j-1), dm_concentration)
      if (dm_concentration > 100. .or. dm_concentration < 1.) then
        write(stdout, *) "mass, z, c: ", P_Node%M_peak, zlev(j-1), dm_concentration
      endif

      ! The stellar feedback works differently depending on virial temperature (heating of the gas or blowing gas away).
      V_circ = SQRT(G*P_Node%M_peak/R_vir_current)*1.0e5 !circular velocity in cm/s
      T_vir  = get_T_vir(P_Node%M_peak, zlev(j-1)) ! virial temperature in K

      ! t_dyn determines how efficient the hot gas is cooling down.
      ! rel_tff (from coldgas_tff) determines how efficient cold gas is converted to stars, density dependent.

      ! for calculation of total stellar mass forming in one substep
      del_M_hot = OMEGA_b / OMEGA_m * P_node%mhalo - P_node%M_hot - P_node%M_cold - P_node%M_starII - &
          P_Node%M_out - P_node%M_starIII
      del_M_hot = max(del_M_hot, 0.0)
      if (P_Node%i_sub .or. is_ion) then
        del_M_hot = 0.0
      endif

      ! old stellar contents
      M_star_old = P_Node%M_starII + P_Node%M_starIII
      M_star_old_II  = P_Node%M_starII
      M_star_old_III = P_Node%M_starIII

      ! prevent star formation if halo is ionized
      ! or if the matter budget overshoots (i.e. del_M_hot negative, can happen due to double-counting of matter in mergers)
#ifdef METAL_YIELDS
      Mgas_firststep = P_node%M_cold + P_Node%M_hot
      call check_m_metals(P_Node)
      MetalsSum = &
          &P_Node%m_metals(1,:)+P_Node%m_metals(2,:)+&
          &P_Node%m_metals(3,:)+P_Node%m_metals(4,:)
      if ( Mgas_firststep > 0. .and. &
          &(MetalsSum(N_ELEMENTS)/(Mgas_firststep*X_Hydrogen)) > 1.e-20 ) then
        do ii=1, N_ELEMENTS
          Z_ThisStep(ii) =  get_metallicity( Mgas_firststep*X_Hydrogen, MetalsSum(ii),ii)!Metallicity assuming homogeneous mixing
        enddo
      else
        Z_ThisStep(:) = -20.0
      endif
      ! Calculate dZ before star formation because it should not include stellar mass formed in this timestep
      dZ_now = calc_dZ(P_Node%m_metals(3,N_ELEMENTS)+P_Node%m_metals(1,N_ELEMENTS),random_thisNode,Z_ThisStep(N_ELEMENTS))
      if (P_Node%m_metals(3,N_ELEMENTS)+P_Node%m_metals(1,N_ELEMENTS) .lt. 1.0e-15) then
        i_ext = .true.
      else
        i_ext = .false.
      endif
#endif
      t_current = tlev(j)
      do while(t_current < tlev(j-1))
        ! substeps star formation loop
        ! These are step-wise quantities. Reset every step.
        TotalSNeMomentumThisStep = 0.
        TotalMassNewStars = 0.
        M_out_hot = 0.
        TotalMetalEjectaThisStep(:) = 0.
#ifdef METAL_YIELDS
        MetalsSum = 0.
        !Which population shall form?
        !In the nomenclature of asloth, we distinguish between PopIII and PopII stars.
        !PopII stars in asloth also include PopI stars,
        !but the exact distinction is not relevant for all current applications of asloth.
        if(MetalPoor(P_Node,Z_ThisStep(N_ELEMENTS),dZ_now))then
          pop_now = 3
        else
          pop_now = 2
        endif
#else
        if (P_Node%i_enr) then
          pop_now = 2
        else
          pop_now = 3
        endif
#endif
        if (pop_now .ne. pop%pop) then
          if (pop_now .eq. 2) then
            pop = PopII
          else if (pop_now .eq. 3) then
            pop = PopIII
          else
            write(*,*) "failed to find correct population", pop%pop
            stop
          endif
        endif

#ifdef DUST
        ! obtain the timescales relevant for dust
        ! FIXME L_gal is total radiated energy over dt, divided by dt.
        ! This depends on dt while dt depends on t_dust_rad, creating circular dependency.
        ! for now we put the same timestep as before when calculating L_gal, plus 1yr to avoid division by 0.
        L_gal = get_L_gal(P_Node, t_current, dt+YEAR_cgs)
        call dust(P_Node, zlev(j-1), R_vir_current, dm_concentration, 10**(Z_ThisStep(N_ELEMENTS)+dZ_now),&
                 &L_gal, t_dust_grow, t_dust_rad_hot, t_dust_rad_cold)
        t_dust = min(t_dust_grow, t_dust_rad_hot, t_dust_rad_cold)
#else
        ! make t_dust huge that it doesn't affect the timestep
        t_dust = 1.d50
        ! also make t_dust_rad... huge to (effectively) switch off radiation feedback onto gas
        t_dust_rad_hot = 1d50
        t_dust_rad_cold = 1d50
#endif


        ! Recalculate the timescales in each step since some are gas density-dependent.
        ! t_dyn determines how efficient the hot gas is cooling down.
        ! rel_tff (from coldgas_tff) determines how efficient cold gas is converted to stars, density dependent.
        call TimeScaleDetermination(P_Node, V_dyn, t_dyn, n_hot,&
            &coldgas_tff, R_vir_current, dm_concentration, T_vir, t_cool)

        !!! TIME SCALES !!!
        if (T_vir .lt. 1.e4) then
          ! Our estimate of t_cool here will be too small for minihalos with high Tvir, so arguably we should also
          ! add a check that t_cool > t_dyn. However, only effect here of passing in small t_cool is a small loss
          ! of efficiency, since we'll get an adaptive dt that is too small; otherwise, this should be harmless
          ! Therefore, let's leave it as is for now...
          dt = dt_adaptive(P_Node,coldgas_tff,t_cool,t_dust,del_M_hot,del_t,tlev(j-1)-t_current,pop_now,t_current)
        else
          dt = dt_adaptive(P_Node,coldgas_tff,t_dyn,t_dust,del_M_hot,del_t,tlev(j-1)-t_current,pop_now,t_current)
        endif
        t_current = t_current + dt
        del_M_hot_now = del_M_hot*dt/del_t

        !!! Star formation
        M_star_form = M_star_dot(P_Node%M_cold,coldgas_tff,pop_now, t_current) * dt

        !Jeans mass at 200K-ish at 1e4/ccm: ~2000Msun (Yoshida+06 Astrophys.J.652:6-25)
        if (P_node%M_cold .lt. 2000 ) M_star_form = 0.0 !not Jeans unstable !ideally:metallicity-dependent Jeans mass

        if (M_star_form .gt. 0) then
#ifdef METAL_YIELDS
          Z_temp = Z_ThisStep(:)+dZ_now
          !Note: Z_ThisStep is array, but dZ_now only one number that is applied to all array elements
#else
          Z_temp = 0
#endif
          call IMFSampling(P_Node, TotalMassNewStars, M_star_form, t_current, random_thisNode, Z_temp, pop)
        endif !!! finish star formation

        if(TotalMassNewStars > P_Node%M_cold) then
          !this can occur for high SFR because of stochstic IMF sampling
          write(0,*)"WARNING: TotalMassNewStars > M_cold"
          write(0,*)P_Node%M_cold,TotalMassNewStars,pop_now
          write(0,*)"TEMPORARY FIX: pretend TotalMassNewStars = M_cold"
          TotalMassNewStars = P_Node%M_cold
        endif

#ifdef METAL_YIELDS
        ! Metal outflows
        P_Node%m_metals(4,:) = P_Node%m_metals(4,:) + del_M_hot_now * P_Node%IGM_Z(:)
#endif
        call get_N_ion(P_Node, t_current, dt, dN_ion_II, dN_ion_III)
        N_ion_II  = N_ion_II  + dN_ion_II
        N_ion_III = N_ion_III + dN_ion_III
        N_ion = N_ion_II + N_ion_III
        !!! Only work on feedback when there is still gas left after SF
        MHeatThisStep = 0.0
        M_out_cold = 0.0
        if_SN = .False.
        if ( (P_Node%M_hot + del_M_hot_now + P_Node%M_cold - TotalMassNewStars) > 0. ) then 
          call HMS_Feedback(P_Node, t_current, TotalMetalEjectaThisStep, TotalSNeEnergyThisStep,&
              &MheatThisStep, dt, random_thisNode, TotalDustEjectaThisStep, DustWeightedTotalSNeEnergyThisStep)

          if (T_vir .lt. 1.e4) then !below the atomic cooling limit ionizing radiation heating causes outflows
            M_out_cold = MHeatThisStep
            MHeatThisStep = 0.0
          else
            M_out_cold = 0.
          endif

#ifdef OUTPUT_ALL_Z
          if(TotalMassNewStars>0)then
            write(iunit_list(7),'(5E12.4)')&
              zlev(P_Node%jlevel), TotalMassNewStars, P_Node%Mhalo,&
              Z_ThisStep(N_ELEMENTS)+dZ_now, dt
          endif
#endif

#if defined(OUTPUT_GAS_BRANCH) && defined(METAL_YIELDS)
          !moved to here because TotalSNeEnergyThisStep gets consumed afterwards
          if(P_Node%id == NextOutputID)then
#ifdef DUST
            write(iunit_list(4),'(I10,E16.8,16E12.4)')&
                P_Node%id,t_current,zlev(P_Node%jlevel),TotalMassNewStars,P_Node%Mhalo,&
                P_Node%M_starII,P_Node%m_hot,P_Node%m_cold, P_Node%m_out,&
                P_Node%m_dust_hot,P_Node%m_dust_cold,&
                TotalMetalEjectaThisStep(N_ELEMENTS), TotalSNeEnergyThisStep, MHeatThisStep,&
                TotalSNeMomentumThisStep, P_Node%M_starIII,&
                Z_ThisStep(N_ELEMENTS)+dZ_now,L_gal
            write(iunit_list(9),*)&
                zlev(P_Node%jlevel), t_dyn, coldgas_tff, t_dust_grow, t_dust_rad_cold, t_dust_rad_hot
#else
            write(iunit_list(4),'(I10,E16.8,14E12.4)')&
                P_Node%id,t_current,zlev(P_Node%jlevel),TotalMassNewStars,P_Node%Mhalo,&
                P_Node%M_starII,P_Node%m_hot,P_Node%m_cold, P_Node%m_out,&
                TotalMetalEjectaThisStep(N_ELEMENTS), TotalSNeEnergyThisStep, MHeatThisStep,&
                TotalSNeMomentumThisStep, P_Node%M_starIII,&
                Z_ThisStep(N_ELEMENTS)+dZ_now,log10(P_Node%IGM_Z(N_ELEMENTS))
#endif
          endif
#endif
          if (TotalSNeEnergyThisStep > 0.) then
            if_SN = .True.
            call SNeDrivenOutflow(P_Node, R_vir_current, dm_concentration, TotalSNeEnergyThisStep, &
                &del_M_hot_now, TotalMassNewStars, M_out_hot, M_out_cold)
          else
            if_SN = .False.
          endif
        endif !!! finish feedback

        !outflow mass owing to dust ejection
#ifdef DUST
        M_out_cd = P_Node%M_cold*dt/t_dust_rad_cold
#else
        M_out_cd = 0.0
#endif

        if(P_Node%M_cold < TotalMassNewStars + MHeatThisStep + M_out_cd) then
          MheatThisStep = P_node%M_cold - TotalMassNewStars - M_out_cd
        endif

        !they are not included in adaptive timestep calculation
        !but they are "protected" against getting too small:
        M_out_max = (P_Node%M_cold - mheatthisstep - TotalMassNewStars - M_out_cd)*0.99!COLD

        if(M_out_cold .gt. M_out_max) then!we want to eject more cold gas than what is available
          M_out_hot = M_out_hot + (M_out_cold - M_out_max)!remove additional hot gas
          M_out_cold = M_out_max
        endif

        if (T_vir .lt. 1.e4 .and. t_cool .gt. t_dyn) then
          M_out_max = (P_Node%M_hot*(1 - dt/t_cool) + del_M_hot_now + mheatthisstep)*0.99!HOT
        else
          M_out_max = (P_Node%M_hot*(1 - dt/t_dyn) + del_M_hot_now + mheatthisstep)*0.99!HOT
        endif
        M_out_hot = min(M_out_hot, M_out_max)

#ifdef DUST
        if (P_Node%M_cold > 0.0d0) then
          M_dust_dest = DustWeightedTotalSNeEnergyThisStep/1e51*6.8e3/(vsh_dust_dest/1e7)**2 * P_Node%M_dust_hot/P_Node%M_hot
          M_dust_heatthisstep = mheatthisstep*P_Node%M_dust_cold/P_Node%M_cold
          !FIXME IGM accretion?
          if (T_vir .lt. 1.e4) then
            if (t_cool .gt. t_dyn) then
               P_Node%M_dust_cold = P_Node%M_dust_cold + P_Node%M_dust_hot*dt/t_cool&
                       &-(totalmassnewstars+m_out_cold)*P_Node%M_dust_cold/P_Node%M_cold&
                       &+P_Node%M_dust_cold*dt/t_dust_grow&
                       &-P_Node%M_dust_cold*dt/t_dust_rad_cold
               P_Node%M_dust_hot = P_Node%M_dust_hot * (1 - dt/t_cool) + TotalDustEjectaThisStep&
                       &-m_out_hot*P_Node%M_dust_hot/P_Node%M_hot - M_dust_dest&
                       &-P_Node%M_dust_hot*dt/t_dust_rad_hot
            else
               P_Node%M_dust_cold = P_Node%M_dust_cold + P_Node%M_dust_hot*dt/t_dyn&
                       &-(totalmassnewstars+m_out_cold)*P_Node%M_dust_cold/P_Node%M_cold&
                       &+P_Node%M_dust_cold*dt/t_dust_grow&
                       &-P_Node%M_dust_cold*dt/t_dust_rad_cold
               P_Node%M_dust_hot = P_Node%M_dust_hot * (1 - dt/t_dyn) + TotalDustEjectaThisStep&
                       &-m_out_hot*P_Node%M_dust_hot/P_Node%M_hot - M_dust_dest&
                       &-P_Node%M_dust_hot*dt/t_dust_rad_hot
            endif
          else
            P_Node%M_dust_cold = P_Node%M_dust_cold + P_Node%M_dust_hot*dt/t_dyn&
                    &-M_dust_heatthisstep - (totalmassnewstars+m_out_cold)*P_Node%M_dust_cold/P_Node%M_cold&
                    &+P_Node%M_dust_cold*dt/t_dust_grow&
                    &-P_Node%M_dust_cold*dt/t_dust_rad_cold
            P_Node%M_dust_hot = P_Node%M_dust_hot * (1 - dt/t_dyn) + TotalDustEjectaThisStep&
                    &+M_dust_heatthisstep - m_out_hot*P_Node%M_dust_hot/P_Node%M_hot - M_dust_dest&
                    &-P_Node%M_dust_hot*dt/t_dust_rad_hot
          endif
        endif
        P_Node%M_dust_cold = max(0.0, P_Node%M_dust_cold)
        P_Node%M_dust_hot = max(0.0, P_Node%M_dust_hot)
#endif

        if(P_Node%M_cold < TotalMassNewStars) then
          write(0,*)"WARNING: M_cold < TotalMassNewStars"
          write(0,*)P_Node%M_cold,TotalMassNewStars,pop_now
        endif

        if (T_vir .lt. 1.e4) then
          if (t_cool .gt. t_dyn) then
             p_node%m_cold = p_node%m_cold + (p_node%m_hot/t_cool) * dt &
                           & - totalmassnewstars - m_out_cold - m_out_cd
             p_node%m_hot  = p_node%m_hot * (1 - dt/t_cool) + del_m_hot_now &
                           & - m_out_hot - p_node%m_hot*dt/t_dust_rad_hot
          else
             p_node%m_cold = p_node%m_cold + (p_node%m_hot/t_dyn) * dt &
                           & - totalmassnewstars - m_out_cold - m_out_cd
             p_node%m_hot  = p_node%m_hot * (1 - dt/t_dyn) + del_m_hot_now &
                           & - m_out_hot - p_node%m_hot*dt/t_dust_rad_hot
          endif
        else
          p_node%m_cold = p_node%m_cold + (p_node%m_hot/t_dyn) * dt &
                         &- totalmassnewstars - m_out_cold - mheatthisstep - m_out_cd
          p_node%m_hot  = p_node%m_hot * (1 - dt/t_dyn) + del_m_hot_now - m_out_hot + mheatthisstep - p_node%m_hot*dt/t_dust_rad_hot
        endif
        M_out = M_out_hot + M_out_cold
        P_Node%M_out = P_Node%M_out + M_out

        !!! updating gas content
!        write(*,*) P_node%M_cold,P_node%M_hot, m_out_cold, m_out_hot, totalmassnewstars, mheatthisstep,&
!        &p_node%m_cold*dt/t_dust_rad_cold, p_node%m_hot*dt/t_dust_rad_hot
        call assert((min(P_node%M_cold,P_node%M_hot) .ge. 0), "Negative gas mass")

        if (P_Node%M_hot + P_Node%M_cold + M_out + TotalMassNewStars .gt. 0) then
          ! changing baryon content. Compute fracion of metals that
          ! - remain in the gas phase
          frac_remain =  (P_Node%M_hot + P_Node%M_cold) / &
              &(P_Node%M_hot + P_Node%M_cold + M_out + TotalMassNewStars)
          ! - get added to the outflows
          frac_out = (M_out) / (P_Node%M_hot + P_Node%M_cold + M_out + TotalMassNewStars)
        else
          ! nothing happens
          frac_remain = 1.0 
          frac_out = 0.0
        endif

#ifdef METAL_YIELDS
        ! applying the mixing factors to metals already present
        MetalsSum = &
            &P_Node%m_metals(1,:)+P_Node%m_metals(2,:)+&
            &P_Node%m_metals(3,:)+P_Node%m_metals(4,:)
        P_Node%m_metals(5,:) = P_Node%m_metals(5,:) + frac_out*MetalsSum 

        P_Node%m_metals(1,:) = MAX(P_Node%m_metals(1,:) * frac_remain,1e-20)
        P_Node%m_metals(2,:) = MAX(P_Node%m_metals(2,:) * frac_remain,1e-20)
        P_Node%m_metals(3,:) = MAX(P_Node%m_metals(3,:) * frac_remain,1e-20)
        P_Node%m_metals(4,:) = MAX(P_Node%m_metals(4,:) * frac_remain,1e-20)

        MetalsSum = &
            &P_Node%m_metals(1,:)+P_Node%m_metals(2,:)+&
            &P_Node%m_metals(3,:)+P_Node%m_metals(4,:)

        if ( (P_Node%M_cold+P_Node%M_hot)*X_Hydrogen > 1.e-20 ) then
          if ( MetalsSum(N_ELEMENTS)/((P_Node%M_cold+P_Node%M_hot)*X_Hydrogen) > 1.e-20) then
            do ii=1,N_ELEMENTS
              Z_ThisStep(ii) = get_metallicity((P_Node%M_cold+P_Node%M_hot)*X_Hydrogen, MetalsSum(ii),ii)
            enddo
          else
            Z_ThisStep(:) = -20.
          endif
        endif
        if (P_Node%m_metals(3,N_ELEMENTS)+P_Node%m_metals(1,N_ELEMENTS) .ge. 1.0e-15 .and. i_ext) then
          !update dZ after halo switched from external to internal enrichment
          i_ext = .False.
          dZ_now = calc_dZ(P_Node%m_metals(3,N_ELEMENTS)+P_Node%m_metals(1,N_ELEMENTS),random_thisNode,Z_ThisStep(N_ELEMENTS))
        endif
#endif
        if (if_SN) then
          P_node%i_enr = .True.
        endif

      enddo ! end star formation loop

      !update internal enrichment
      !For simplicity, we do not exactly trace the original population of SNe (Pop II or III)
      !but the dominant stellar mass when the SN explode
      !This is for bookkeeping only and has no backreacting on the model.
      !Can be improved in the future


      ! enrichment 
      P_Node%r_en = max(R_vir_current, P_Node%r_en)
      v_current = V_plow_ad(P_Node%M_out, zlev(j), P_Node%r_en, R_vir_current) ! Snowplow expansion model
      if (v_current .ge. 10.0e5) then ! only expand if the veolicty is faster the 10 km/s (ambient speed of sound)
        P_Node%r_en = P_Node%r_en + v_current*(del_t)/MPC_cgs
      endif

      ! Tracking masses, SFRs
      M_star_form = P_node%M_starII+P_node%M_starIII - M_star_old!star forming mass with metals not yet included
      M_star_form_II = P_node%M_starII - M_star_old_II
      M_star_form_III = P_node%M_starIII - M_star_old_III
      this_SFR = M_star_form / (del_t / YEAR_cgs)
      
      !$OMP CRITICAL
      !This OMP directive identifies a section of code that must be executed by a single thread at a time.

      ! We want to know how many Pop II progenitor branches there are for each base nodes.
      if ( P_Node%firstPopIISF .eqv. .True. .and. M_star_form > 0. ) then
        P_Node%firstPopIISF = .False.
        if ( M_star_old == 0. ) then
          P_Node%base%num_PopIIProgenitor = P_Node%base%num_PopIIProgenitor + 1
        endif
      endif

      ! Note: If our halo has both Pop III and Pop II massive stars, we use the Pop II value of F_esc for both
      ! We only use the Pop III F_esc for halos that solely have Pop III stars
     
      P_Node%R_ion = R_ion_ad(N_ion*pop%F_esc, P_Node%R_ion, zlev(j), del_t, P_Node%M_peak) !update ionized region
!      P_Node%L_ion = Pop%F_esc * N_ion / del_t! ionizing luminosity [in photons / second]
      P_Node%L_ion_II  = Pop%F_esc * N_ion_II  / del_t ! ionizing luminosity from Pop II stars  [in photons / second]
      P_Node%L_ion_III = Pop%F_esc * N_ion_III / del_t ! ionizing luminosity from Pop III stars [in photons / second]

      if (P_node%M_starII+P_node%M_starIII .lt. M_star_old) then
        write(*,*) "Stellar mass is sinking?!?", P_node%M_starII, P_node%M_starIII, M_star_old, j
        stop
      endif

      Dummy_Node => P_Node%base


#ifdef METAL_YIELDS
      if ( .not. (P_Node%m_metals(3,N_ELEMENTS) .eq. P_Node%m_metals(3,N_ELEMENTS))) then
        write(*,*) "Metal mass is NAN:", P_Node%M_hot, P_Node%M_cold, M_out_hot+M_out_cold, &
            &P_Node%m_metals(1:4,N_ELEMENTS), frac_remain
        stop
      endif
#endif

#ifdef Satellites
      call WriteLLStarsToFileAndDel(P_Node)
#endif
      P_node%pop = pop_now

#if defined(DEBUG) && defined(NBODY)
      !output times that halos spend in SF_step() function
      call CPU_TIME(time_end)!not thread-safe
      if(time_end-time_start > 1e-3 .and. N_ELEMENTS == 3)then!output only long ones, otherwise file is too big
        write(iunit_list(5),'(I2,I10,16E11.3)')&
            P_node%pop,P_node%id,time_end-time_start,zlev(P_Node%jlevel),P_Node%Mhalo,M_star_form,&!4
            P_Node%M_starII,P_Node%m_hot,P_Node%m_cold, P_Node%m_out,&!4
            TotalMetalEjectaThisStep(N_ELEMENTS), TotalSNeEnergyThisStep, MHeatThisStep,&!3
            TotalSNeMomentumThisStep, P_Node%M_starIII, Z_temp!2+len(Z_temp)
      endif

      !output data around specified redshift to generate slice plot
      if(ABS(zlev(P_Node%jlevel) - 6) < 0.1) then
        write(iunit_list(6),'(8E12.4)')&
            zlev(P_Node%jlevel),P_Node%Mhalo,P_Node%x(1),P_Node%x(2),P_Node%x(3),&
            P_Node%IGM_Z(3),P_Node%R_ion,P_Node%r_en
      endif
#endif

#ifdef OUTPUT_ALL_MASSIVE_STARS
      NextStarToOutput => P_Node%HighStars_first
      do while(associated(NextStarToOutput))
         pop_out = NextStarToOutput%pop
         num_out = NextStarToOutput%num
         id_out  = NextStarToOutput%bin_id
         if (pop_out .eq. 2) then
            mass_out = popII%mass(id_out)
         else
            mass_out = popIII%mass(id_out)
         endif
         age = t_current - NextStarToOutput%timeborn
         write(iunit_list(8),'(1E12.4, I15, I2, I8, 2E12.4)')&
             zlev(P_Node%jlevel), P_Node%id, pop_out, num_out, mass_out, age
         NextStarToOutput => NextStarToOutput%next
      enddo
#endif

      !$OMP END CRITICAL
  end subroutine SF_step


  subroutine add_parent_to_tree(Node)
      type(TreeNode), intent(inout) :: Node
      type(TreeNode), pointer :: P_Node, C_Node ! parent node, child nodes

#ifndef METAL_YIELDS
      real, dimension(N_ELEMENTS) :: null_metal
#endif

      P_Node => Node%parent
      C_Node => P_Node%child
      !To make sure each halo is only added to the tree once
      do while (.not. C_Node%i_SF)
        C_Node => C_Node%sibling
      end do
      if (C_Node%ID .ne. Node%ID) return ! Makes sure we only call this routine once for each halo

#ifndef NO_XYZ
      !bubbles only work with spatial information
#ifdef METAL_YIELDS
      call add_bubble_to_tree(P_Node, get_m_metals(P_Node,5))
#else
      null_metal(:) = 0.
      call add_bubble_to_tree(P_Node, null_metal)
#endif
#endif

  end subroutine


  real function dt_adaptive(P_Node,coldgas_tff,t_dyn,t_dust,del_M_hot,del_t,t_step,pop_now,t_current)
      !timestep in sec

      type(TreeNode), intent(in) :: P_Node
      real, intent(in) :: coldgas_tff,t_dyn,t_dust,del_M_hot,del_t,t_step,t_current
      integer, intent(in) :: pop_now

      real :: t_star, t_acc
      real :: M_SF

      !We add an offset mass to prevent small timescales due to small masses
      if(pop_now .eq. 3) then
        M_SF = MIN(P_Node%M_cold,P_Node%M_starIII)
      else
        M_SF = MIN(P_Node%M_cold,P_Node%M_starII)
      endif
      t_star = (M_SF+1e2) / M_star_dot(P_Node%M_cold,coldgas_tff,pop_now, t_current)

      t_acc = (P_Node%M_hot+1e4) / ((del_M_hot+1.0)/del_t)!offset +1Msun to avoid division by infitiny

      !using 10% of dynamical time here improves time integration for cold gas
      dt_adaptive = MIN(t_star,0.1*t_dyn,0.1*t_dust,t_acc,del_t)

#if defined(OUTPUT_GAS_BRANCH) && defined(DEBUG)
      !Write this information only for halos along OUTPUT_GAS_BRANCH
      !Otherwise, too crowded
      if(P_Node%id == NextOutputID .and. dt_adaptive<3.15e7*1e6)then
        if(t_star < 3.15e13) write(0,*)"t_star < 1Myr"
        if(t_dyn < 3.15e13) write(0,*)"t_dyn < 1Myr"
        if(t_dust < 3.15e13) write(0,*)"t_dust < 1Myr"
        if(t_acc < 3.15e13) write(0,*)"t_acc < 1Myr"
        if(del_t < 3.15e13) write(0,*)"del_t < 1Myr"
      endif
#endif

      !!!Set accuracy here!!!
      !2.5% is sufficient in tested cases
      dt_adaptive = (0.025*dt_adaptive)+YEAR_cgs
      !+YEAR_cgs has two effects:
      !1. It prevents the timestep of getting unnecessarily small in some pathological configurations.
      !2. This offset prevents the last timestep in a redshift step from being tiny due to numerical rounding errors:
      !   We always guarantee that the last timestep ends on the next redshift step.
      !   When all physical times are long, a redshift step would be divided into 40 timesteps of equal length.
      !   However, due to numerical inprecission and rounding errors, these timesteps do not have exactly the same length.
      !   So it could happen that we have 40 timesteps of 1Myr each, but then need one final step of the order few seconds
      !   to complete to the next redshift step. Adding one year to each timestep mitigates the problem,
      !   because we would then have 39 timesteps that are a bit longer and the final timestep that is a bit shorter,
      !   but all timesteps are of the same order of magnitude.

#ifdef DEBUG
      !A small timestep is mostly caused by t_acc if P_Node%M_hot is very small
      !Check for other reasons of small timestep
      if(MIN(t_star,t_dyn,t_dust,del_t) < 1e-6*del_t) then
        write(0,*)"SMALL dt"
        write(0,*)dt_adaptive/YEAR_cgs,del_t/YEAR_cgs
        write(0,*)t_star/YEAR_cgs,t_dyn/YEAR_cgs,t_dust/YEAR_cgs,t_acc/YEAR_cgs,t_step/YEAR_cgs
        write(0,*)P_Node%M_hot,del_M_hot,P_Node%Mhalo
      endif
      if(dt_adaptive < 0) then
        write(0,*)"WARNING!!! dt_adaptive < 0"
      elseif(t_step < 0) then
        write(0,*)"WARNING!!! t_step < 0"
      endif
#endif

      dt_adaptive = MIN(dt_adaptive,t_step)!to end exactly on next redshift step


  end function dt_adaptive


  real function M_star_dot(M_cold,coldgas_tff,pop_now, time)
      !star formation rate in this substep (Msun/s)
      use Numerical_Parameters, only: ETAII, ETAIII
      implicit none
      real, intent(in) :: M_cold,coldgas_tff,time
      integer, intent(in) :: pop_now

      if(pop_now .eq. 3)then
        M_star_dot = ETAIII * (M_cold/coldgas_tff)
      else
        M_star_dot = ETAII * (M_cold/coldgas_tff)
      endif
  end function M_star_dot

end module star_formation
