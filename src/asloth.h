!Compiler flag options:
!You can comment in/out desired compiler flag options that change the behaviour/physics of A-SLOTH


!Switches on additional text outputs while running
#define INFO

!Follow individual chemical elements
#define METAL_YIELDS

!Metallicity Distribution Function; tracks stellar masses as a function of [Fe/H] down to z=0
#define MDF

!Output volume fractions based on random sampling of volume. Works best for rectangular boxes.
#define VOL_FRACS

!Run without spatial information. Necessary for EPS-based trees. Not recommended if spatial information is available
#define NO_XYZ

!Using NBODY trees (e.g. Caterpillar) instead of statistical EPS trees
!#define NBODY

!Outputs baryon-information during sub-cycling along main branch. NextOutputID needs to be set if NBODY
!#define OUTPUT_GAS_BRANCH

!Use stellar parameters from SEVN
!Compile with 'make hdf5' to also include HDF5 libraries
!#define SEVN

!Shows more debugging information
!#define DEBUG

!Remove unnecessary columns of tree file and write smaller version. Only useful with NBODY
!#define CREATE_SMALL_TREE_FILE

!Output information of satellite galaxies
!#define Satellites

!Have only one IMF mass bin between M_SURVIVE and M_ION to save computational time and memory
!#define COMPACT_IMF

!Output recovery times defines as the time between the first PopIII SN and the first PopII SN on a branch
#define RECOVERY_TIME

!write fine grained information about SF and metallicities to a file
!#define OUTPUT_ALL_Z

!write out list of every living massive star after every timestep
#define OUTPUT_ALL_MASSIVE_STARS

! Disable spatial information if running with EPS trees
#ifndef NBODY
#ifndef NO_XYZ
#define NO_XYZ
#endif
#endif

!Dust physics
!#define DUST
