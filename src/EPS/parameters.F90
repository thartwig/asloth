#include "../asloth.h"
! Prompt for and read in all model parameters.
! Many are then distributed to the subroutines that need to know them
! via a set of modules.



module Power_Spectrum_Parameters
  ! Variables used to hold properties of the power spectrum
  !
  !
  ! Integers
  integer igwave,ireset,itrans,nktab,NKTABMAX,NSPL
  !
  ! Array dimensions
  parameter(NKTABMAX=4096)
  !
  ! Floats
  real dndlnk,gamma,kref,lnktab(NKTABMAX),lnpktab(NKTABMAX),mwdm,nspec,sigma8,scla,sclm
  !
  !
  ! Characters
  character pkinfile*1024,splinefile*220
  !
  ! Parameters
  parameter (NSPL=200)
end module Power_Spectrum_Parameters



module Time_Parameters
  ! Parameters used in making binary splits in the merger tree
  real eps1,eps2
end module Time_Parameters



module Run_Statistics
  ! Variables holding information on run-time statistics
  !
  ! Integers
  integer max_inode,nc1,nc2,ncall,nfail
#ifdef DEBUG
  data ncall /0/
  data nc1 /0/
  data nc2 /0/
#endif
end module Run_Statistics
