#include "../asloth.h"
module EPS_wrapper
  !module to interface with the Parkinson et al. 2008 tree generation routines
  use Power_Spectrum_Parameters ! parameters.F90
  use make_tree_module
  use Tree_Memory_Arrays ! memory_modules.F90
  use Tree_Memory_Arrays_Passable ! memory_modules.F90 ! MergerTree in here
  use Tree_Routines ! tree_routines.F90
  use Modified_Merger_Tree ! modified_merger_tree.F90
  use defined_types, only: TreeNode
  use numerical_parameters, only: mres, zmin
  use tree_initialization, only: set_tree_params
  implicit none
contains

subroutine make_EPS_tree
  integer :: j !loop index
  type (TreeNode), pointer :: This_Node
  integer :: nhalomax, nhalo
  integer :: jphalo(nlev_max), nhalolev(nlev_max), iter, ierr
  real :: deltcrit, sigmacdm, ahalo
  EXTERNAL deltcrit, sigmacdm, split
  call set_tree_params
  ierr=1     !initial error status used to control make_tree()
  nhalomax=0 !initialise
  nhalo=0
  ahalo=1/(1+zmin)
  iter=1   !if we run out of allocated memory, which is flagged
  !by ierr=1 or ierr=2 then we do another iteration 
  !with more allocated memory
  do while (ierr.ne.0 .or. iter.eq.1) 
    !if needed increase the amount of memory allocated
    call Memory(nhalo,nhalomax,ierr,nlev,tree_mass,mres)
    do j = 1, nhalomax, 1
      MergerTree_Aux(j)%id = j
    end do
    MergerTree => MergerTree_Aux  !Maps MergerTree to allocated 

    call make_tree(tree_mass,mres,split,sigmacdm,deltcrit,&
        & nhalomax,ierr,nhalo,nhalolev,jphalo)
    iter=iter+1
  end do

  write(stdout,*)'made tree'

  ! - determine the number of nodes
  This_Node => MergerTree(1)
  write(stdout, *) 'counting nodes'
  number_of_nodes = 0
  do while (associated(This_Node)) !go through all nodes
    number_of_nodes = number_of_nodes + 1
    This_Node => Walk_Tree(This_Node)
  end do
  end subroutine 

end module EPS_wrapper
